
exports.up = function(knex, Promise) {

    return Promise.all([


         knex.schema.alterTable('users', function(table) {
            table.string("u_email_login_ok_subsystems").notNullable().defaultTo(" accountapi ftp ");
         }),

    ])

};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.alterTable('users', function(table){
            table.dropColumn("u_email_login_ok_subsystems");
        }),
    ])

};
