
exports.up = function(knex, Promise) {

    return Promise.all([


         knex.schema.alterTable('tels', function(table) {
            table.boolean('t_escalate').notNullable().defaultTo(false);
         }),

         knex.schema.alterTable('emails', function(table) {
            table.boolean('e_escalate').notNullable().defaultTo(false);
         }),

    ])

};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.alterTable('emails', function(table){
            table.dropColumn("e_escalate");
        }),
        knex.schema.alterTable('tels', function(table){
            table.dropColumn("t_escalate");
        }),
    ])

};
