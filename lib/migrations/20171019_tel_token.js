
exports.up = function(knex, Promise) {

    return Promise.all([


         knex.schema.alterTable('tels', function(table) {
            table.string('t_token');
            table.timestamp('t_token_generated_at');
         }),

    ])

};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.alterTable('tels', function(table){
            table.dropColumn("t_token");
            table.dropColumn("t_token_generated_at");
        }),
    ])

};
