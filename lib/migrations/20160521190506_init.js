
exports.up = function(knex, Promise) {

    return Promise.all([

        knex.schema.createTable('users', function(table) {
            table.uuid('u_id').primary();
            table.string('u_name').notNullable();
            table.string('u_primary_email').notNullable();
            table.string('u_primary_email_stripped').notNullable();

            table.string('u_primary_tel').notNullable();

            // these two are redundent here, so token based monsterexpress apps might implement any actions if the user is not yet verified
            table.boolean('u_primary_tel_confirmed').notNullable().defaultTo(false);
            table.boolean('u_primary_email_confirmed').notNullable().defaultTo(false);

            table.string('u_language').notNullable();

            // callin secret of the user for telephone based authentication
            table.string('u_callin').notNullable().defaultTo("");
            
            table.string('u_comment').notNullable().defaultTo("");

            table.boolean("u_deleted").notNullable().defaultTo(false);

            table.index(["u_primary_email_stripped","u_deleted"]);

            table.uuid('u_grant_access_for') // this is a reference to another user account who is permitted to manage this account
                 .references('u_id')
                 .inTable('users')
                 .notNullable()
                 .defaultTo(0)

            table.boolean("u_sms_notification").notNullable().defaultTo(1);

            // todo: change this one to subsystem based (space separated elements in a string)
            table.boolean("u_email_login_fail").notNullable().defaultTo(0);
            table.boolean("u_email_login_ok").notNullable().defaultTo(0);

            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());
            table.timestamp('updated_at').notNullable().defaultTo(knex.fn.now());

        }),

         knex.schema.createTable('emails', function(table) {
            table.increments('e_id').primary();
            table.uuid('e_user_id')
                 .references('u_id')
                 .inTable('users');
            table.string('e_email').notNullable();
            table.string('e_email_stripped').index().notNullable();
            table.boolean('e_confirmed').notNullable().defaultTo(false);
            table.timestamp('e_confirmed_at');
            table.boolean('e_primary').notNullable().defaultTo(false);
            table.string('e_categories').notNullable();
            table.string('e_token');
            table.timestamp('e_token_generated_at');
            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());
            table.timestamp('updated_at').notNullable().defaultTo(knex.fn.now());
        }),

         knex.schema.createTable('tels', function(table) {
            table.increments('t_id').primary();
            table.uuid('t_user_id')
                 .references('u_id')
                 .inTable('users');
            table.string('t_country_code').notNullable();
            table.string('t_area_code').notNullable();
            table.string('t_phone_no').notNullable();
            table.string('t_human').notNullable().index();
            table.boolean('t_confirmed').notNullable().defaultTo(false);
            table.timestamp('t_confirmed_at');
            table.boolean('t_primary').notNullable().defaultTo(false);
            table.string('t_category').notNullable();
            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());
            table.timestamp('updated_at').notNullable().defaultTo(knex.fn.now());
        }),

        knex.schema.createTable('credentials', function(table){
            table.increments('c_id').primary();
            table.uuid('c_user_id')
                 .references('u_id')
                 .inTable('users');

            table.string('c_username').unique().index().notNullable();
            table.string('c_password').notNullable();

            table.string('c_grants').notNullable();

        }),


        knex.schema.createTable('sessions', function(table){
            table.increments('s_id').primary();
            table.uuid('s_user_id')
                 .references('u_id')
                 .inTable('users');
            table.integer('s_credential_id')
                 .references('c_id')
                 .inTable('credentials');

            table.string('s_authtoken').index().notNullable();

            table.string('s_grants').notNullable();

            table.timestamp('s_added').notNullable();
            table.timestamp('s_expires').notNullable();
        }),


        // we dont need uuid here, since this table will be used for looking up by domain names only
        knex.schema.createTable('domains', function(table){
            table.increments('d_id').primary();
            table.uuid('d_user_id')
                 .references('u_id')
                 .inTable('users');
            table.string('d_domain_canonical_name').notNullable();
            table.string('d_domain_name').notNullable();
            
            table.index(["d_domain_canonical_name","d_domain_name"]);
        }),

        // same here. the real lookup would be done via the server/webhosting_id combination (which should be unique)
        knex.schema.createTable('webhostings', function(table){
            table.increments('wh_id').primary();
            table.uuid('wh_user_id')
                 .references('u_id')
                 .inTable('users');
            table.integer('wh_server_id').notNullable();
            table.integer('wh_webhosting_id').notNullable();
        })        


    ])
	
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('users'),
        knex.schema.dropTable('emails'),
        knex.schema.dropTable('tels'),
        knex.schema.dropTable('credentials'),
        knex.schema.dropTable('sessions'),
        knex.schema.dropTable('domains'),
        knex.schema.dropTable('webhostings'),
        knex.schema.dropTable('loginlog'),
    ])
	
};
