
exports.up = function(knex, Promise) {

    return Promise.all([


        knex.schema.createTable('sms', function(table){
            table.increments('s_id').primary();
            table.string('s_provider').notNullable().defaultTo("");

            table.string('s_country_code').notNullable();
            table.string('s_area_code').notNullable();
            table.string('s_phone_no').notNullable();
            table.string('s_recipient').notNullable();

            table.string('s_message').notNullable();
            table.enum('s_status', ["failed","sent","sent_and_received"]).notNullable();
            table.timestamp('s_timestamp').notNullable().defaultTo(knex.fn.now());

            table.index(["s_recipient"]);
        }),

    ])

};


exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('sms'),
    ])

};
