
exports.up = function(knex, Promise) {

    return Promise.all([


         knex.schema.alterTable('users', function(table) {
            table.string('u_totp_secret');
            table.boolean('u_totp_active').notNullable().defaultTo(false);


            // note: we dont drop this column in knex, since it invalidates the unique column indexes
            // table.dropColumn("t_max_number_of_python_apps");

        }),

    ])

};


exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.alterTable('users', function(table){
            table.dropColumn("u_totp_secret");
            table.dropColumn("u_totp_active");
        }),
    ])

};
