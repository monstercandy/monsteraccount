exports.up = function(knex, Promise) {

    return Promise.all([

         knex.schema.createTable('webhostingdomains', function(table) {
            table.increments('wd_id').primary();
            table.string('wd_domain_canonical_name').notNullable().index();
            table.string('wd_domain_name').notNullable();
            table.string('wd_server').notNullable();
            table.integer('wd_webhosting');
         })
    ])

};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('webhostingdomains'),
    ])
};
