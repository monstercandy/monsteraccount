
exports.up = function(knex, Promise) {

    return Promise.all([


         knex.schema.alterTable('sessions', function(table) {
            table.string('s_ip').notNullable().defaultTo("");
        }),


    ])
    
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.alterTable('sessions', function(table){
            table.dropColumn("s_ip");
        }),
    ])
    
};
