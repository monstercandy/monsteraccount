
exports.up = function(knex, Promise) {

    return Promise.all([


         knex.schema.alterTable('webhostingtemplates', function(table) {
            table.integer('t_git_max_repos').notNullable().defaultTo(0);
            table.integer('t_git_max_branches').notNullable().defaultTo(0);
            

        }),


    ])
    
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.alterTable('webhostingtemplates', function(table){
            table.dropColumn("t_git_max_repos");
            table.dropColumn("t_git_max_branches");
        }),
    ])
    
};
