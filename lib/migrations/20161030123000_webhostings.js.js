
exports.up = function(knex, Promise) {

    return Promise.all([


         knex.schema.createTable('webhostingtemplates', function(table) {
            table.increments('t_id').primary();
            table.string('t_name').notNullable().unique();

            table.integer('t_storage_max_quota_hard_mb').notNullable();   
            table.integer('t_storage_max_quota_soft_mb').notNullable();   

            table.integer('t_domains_max_number_attachable').notNullable();   

            table.integer('t_email_max_number_of_accounts').notNullable();   
            table.integer('t_email_max_number_of_aliases').notNullable();  
 
            table.integer('t_ftp_max_number_of_accounts').notNullable();  
            table.boolean('t_awstats_allowed').notNullable();  
            table.integer('t_cron_max_entries').notNullable();  

            table.string('t_allowed_dynamic_languages').notNullable();  

            table.integer('t_php_max_number_of_emails_in_window').notNullable();

            table.boolean('t_db_max_number_of_databases').notNullable();  
            table.integer('t_db_max_databases_size_mb').notNullable();

            table.boolean('t_x509_certificate_external_allowed').notNullable();
            table.boolean('t_x509_certificate_letsencrypt_allowed').notNullable();

            table.boolean('t_public').notNullable().defaultTo(false);

            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());
        }),


    ])
    
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('webhostingtemplates'),
    ])
    
};
