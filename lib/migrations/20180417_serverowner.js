exports.up = function(knex, Promise) {

    return Promise.all([


         knex.schema.alterTable('servers', function(table) {
            table.string('s_users').notNull().defaultTo("");
         }),

    ])

};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.alterTable('servers', function(table){
            table.dropColumn("s_users");
        }),
    ])

};
