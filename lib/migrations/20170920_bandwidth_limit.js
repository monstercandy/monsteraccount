
exports.up = function(knex, Promise) {

    return Promise.all([


         knex.schema.alterTable('webhostingtemplates', function(table) {
            table.string('t_bandwidth_limit').notNullable().defaultTo("{}");

        }).then(()=>{
            return knex.raw("UPDATE webhostingtemplates SET t_bandwidth_limit='{}'").return()
        }),

    ])

};


exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.alterTable('webhostingtemplates', function(table){
            table.dropColumn("t_bandwidth_limit");
        }),
    ])

};
