
exports.up = function(knex, Promise) {

    return Promise.all([


         knex.schema.alterTable('webhostingtemplates', function(table) {
            table.integer('t_max_container_apps').notNullable().defaultTo(0);


            // note: we dont drop this column in knex, since it invalidates the unique column indexes
            // table.dropColumn("t_max_number_of_python_apps");

        }),

    ])

};


exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.alterTable('webhostingtemplates', function(table){
            table.dropColumn("t_max_container_apps");
        }),
    ])

};
