
exports.up = function(knex, Promise) {

    return Promise.all([

        knex.schema.createTable('servers', function(table) {
            table.increments('s_id').primary(); // we dont need uuid here neither because lookups would be done via the servername
            table.string('s_name').notNullable();
            table.string("s_secret").notNullable();
            table.string("s_roles").notNullable().defaultTo("");
            table.unique(['s_name']);

        }),


        knex.schema.dropTable('webhostings'), // we havent used it so far anyway

    ])
    .then(()=>{

        return Promise.all([
                    knex.schema.createTable('webhostings', function(table){
                        table.increments('wh_id').primary();
                        table.uuid('wh_user_id')
                             .references('u_id')
                             .inTable('users');
                        table.string('wh_server_id').notNullable();
                        table.integer('wh_webhosting_id').notNullable();
                        table.unique(['wh_server_id', 'wh_webhosting_id']);
                    })     

            ])


        // we will need one more table which connects domains to servers.
        // so we would know whether dns adjustment would be needed when the user attaches the domain to one of his webhostings

    })
	
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('servers'),
    ])
	
};
