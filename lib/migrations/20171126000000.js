
exports.up = function(knex, Promise) {

    return Promise.all([


         knex.schema.alterTable('credentials', function(table) {
            table.string('c_server');
            table.string('c_uuid');
         }),

    ])

};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.alterTable('credentials', function(table){
            table.dropColumn("c_server");
            table.dropColumn("c_uuid");
        }),
    ])

};
