
exports.up = function(knex, Promise) {

    return Promise.all([


         knex.schema.alterTable('webhostingtemplates', function(table) {
            table.integer('t_max_number_of_python_apps').notNullable().defaultTo(0);
        }),


    ])
    
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.alterTable('webhostingtemplates', function(table){
            table.dropColumn("t_max_number_of_python_apps");
        }),
    ])
    
};
