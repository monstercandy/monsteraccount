
exports.up = function(knex, Promise) {

    return Promise.all([


         knex.schema.alterTable('webhostingtemplates', function(table) {
            table.dropColumn('t_php_max_number_of_emails_in_window');
            table.string('t_webapp_email_quota_policy').notNull().defaultTo("");
            // TODO: upgrade knex to latest then remove this line before upgrading anywhere in production:
            table.unique('t_name'); // this is lost otherwise (knex bug)
         }),

    ])

};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.alterTable('webhostingtemplates', function(table){
            table.dropColumn("t_webapp_email_quota_policy");
        }),
    ])

};
