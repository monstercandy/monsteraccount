
exports.up = function(knex, Promise) {

    return Promise.all([


         knex.schema.alterTable('webhostingtemplates', function(table) {
            table.integer('t_installatron_allowed').notNullable().defaultTo(0);

        }),

    ])
    
};


exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.alterTable('webhostingtemplates', function(table){
            table.dropColumn("t_installatron_allowed");
        }),
    ])
    
};
