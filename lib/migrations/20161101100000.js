
exports.up = function(knex, Promise) {

    return Promise.all([

        knex.schema.dropTable('webhostings'), // we havent used it so far anyway

    ])
    .then(()=>{

        return Promise.all([
                    knex.schema.createTable('webhostings', function(table){
                        table.increments('w_id').primary();
                        table.uuid('w_user_id')
                             .references('u_id')
                             .inTable('users');
                        table.string('w_server_name').notNullable();
                        table.integer('w_webhosting_id').notNullable();
                        table.unique(['w_server_name', 'w_webhosting_id']);
                    })     

            ])


        // we will need one more table which connects domains to servers.
        // so we would know whether dns adjustment would be needed when the user attaches the domain to one of his webhostings

    })
	
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('servers'),
    ])
	
};
