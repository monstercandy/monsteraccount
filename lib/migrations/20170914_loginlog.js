
exports.up = function(knex, Promise) {

    return Promise.all([


        knex.schema.createTable('loginlog', function(table){
            table.uuid('l_id');
            table.string('l_server').notNullable();
            table.uuid('l_user_id').notNullable();
            table.uuid('l_username').notNullable();
            table.timestamp('l_timestamp').notNullable();
            table.string('l_ip').notNullable();
            table.string('l_subsystem').notNullable();
            table.string('l_event_type').notNullable();
            table.string('l_agent').notNullable().defaultTo("");
            table.boolean('l_successful').notNullable();
            table.string('l_other').notNullable().defaultTo("");
            table.boolean('l_processed').notNullable().defaultTo(0);

            // todo: reverse dns, country/city
            table.string('l_country').notNullable().defaultTo("");

            table.index(["l_id"]);
            table.index(["l_user_id"]);
            table.index(["l_processed"]);
        }),

    ])

};


exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('loginlog'),
    ])

};
