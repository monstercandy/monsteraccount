// expected permissions:  ["SEND_EMAIL","FEED_LOGIN_IP","EMIT_LOG","INFO_WEBHOSTING"]

module.exports = function(moduleOptions) {
  moduleOptions = moduleOptions || {}
  moduleOptions.subsystem = "accountapi"

  require( "../../MonsterCommon/lib/MonsterDependencies")(__dirname)

  const loginLogLib = require("loginlog-lib.js")
  const smsLib = require("sms-lib.js")
  const path = require('path')
  const util = require('util');

  const dotq = require("MonsterDotq");

  var fs = dotq.fs();

  var config = require('MonsterConfig');
  var me = require('MonsterExpress');
  const MError = me.Error

  const webhostingDomainConstraint = {
            wd_server: {presence: true, isString: {strictName: true}},
            wd_domain_name: {presence: true, isHost: {returnCanonical:true}},
            wd_webhosting: {presence: true, isInteger: {lazy:true,returnInt: true}},
         };


  config.appRestPrefix = "/accountapi"

  config.defaults({
    "i18n-2": {
       "locales": ["en", "hu"],
    },

    "throw_when_domain_is_present_for_others": false,

    "sms_seeme_api_url": "https://seeme.hu/gateway",

    "escalate_sms_interval_seconds": 3600,
    "escalate_sms_silent_hours": [23,24,0,1,2,3,4,5,6,7],
    "escalate_exceptions_via_sms": ["WEBSERVER_REHASH_FAILED"],
    "escalation_interval_second": 60,
    "escalation_email_same_event_type_delay_second": 60*60,

    "webhosting_domains_can_be_added_without_txt_records": false,

    "allow_telno_confirmation": true,

    "max_request_body_size": 50*1024, // 50 kbytes per default due to eventlog synchronization (this should be good for 300 events)
    "session_expiration": 3600,
    "session_expiration_subcredential": 24*3600,
    "session_cleanup_interval_seconds": 900,
    "email_token_validity": 86400,
    "tel_token_validity": 3600,
    "cron_loginlog_notify_unsuccessful":"0 * * * *",
    "cron_loginlog_notify_successful": "*/5 * * * *",
    "loginlog_active": true,
    "loginlog_notify_successful_interval": 5*60*1000, // 5 minutes
    "loginlog_notify_unsuccessful_interval": 60*60*1000, // 1 hour
    "telephone_required": true,
    "tel_should_be_confirmed": true,
    "linear_numeric_account_ids": false,
    "max_number_of_credentials_per_account": 20,
    "event_sync_flush_seconds": 5,
    "loginlog_cleanup": {
      "0 5 * * *": [
        {table: "loginlog", agePrefix: "l_", ageDays: 14, rawFilter:"l_successful=0 AND l_subsystem!='accountapi'"},
        {table: "loginlog", agePrefix: "l_", ageDays: 90, rawFilter:"l_successful=1 AND l_subsystem!='accountapi'"},
      ]
    },
  });


  const webhosting_domains_can_be_added_without_txt_records = config.get("webhosting_domains_can_be_added_without_txt_records");


  var app = me.Express(config, moduleOptions);
  app.enable("trust proxy")  // this application receives x-forwarded-for headers from other internal systems (typically relayer)

  const dns = require("dns");
  app.dnsResolveAsync = dotq.single(dns.resolve);

  const EMAIL_NOT_CARE= 0
  const EMAIL_SHOULD_BE_VERIFIED = 1
  const EMAIL_SHOULD_NOT_BE_VERIFIED = 2

  const request = require("RequestCommon");
  app.PostForm = request.PostForm;

  app.smsProviders = buildSmsProviders();

  var router = app.PromiseRouter(config.appRestPrefix)

  app.GetRelayerPool = app.GetRelayerMapiPool

  const MonsterInfoLib = require("MonsterInfo")
  app.MonsterInfoWebhosting = MonsterInfoLib.webhosting({config: app.config, webhostingServerRouter: app.ServersRelayer})


  var knex
  var Common = require("models/common.js")
  var Account
  var Credential
  var Tel
  var Email
  var Server
  var WebhostingTemplates
  var bcrypt = require('bcrypt-promise.js');
  var vali = require("MonsterValidators").ValidateJs()

  const moment = require("MonsterMoment")



  app.getOldestPossibleEmailToken= function(){
      return moment().add(-1 * config.get("email_token_validity"),"second").toISOString()
  }
  app.getOldestPossibleTelToken= function(){
      return moment().add(-1 * config.get("tel_token_validity"),"second").toISOString()
  }


  const Token = require("Token")
  var txtRecordToken;


  function sendAuthToken(prom, knex, plainPassword, req, next, eventLog){
    var credential
    var token
    var expiresAt
    var grants
    var session_identifier

    eventLog = eventLog ||{}

    var account;

    return prom
          .then((aCredential) => {
              credential = aCredential

              return credential.ResolveAccount()
          })
          .then((aAccount)=>{
              account = aAccount;

              if((credential.is_primary)&&(account.u_totp_active)&&(!eventLog.discardTotp)) {
                  if(!req.body.json.userToken)
                    throw new MError("TOTP_REQUIRED");

                  return account.ThrowIfInvalidToken(req.body.json.userToken);
              }
          })
          .then(()=>{

              grants = Common.ForceEnumerableToString( credential.c_grants )

              return Token.GetPseudoTokenAsync(plainPassword+credential.c_user_id+grants, credential["c_password"], req.query.ts, 16)
              // return Token.GetTokenAsync(16)
          })
          .then((aToken)=>{

             token = aToken

              return Token.GetPseudoTokenAsync(plainPassword+credential.c_user_id+grants, token, req.query.ts, 16)


          })
          .then((aSession_identifier)=>{

             session_identifier = aSession_identifier

             return knex("sessions").select("s_id").whereRaw("s_authtoken=?", [token])
          })
          .then(rows=>{
             if(rows.length > 0) {
               expiresAt = rows[0].s_expires
               return Promise.resolve()
             }

             expiresAt = getExpirationDate(credential.is_primary);

             return knex.insert({
                            s_user_id: credential.c_user_id,
                            s_grants: grants,
                            s_credential_id: credential.c_id,
                            s_authtoken: token,
                            s_unique_session_identifier: session_identifier,
                            s_added: moment.now(),
                            s_expires: expiresAt ,
                            s_ip: req.clientIp,
                          })
                         .into("sessions")

          })
          .then(()=>{
              var otherStr = (eventLog.other || "")
              if(otherStr) otherStr += ","
              otherStr += grants

              app.InsertEvent(req, {
                 e_event_type: eventLog.type,
                 e_username: eventLog.username|| "",
                 e_user_id: credential.c_user_id,
                 e_other: {msg:otherStr, successful: true}
              })

              req.result = {
                "token": token,
                "expires_at": expiresAt,
                "u_name": account.u_name,
                "u_id": account.u_id,
                "u_primary_email": account.u_primary_email,
                "s_grants": credential.c_grants, // we return the grants, since it might be needed for custom redirects on the client side
              }
              next()

          })

  }

  router.get( "/authentication/sessionips", function(req,res,next){
    return knex("sessions").distinct("s_user_id").select("s_ip","s_expires")
          .then(m=>{
              req.result = m
              next()
          })
  })


  router.post( "/authentication/credential",
       function (req, res, next) {
          var j = req.body.json
          var credential

          return knex.transaction(function(trx){

              var  cred = new Credential(trx)

              var p = vali.async(j, {
                 "username": {presence: true, isString: {trim:true}},
                 "password": Common.passwordEnforcements,
                 "server": {isString: {trim:true}},
                 "userToken": {isString: {trim:true}},
              })
              .then((data)=>{

                   return cred.GetCredentialByUsername(j["username"], data.server)
                     .catch(ex=>{
                         app.InsertEvent(req, {e_event_type: "auth-credential", e_other: {msg:"username not found", successful: false}, e_username: j["username"]})

                         throw ex // rethrowing
                     })

              })
              .then(aCredential =>{
                   credential = aCredential
                   return credential.Authenticate(j["password"])
                     .catch(ex=>{
                         app.InsertEvent(req, {e_event_type: "auth-credential", e_other: {msg:"password incorrect", successful: false}, e_username: j["username"], e_user_id: credential.c_user_id})

                         throw ex

                     })
              })
              .then(()=>{

                var x = {}
                x[credential.c_user_id] = req.clientIp;

                if(credential.is_primary) {

                   reportSuccessfulAuthenticationToFtp(x)
                }

                return Promise.resolve(credential)
              })

              return sendAuthToken(p, trx, j["password"], req, next, {type: "auth-credential", username:j.username})


          })


       })

  function reportSuccessfulAuthenticationToFtp(userIpPairs) {

     // userIpPairs is: {"1234": "127.0.0.1"}

     var ps = []
     var perServer = {}
     Object.keys(userIpPairs).forEach(user_id=>{

         var ip = userIpPairs[user_id]

         ps.push(
            knex("webhostings").distinct("w_server_name").whereRaw("w_user_id=?",[user_id])
              .then(rows=>{
                  rows.forEach(row=>{
                     if(!perServer[row.w_server_name]) perServer[row.w_server_name] = {}
                     perServer[row.w_server_name][user_id] = ip
                  })
              })
         )
     })

     return Promise.all(ps)
       .then(()=>{
           var servers = Object.keys(perServer)
           if(servers.length <= 0) return

           var ps = []
           var pool = app.GetRelayerPool()
           servers.forEach(server=>{
              var uri = `/s/${server}/ftp/sites/admin-ips`
              var payload = perServer[server]

              ps.push(
                 pool.postAsync(uri, payload)
                   .catch(ex=>{
                       console.error("Couldnt report ips: ", uri, payload, ex)
                   })
              )

           })

           return Promise.all(ps)
       })
  }

  router.post("/authentication/forgotten", function(req,res,next) {

      return vali.async(req.body.json,{
          "email": {presence: true, email: true},
          "confirmation_token": {presence: true, isString: true},
          "account_id": {presence: true, isString: true},
      })
      .then(d=>{
          req.params.account_id = d.account_id

          return getAccountTrx(req,res,next,(trx, account)=>{

              if(d.email != account.u_primary_email) throw new MError("AUTH_FAILED")

              var eventLogParams = {type: "auth-token"}
              var p = account.FetchEmails(d.email)
                .then(emails =>{
                     var email = Common.EnsureSingleRowReturned(emails, "AUTH_FAILED")
                     if(!email.e_primary) throw new MError("AUTH_FAILED")

                     eventLogParams.username= email.e_email

                     return email.Verify(d, app, req)
                        .catch(ex=>{
                            app.InsertEvent(req, {e_event_type: "auth-token", e_other: {msg:"invalid token", successful: false},  e_username: email.e_email, e_user_id: account.u_id})

                            throw new MError("AUTH_FAILED")
                        })
                })
                .then(()=>{
                   var cred = new Credential(trx)
                   var cr = cred.FromConstants({"c_id": 0, "c_user_id": account.u_id, "c_password": d.confirmation_token, "c_grants": ["ACCOUNT_FORCE_PASSWORD"]})
                   return Promise.resolve(cr)
                })

              return sendAuthToken(p, trx, d.confirmation_token, req, next, eventLogParams)

          })
          .catch(ex=>{
               throw new MError("AUTH_FAILED",null, ex)
          })

      })

  })

  router.post( "/authentication/switch_user",   function (req, res, next) {
        var j = req.body.json

        return vali.async(j, {
           "account_id": {presence: true, isString: {trim: true}},
           "username": {isString: {trim: true}},
           "on_behalf": {isString: {trim: true}},
        })
        .then((d)=>{

          req.params.account_id = d.account_id

          return getAccountTrx(req,res,next,(trx,account)=>{

              var re
              var username = d.username || d.account_id
              if(!d.username) {
                  re = account.GetDefaultCredential()
              }
              else
                  re = account.GetCredentialByUsername(d.username)

              var type = d.on_behalf ? "auth-switch-user" : "superuser-switch-user"

              return sendAuthToken(re, trx, username, req, next, {
                type: type,
                discardTotp: true,
                username: d.username || account.u_primary_email,
                other: "on behalf: "+d.on_behalf
              })

          })



        })
  })

  function getSessionQueryHead(trx) {
            return trx.select("s_id", "s_user_id", "s_credential_id", "s_unique_session_identifier", "s_expires","s_grants") // note s_id here, its needed for update below!
               .table("sessions")
  }

  function getSessionByPrimaryToken(trx, token) {

            return getSessionQueryHead(trx)
               .whereRaw("s_authtoken=? AND s_expires > ?", [ token, moment.now() ])

  }

  function getSessionByDerivedToken(trx, destination_server, tokenDetails) {
      // we expect a derived token to look like (without spaces):
      // unique_session_identifier : derived_token

      if(!destination_server) {
        console.error("destination server was not provided, cant verify derived token")
        return Promise.reject(new MError("AUTH_FAILED"))
      }

      var sessionRow
      return getSessionQueryHead(trx)
         .whereRaw("s_unique_session_identifier=? AND s_expires > ?", [ tokenDetails[1], moment.now() ])
         .then(rows=>{
            sessionRow = Common.EnsureSingleRowReturned(rows, "AUTH_FAILED")

            // now lets lookup the destination server
            return Server(trx).FetchServer(destination_server)

         })
         .then(server=>{


            var expectedDerivedToken = server.CalculateDerivedToken(sessionRow.s_authtoken, tokenDetails[3])
            if(expectedDerivedToken != tokenDetails[2]) {
              console.error("Authentication failed, we expected a different derived token")
              throw new MError("AUTH_FAILED")
            }


            // this is how we indicate towards the relayer where the original request is coming from
            if(tokenDetails[3])
               sessionRow.originatingServer = tokenDetails[3];

            // all good!
            return Promise.resolve([sessionRow])
         })
  }

  router.post( "/authentication/token",   function (req, res, next) {

        var j = req.body.json

        return vali.async(j, {
           "token": {presence: true, isString: {trim:true}},

           // in case of derived tokens, this is the destination server; in case of main tokens it is the source server
           "server": {isString: {lazy: true, trim:true}},
           "auth_code": {isString: {lazy: true, trim:true}},

        })
        .then((data)=>{


            var sessionRow
            var re = {}

            const derivedTokenRegexp = new RegExp("^([^:]+):(.+?)(?::(.+))?$");

            return knex.transaction(function(trx){

                var re = {}
                var derivedParams = derivedTokenRegexp.exec(data.token)
                var derived_mode = derivedParams ? true : false

                var p = (derived_mode ? getSessionByDerivedToken(trx, data.server, derivedParams) : getSessionByPrimaryToken(trx, data.token))
                return p
                 .then(function(rows){

                     sessionRow = Common.EnsureSingleRowReturned(rows, "AUTH_FAILED")

                     sessionRow.s_grants = Common.ForceEnumerableToList(sessionRow.s_grants)


                     re.session = sessionRow

                     // sessions belonging to forgotten passwords dont have a credential id
                     if(sessionRow.s_credential_id) {
                        var cred = new Credential(trx)
                        return cred.GetCredentialById(sessionRow.s_credential_id)
                     }

                 })
                 .then(cred=>{

                     if(cred)
                        sessionRow.s_uuid = cred.c_uuid;

                     // console.log("fucking here, should update session expiration to:", getExpirationDate(), sessionRow)
                     var n = getExpirationDate(cred ? cred.is_primary : true);
                     var x = trx
                       .update({s_expires: n})
                       .into("sessions")
  					           .whereRaw("s_id=?",[sessionRow.s_id])
                       .return() // this one is needed in order to execute the query

                     console.log("updating session, new expiration date", sessionRow, n);
                     sessionRow.s_expires = n;

                     if((cred)&&(cred.c_server)) {
                       // this credential is server bound. we do not share any derived tokens.
                       // console.log("this is a server bound credential")
                       data.dontDerive = true;
                       sessionRow.c_server = cred.c_server;
                     }

                     return x;
                  })
                 .then(()=>{
                    var acc = new Account(trx)
                    return acc.GetAccountById(sessionRow.s_user_id)
                 })
                 .then((account)=>{
                    re.account = account.Cleanup()

                    if(derived_mode) {
                      delete sessionRow.s_authtoken
                      return [];
                    }

                    if(!data.auth_code) {
                      return [];
                    }

                    if(data.dontDerive)
                       return [];

                    return Server(trx).FetchServersWithAuthCode()
                 })
                 .then(servers=>{

                    // console.log("are here!", data, servers);

                    re.derived_server_tokens = {};
                    if(!derived_mode) {
                       var gotit = false;
                       servers.some(server=>{
                          if(server.auth_code === data.auth_code) {
                             gotit = true;
                             return true;
                          }
                       })

                       if(!gotit) {
                         console.error("Invalid auth code received (", data.auth_code,"), not sending any derived tokens");
                         servers = [];
                       }
                    }

                    var source_server = data.server;
                    servers.forEach(server=>{
                       var derived_token = server.CalculateDerivedToken(sessionRow.s_authtoken, source_server);
                       // console.log("hey", sessionRow)
                       re.derived_server_tokens[server.s_name] = sessionRow.s_unique_session_identifier + ":" + derived_token
                       if(source_server)
                          re.derived_server_tokens[server.s_name] += ":"+source_server
                    })


                    return re.account.getEverythingForAccount()
                 })
                 .then((belongings)=>{

                    for(var q of Object.keys(belongings)) {
                       re[q] = belongings[q]
                    }

                    re.webhostings.forEach(x=>{
                       // these two are just not needed
                       delete x.w_id;
                       delete x.w_user_id;
                    });

                    re.derived_mode = derived_mode;

                    // we hide these ones here
                    delete re.session.s_id
                    delete re.session.s_user_id
                    delete re.session.s_credential_id
                    delete re.session.s_unique_session_identifier

                    return Promise.resolve(re)
                 })
            })
         })
        .then(re=>{
            req.result = re
            next()
        })

  })

  router.post("/accounts/by-role/:role", function(req,res,next){
      return Credential(knex).GetAccountsByRole(req.params.role, req.body.json).then(accounts=>{
         return req.sendResponse({accounts: accounts});
      })
  });


  router.post("/accounts/lookup/id", function(req,res,next){
          return vali.async(req.body.json, {
             "ids": {presence: true, isArray: true},
          })
          .then(function(d){
             questionMarks = d.ids.map(x => { return "?" }).join(",")
             // console.log("qmarks", questionMarks)
             return knex("users").select("u_id","u_name").whereRaw("u_id IN ("+questionMarks+")", d.ids)
          })
          .then(m=>{
              var re = {}
              m.forEach(row=>{
                 re[row.u_id] = row.u_name
              })
              req.result = re
              next()
          })
  })


  router.post("/accounts/registration", function(req,res,next){
      var j = req.body.json
      j["password_type"] = "clear"
      j["e_confirmed"] = false
      j["u_comment"] = ""
      delete j["created_at"]
      var uid
      var acc = new Account(knex)
       return acc.CreateAccount(j, req.query.ts, app.config.get("telephone_required"), app.config)
       .then((uid) => {
            // transaction finished, everything succeeded.

            app.InsertEvent(req, {e_event_type: "registration", e_other: {"u_name": j.u_name}, e_username: j.e_email, e_user_id: uid})

            req.params.account_id = uid
            return expressEmailCommon(EMAIL_SHOULD_NOT_BE_VERIFIED, function(trx,account,email, req){

               return email.SendRegistration(app, req)
                 .catch(ex=>{
                    console.error("Unable to send email", ex);
                    throw new MError("EMAIL_SENDING_FAILED");
                 })
                 .then(function(){
                    req.result = {"u_id": uid}
                    next()
                 })
            })(req,res,next)

       })

  })

  function typicalAccountTailer(prom, req, res, next){
     return prom.then(()=>{
               if(!req.result) {
                  req.result = "ok"
                  next()
               }
           })
  }


  function getAccount(aKnex, req,res,next,accountCallback, skipTrailer){
        var acc = new Account(aKnex, app)
        var account

        var prom = acc.GetAccountById(req.params.account_id)
           .then(aAccount=>{
               return accountCallback(aAccount, req,res,next)
           })

        return skipTrailer ? prom : typicalAccountTailer(prom, req, res, next);

  }
  function expressGetAccount(callback) {
     return function(req,res,next) {

        return getAccount(knex, req, res, next, callback)

     }
  }
  function expressGetAccountAndWebhosting(callback){
    return expressGetAccount(function(account, req,res,next){
       return account.GetWebhostings(req.params.server_name, req.params.webhosting_id)
          .then(function(domains) {
            var webhosting = Common.EnsureSingleRowReturned(domains, "WEBHOSTING_NOT_FOUND")

            return callback(account, webhosting, req, res, next)
          })

    })
  }

  function expressGetAccountAndDomain(callback){
    return expressGetAccount(function(account, req,res,next){
       return account.GetDomains(null, req.params.domain_name)
          .then(function(domains) {
            var domain = Common.EnsureSingleRowReturned(domains, "DOMAIN_NOT_FOUND")

            return callback(account, domain, req, res, next)
          })

    })
  }
  function getAccountTrx(req,res,next,trxAccountCallback){
        return typicalAccountTailer(knex.transaction(function(trx){

            return getAccount(trx, req, res, next, (account)=>{
               return trxAccountCallback(trx, account, req,res, next)
            }, true)
        }), req, res, next);
  }
  function expressGetAccountTrx(trxAccountCallback){

    return function(req,res,next) {
         if(req.result) return next()

         return getAccountTrx(req,res,next, trxAccountCallback)
    }
  }

  router.delete("/account/:account_id/account_grant",expressGetAccountTrx(function(trx, mainAccount, req,res,next) {
           return mainAccount.SetGrant({"u_id": ""}) //removing
           .then((r)=>{
              app.InsertEvent(req, {e_event_type: "account-grant-cleared", e_other: true});
              return r;
           })
  }))
  router.post("/account/:account_id/account_grant",expressGetAccountTrx(function(trx, mainAccount, req,res,next) {

      return vali.async(req.body.json, {
        "grant_access_for": {presence: true, isString: true}
      })
      .then((a_data)=>{

        var acc = new Account(trx)
        a_data.email = a_data.grant_access_for
        return acc.LookupAccountByPrimaryEmail(a_data)
      })
      .then(accountGrantFor=>{

          if(mainAccount.u_id == accountGrantFor.u_id)
             throw new MError("ACCOUNTS_CANT_BE_THE_SAME")


           return mainAccount.SetGrant(accountGrantFor)


      })
     .then((r)=>{
        app.InsertEvent(req, {e_event_type: "account-grant-set", e_other: true});
        return r;
     })

  }))

  router.get("/account/:account_id/account_grants/for",expressGetAccount(function(account, req,res,next){
     // list accounts what this account is granted access for
     return account.GetAccountsThisHasAccessFor()
       .then((rows)=>{
           req.result = rows.Cleanup()
           next()
       })
  }))
  router.get("/account/:account_id/account_grants/to",expressGetAccount(function(account, req,res,next){
     // list accounts for which access is granted to this account
     return account.GetAccountsHavingAccessToThis()
       .then((rows)=>{
           req.result = rows.Cleanup()
           next()
       })
  }))

  router.route("/account/:account_id/domain/:domain_name")
    .get(expressGetAccountAndDomain(function(account, domain, req,res,next){
        req.result = domain.Cleanup()
        next()
    }))
    .delete(expressGetAccountAndDomain(function(account, domain, req,res,next){
        return domain.Delete()
          .then((affected_rows)=>{
             req.result = { "deleted": affected_rows }

             app.InsertEvent(req, {e_event_type: "account-domain-access-revoked", e_other: true});

             next()
          })

    }))

  router.use("/config", require("account-config-router.js")(app))


  app.get_callback_request_email=function (){
    return  app.config.get("callback_request_email_address")
  }

  router.post("/account/:account_id/callback_request", expressGetAccount(function(account,req,res,next){
     var email_address = app.get_callback_request_email()
     if(!email_address)
      throw new MError("CALLBACK_REQUEST_NOT_SUPPORTED")

      var p = vali.async(req.body.json, {
         "message": {isString: {trim:true}},
         "shopping_cart": {isString: {trim:true}},
      })
      .then((data)=>{
         var mailer = app.GetMailer()

         app.InsertEvent(req, {e_event_type: "callback-request", e_other: true});

         var x = {}
         x.to = email_address
         x.template = "account/callback"
         x.locale = account.u_language
         data.user = account
         x.context = data

         return mailer.SendMailAsync(x)

      })

      return p

  }))

  // this is the replacement of the original toAccount based mailing approach
  router.post("/account/:account_id/sendmail/:email_category?", expressGetAccount(function(account,req,res,next){
     var p;
     if(req.params.email_category)
        p = account.FetchEmails(null, req.params.email_category)
     else
        p = Promise.resolve();

     return p.then(function(emails){

       var mailer = app.GetMailer();
       app.InsertEvent(req, {e_event_type: "email-send-direct", e_other: true});

       var x = req.body.json;
       x.locale = account.u_language;
       x.to = emails ? emails.map((e)=>{return e.e_email}).join(", ") : account.u_primary_email;

       // otherwise a compromised node in the plant could still learn the email addresses of our users
       delete x.cc;
       delete x.bcc;

       x.context.account = account.Cleanup();

       return mailer.SendMailAsync(x);

     })
     .then(()=>{
        return req.sendOk();
     })


  }))


  router.route("/account/:account_id/webhosting/:server_name/:webhosting_id")
    .get(expressGetAccountAndWebhosting(function(account, webhosting, req,res,next){
        req.result = webhosting.Cleanup()
        next()
    }))
    .delete(expressGetAccountAndWebhosting(function(account, webhosting, req,res,next){
        return webhosting.Delete()
          .then((affected_rows)=>{
             app.InsertEvent(req, {e_event_type: "account-webhosting-access-revoked", e_other: true});

             req.result = { "deleted": affected_rows }
             next()
          })

    }))

  router.route("/account/:account_id/webhostings")
     .get(expressGetAccount(function(account, req,res,next){

        return account.GetWebhostings()
          .then(function(webhostings) {
            req.result = webhostings.Cleanup()
            next()
          })
     }))
     .put(expressGetAccountTrx(function(trx, account, req,res,next) {

             return req.sendOk(
                account.AddWebhosting(req.body.json)
                .then(()=>{
                  app.InsertEvent(req, {e_event_type: "account-webhosting-access-revoked", e_other: true});

                })
             )

        }))

   function getTxtRecordForDomain(account_id, d) {
      var n = {
         account_id: account_id,
         bareDomain: app.getBareDomain(d.wd_domain_name),
         server: d.wd_server,
         webhosting_id: d.wd_webhosting
      }
      var crypto = require('crypto')
        , text = JSON.stringify(n)
        , key = txtRecordToken
        , hash

      n.code = crypto.createHmac('sha256', key).update(text).digest('hex');
      return n;
   }



   router.route("/account/:account_id/webhostingdomains/txt-record-code/")
     .post(function(req,res,next){
        return vali.async(req.body.json, webhostingDomainConstraint)
        .then(function(d){
            return req.sendResponse(getTxtRecordForDomain(req.params.account_id, d))
        })
     })

   // requests to this route are sent by the end-user, a certain level of authorization takes place here
   router.route("/account/:account_id/webhostingdomains/")
     .put(function(req,res,next){
         /*
           Options:
           - The caller has permission for that domain already (present among his/her accountapi domain entries)
           - It is a subdomain of an entry he/she already has access to (present among his/her accountapi domain entries)
           - It is a new external domain that is not yet present inside the system; authorized by the TXT record
           - External domain that is not yet present inside the system - and adding them without TXT authorization is allowed
         */

         req.body.json.wd_domain_name = req.body.json.wd_domain_name.toLowerCase();
         var r = new RegExp('^(?:www.)+(.+)');
         var m = r.exec(req.body.json.wd_domain_name);
         if((m)&&(m[1]))
            req.body.json.wd_domain_name = m[1];


         var bareDomain;
         var d;
         var wh;
         var acc;
         return vali.async(req.body.json, webhostingDomainConstraint)
         .then(ad=>{
            d = ad;

            bareDomain = app.getBareDomain(d.wd_domain_name);

            return app.MonsterInfoWebhosting.GetInfo({server: d.wd_server, storage_id: d.wd_webhosting})
         })
         .then(re=>{
            wh = re;

            if(wh.wh_user_id != req.params.account_id)
              throw new MError("PERMISSION_DENIED");


            var acLib = new Account(knex, app)
            return acLib.GetAccountById(req.params.account_id)
         })
         .then(aacc=>{
            acc = aacc;
            return acc.GetWebhostings(d.wd_server, d.wd_webhosting);          
         })
         .then(whs=>{
            if(whs.length != 1) 
               throw new MError("WEBSTORE_NOT_AUTHORIZED");

            var domain = getWebhostingDomains();
            return domain.GetByWebhosting(d);
         })
         .then(re=>{  // [ { wd_domain_canonical_name: xx, wd_domain_name: yyy, wd_server: 'sdomain', wd_webhosting: 1234 } ]
            var webstoreDomainsAttached = re;

            var presentAlready = false;
            webstoreDomainsAttached.some(wd=>{
               if((wd.wd_domain_canonical_name == d.wd_domain_name)&&(wd.wd_server == d.wd_server)&&(wd.wd_webhosting == d.wd_webhosting)) {
                  presentAlready = true;
                  return true;
               }
            })

            if(presentAlready)
              return;

            var domainsAttachedCount = webstoreDomainsAttached.length;
            if(wh.template.t_domains_max_number_attachable <= domainsAttachedCount)
              throw new MError("WEBSTORE_DOMAIN_QUOTA_EXCEEDED");


            return acc.GetDomains()
             .then(domains=>{ // [{d_domain_canonical_name: "yy", d_domain_canonical_name: "xx"}]
                var exactMatch = false;
                var isSubdomain = false;
                domains.some(dom=>{
                    if(dom.d_domain_canonical_name == d.wd_domain_name)
                    {
                       exactMatch = true;
                       return true;
                    }

                    var subdomainSuffix = "."+dom.d_domain_canonical_name;
                    if(d.wd_domain_name.endsWith(subdomainSuffix)) {
                       isSubdomain = true;
                       return true;
                    }
                });

                if(exactMatch) return;
                if(isSubdomain) return;

                // txt record needs to be added. lets see first whether this is a pure hostname or a real domain
                var dots = (d.wd_domain_name.match(/\./g) || []).length;
                if(!dots) throw new MError("HOSTNAME_TXT_NOT_SUPPORTED");

                const domainsLib = require("models/domain.js");
                var domains = domainsLib(knex, app);
                var p = app.config.get("throw_when_domain_is_present_for_others") ?
                          domains.ThrowWhenBareDomainIsPresentForOthers(req.params.account_id, d.wd_domain_name) :
                          Promise.resolve();
                return p
                .then(()=>{
                    if(webhosting_domains_can_be_added_without_txt_records) return;

                    // txt record should be present to authorize this request
                    var expectedDnsRecordStruct = getTxtRecordForDomain(req.params.account_id, d);
                    return app.dnsResolveAsync(d.wd_domain_name, "TXT")
                      .catch(err=>{
                         console.error("error while resolving", d.wd_domain_name, err);
                         throw new MError("TXT_RECORD_NOT_FOUND")
                      })
                      .then(records=>{
                          console.log("RECORDS", records); // RECORDS [ [ 'v=spf1 a ip4:88.151.102.104/32 ip4:212.47.236.1 ip4:94.130.37.181 ~all' ] ]
                          records = flatten(records);
                          var found = false;
                          records.some(record=>{
                             if(record == expectedDnsRecordStruct.code){
                                found = true;
                                return true;
                             }
                          })

                          if(!found)
                            throw new MError("TXT_RECORD_NOT_FOUND");
                      })

                })


             })
             .then(()=>{
                // the request has been authorized, now we need to insert it (both into webhostingdomains and domains)

                return knex.transaction(function(trx){
                    var domain = getWebhostingDomains(trx);
                    d.domain = d.wd_domain_name;
                    return domain.Insert(d)
                      .then(()=>{
                          var acc = new Account(trx, app)
                          return acc.GetAccountById(req.params.account_id)
                            .then(a=>{
                               return a.AddDomain({domain:d.wd_domain_name})
                            })

                      })
                })

             })

         })
         .then(()=>{
            return req.sendResponse({
              domain: d.wd_domain_name, 
              bareDomain: bareDomain
            });
         })

     })

     .delete(expressGetAccount(function(account, req,res,next){
        /*
        Removal is only possible when there are no more docroot/email entries present. This is enforced at the relayer layer.
        This is to prevent abuse by users (adding a domain, configuring it and then replacing the domain with others - exceeding the quota)        
        */

        // here we authorize this request just by simple checking if the user has permission for the specified domain
        return vali.async(req.body.json, webhostingDomainConstraint)
         .then(d=>{
           return account.GetDomains()
              .then(domains=>{
                    var userMatch = false;
                    domains.some(dom=>{
                        if(dom.d_domain_canonical_name == d.wd_domain_name)
                        {
                           userMatch = true;
                           return true;
                        }

                    })

                    if(userMatch) return;

                    // lets also check the webhostindomains
                    return account.GetWebhostings()
                      .then(wds=>{
                          var wdMatch = false;
                          wds.some((wd)=>{
                              if((d.wd_server == wd.w_server_name)&&(d.wd_webhosting == wd.w_webhosting_id)) {
                                 wdMatch = true;
                                 return true;
                              }
                          });

                          if(wdMatch) return;

                          throw new MError("PERMISSION_DENIED");
                      })
              })
              .then(()=>{
                 // authorized. Lets delete the record.
                 var wd = getWebhostingDomains();
                 d.domain = d.wd_domain_name;
                 return wd.GetWebhostingDomain(d);
              })
              .then((e)=>{
                 return e.Delete();
              })
              .then(()=>{
                 return req.sendOk();
              })
        })


     }))
     .get(expressGetAccount(function(account, req,res,next){

        return req.sendPromResultAsIs(
          account.GetWebhostingDomains()
          .then(function(domains) {
            return domains.Cleanup()
          }))
     }));

  router.route("/webhostingdomains/mass")
    .put(function(req,res,next){
        return req.sendOk(
          knex.transaction(function(trx){
             var domain = getWebhostingDomains(trx);

             return dotq.linearMap({array: req.body.json, action: function(p){
                return domain.Insert(p);
             }})

          })
        );
      
    })


   router.route("/webhostingdomains/move")
     .post(function(req,res,next){
        var constraints =  { dest_wh_id: {presence: true, isInteger: {lazy:true}}};

        var d;
        return vali.async(req.body.json, constraints)
        .then(function(ad){
            d = ad;

            var domain = getWebhostingDomains();
            return domain.GetWebhostingDomain(req.body.json);
        })
        .then(function(wd){

            return wd.Move(d);

        })
        .then(function(){
           return req.sendOk();
        })
     })


  router.route("/webhostingdomains/by-wh")
    .post(function(req,res,next){
        var domain = getWebhostingDomains();
        return req.sendPromResultAsIs(
            domain.GetByWebhosting(req.body.json)
            .then(rows=>{
               return rows.Cleanup();
            })
        );
    })
    .delete(function(req,res,next){
        var domain = getWebhostingDomains();
        return req.sendOk(
            domain.DeleteByWebhosting(req.body.json)
        );
    })

  router.route("/webhostingdomains")
    .get(function(req,res,next){
        var domain = getWebhostingDomains();
        return req.sendPromResultAsIs(
            domain.GetAll()
            .then(rows=>{
               return rows.Cleanup();
            })
        );
    })
    .put(function(req,res,next){
        var domain = getWebhostingDomains();
        return req.sendOk(
            domain.Insert(req.body.json)
        )
      
    })
    .delete(function(req,res,next){
        var domain = getWebhostingDomains();
        return req.sendOk(
            domain.GetWebhostingDomain(req.body.json)
              .then(wd=>{
                 return wd.Delete();
              })
        )
    })


     function getWebhostingDomains(aknex){
        if(!aknex) aknex = knex;
        var WebhostingDomain=  require("models/webhostingdomain.js")
        var domain = new WebhostingDomain(aknex);
        return domain;
     }

  // this is about the permissions
  router.route("/account/:account_id/domains")
     .get(expressGetAccount(function(account, req,res,next){

        return account.GetDomains()
          .then(function(domains) {
            req.result = domains.Cleanup()
            next()
          })

     }))

     .put(expressGetAccountTrx(function(trx, account, req,res,next) {

             return req.sendPromResultAsIs(
                account.AddDomain(req.body.json)
               .then((domain)=>{
                  app.InsertEvent(req, {e_event_type: "account-domain-access-granted", e_other: true});

                  return domain.Cleanup()
               })
              )

        }))


  router.post("/account/:account_id/email/confirmation/verify",
      expressGetAccount(function(account, req,res,next){

          return account.GetPrimaryEmail()
             .then(email=>{
                 return emailCommon(knex, account, email, req, res, next, EMAIL_SHOULD_NOT_BE_VERIFIED, emailVerifyConfirmation)
             })
             .then(r=>{
                  app.InsertEvent(req, {e_event_type: "email-confirmation-verify", e_other: true});
                  return req.sendOk();
             })

      }))


  router.post("/account/:account_id/email/confirmation/send",
      expressGetAccount(function(account, req,res,next){
         return account.GetPrimaryEmail()
           .then(email=>{

               return emailCommon(knex, account, email, req, res, next, EMAIL_SHOULD_NOT_BE_VERIFIED, emailSendConfirmation)

           })
           .then(r=>{
                app.InsertEvent(req, {e_event_type: "email-confirmation-send", e_other: true});
                return r;
           })


      }))




  function expressListAccountEmails(){
    return expressGetAccount(function(account, req,res,next) {
         return account.FetchEmails(null, req.params.email_category)
           .then(emails=>{
             req.result = emails.Cleanup()
             next()
           })
     })
  }

  router.post("/emails/set-escalate", function(req,res,next){
     return req.sendOk(Email(app.knex).SetEscalate(req.body.json));
  })
  router.post("/emails/search", function(req,res,next) {
        return Email(app.knex)
          .ListBy(req.body.json)
          .then((emails)=>{
              return req.sendResponse(emails.Cleanup())
          })
     })


  router.route("/account/:account_id/emails/:email_category")
     .get(expressListAccountEmails())

  router.route("/account/:account_id/emails")
     .put(expressGetAccountTrx(function(trx, account, req,res,next) {
             return account.AddEmail(req.body.json)
               .then(r=>{
                    app.InsertEvent(req, {e_event_type: "email-insert", e_other: true});
                    return r;
               })

     }))
     .get(expressListAccountEmails())


  router.post("/account/:account_id/tel/:tel/send", expressTelCommonNoTrx(function(account,tel, req){
     return tel.SendCode(req.body.json, app, req)

  }))
  router.post("/account/:account_id/tel/:tel/verify", expressTelCommon( function(trx,account,tel, req){
     return tel.Verify(req.body.json, app, req)

  }))
  router.post("/account/:account_id/tel/:tel/set_primary", expressTelCommon( function(trx,account,tel, req){
     return account.SetPrimaryTel(tel, app.config)
         .then(r=>{
              app.InsertEvent(req, {e_event_type: "tel-primary-changed", e_other: true});
              return r;
         })

  }))

  router.route("/account/:account_id/tel/:tel")
    .get(expressTelCommon(function(trx,account,tel, req,res,next){
       req.result = tel.Cleanup()
       next()
    }))
    .delete(expressTelCommon(function(trx,account,tel, req){
       return tel.Delete()
         .then(r=>{
              app.InsertEvent(req, {e_event_type: "tel-removed", e_other: true});
              return r;
         })

    }))


  router.post("/tels/set-escalate", function(req,res,next){
     return req.sendOk(Tel(app.knex).SetEscalate(req.body.json));
  })
  router.post("/tels/search", function(req,res,next) {
        return Tel(app.knex)
          .ListBy(req.body.json)
          .then((tels)=>{
              return req.sendResponse(tels.Cleanup())
          })
  });

  router.route("/account/:account_id/tels")
     .put(expressGetAccountTrx(function(trx, account, req,res,next) {
             return account.AddTel(req.body.json)
               .then(r=>{
                    app.InsertEvent(req, {e_event_type: "tel-insert", e_other: true});
                    return r;
               })

     }))
     .get(expressGetAccount(function(account, req,res,next) {
         return account.FetchTels()
           .then(tels=>{
             req.result = tels.Cleanup()
             next()
           })
     }))


  function expressGetAccountCredTrx(callback){
     return expressGetAccountTrx(function(trx, account, req,res,next) {
          return account.GetCredentialByUsername(req.params.cred_user)
            .then(cred =>{
                return callback(trx, account, cred, req, res, next)
            })
     })
  }

  function expressGetAccountCred(callback){
     return expressGetAccount(function(account, req,res,next) {
          return account.GetCredentialByUsername(req.params.cred_user)
            .then(cred =>{
                return callback(account, cred, req, res, next)
            })
     })
  }
  function expressGetAccountCredUuid(callback){
     return expressGetAccount(function(account, req,res,next) {
          return account.GetCredentialByUuid(req.params.uuid||req.body.json.c_uuid)
            .then(cred =>{
                return callback(account, cred, req, res, next)
            })
     })
  }


  router.post("/account/:account_id/credential/:cred_user/password",expressGetAccountCredTrx(function(trx, account, cred, req,res,next) {
        var data
        return vali.async(req.body.json, {
           "new_password": Common.passwordEnforcements
        })
        .then((data)=>{
           return cred.ChangePassword(data["new_password"])
        })
        .then(()=>{
            app.InsertEvent(req, {e_event_type: "grants-update", e_other: req.body.json, e_username: req.params.cred_user, e_user_id: account.u_id})
         })
   }))

  router.post("/account/:account_id/totp/status", expressGetAccount(function(account, req,res,next) {
      return Promise.resolve()
        .then(()=>{
            if(!account.u_totp_secret) return account.RegenerateTotpToken();
        })
        .then(()=>{
            var d = {
              u_totp_active: account.u_totp_active,
            };
            if(!account.u_totp_active) {
              var name = config.get("service_name").replace(" ", "");
              d.u_totp_secret = account.u_totp_secret;
              d.u_totp_url = `otpauth://totp/${name}?secret=${account.u_totp_secret}`;
            }

            return req.sendResponse(d);
        })
  }));
  router.post("/account/:account_id/totp/activate", expressGetAccount(function(account, req,res,next) {
      if(account.u_totp_active) {
         throw new MError("ALREADY_ACTIVATED");
      }

      return account.ThrowIfInvalidToken(req.body.json.userToken, false)
        .then(()=>{

            // all good.
            return account.ActivateTotp();          
        })
        .then(()=>{
           app.InsertEvent(req, {e_event_type: "totp-activate", e_other: req.body.json, e_user_id: account.u_id})
        })

  }));
  router.post("/account/:account_id/totp/inactivate", expressGetAccount(function(account, req,res,next) {

      if(!account.u_totp_active) {
         throw new MError("NOT_ACTIVATED");
      }

      return account.ThrowIfInvalidToken(req.body.json.userToken, true)
        .then(()=>{

            // all good.
            return account.InactivateTotp();          
        })
        .then(()=>{
           app.InsertEvent(req, {e_event_type: "totp-deactivate", e_other: req.body.json, e_user_id: account.u_id})
        })

  }));
  router.post("/account/:account_id/totp/force-off", expressGetAccount(function(account, req,res,next) {

       return account.InactivateTotp()
         .then(()=>{
            app.InsertEvent(req, {e_event_type: "totp-force-off", e_user_id: account.u_id})
         })

  }));

  router.route("/account/:account_id/credential/:cred_user")
     .delete(expressGetAccountCredTrx(function(trx, account, cred, req,res,next) {
          return cred.Delete().then(()=>{
            app.InsertEvent(req, {e_event_type: "grants-delete", e_other: req.body.json, e_username: req.params.cred_user, e_user_id: account.u_id})
          })
      }))
     .post(expressGetAccountCredTrx(function(trx, account, cred, req,res,next) {
          return cred.UpdateGrants(req.body.json)
          .then(()=>{
            app.InsertEvent(req, {e_event_type: "grants-update", e_other: req.body.json, e_username: req.params.cred_user, e_user_id: account.u_id})
          })
      }))
     .get(expressGetAccountCred(function(account, cred, req,res,next) {
          req.result = cred.Cleanup()
          next()
      }))
  router.route("/account/:account_id/credential-uuid/:uuid")
     .get(expressGetAccountCredUuid(function(account, cred, req,res,next) {
          req.result = cred.Cleanup()
          next()
      }))
  router.route("/account/:account_id/credential-uuid/")
     .post(expressGetAccountCredUuid(function(account, cred, req,res,next) {
          req.result = cred.Cleanup()
          next()
      }))

  router.route("/account/:account_id/credentials")
     .put(expressGetAccountTrx(function(trx, account, req,res,next) {
        // password validation takes place in .AddCredential
        return Token.GetPseudoTokenAsync(req.body.json.c_password, req.body.json.c_password, req.query.ts, 8, "hex")
          .then(username=>{
             return account.AddCredential(username, req.body.json)
               .then((username)=>{
                  app.InsertEvent(req, {e_event_type: "grants-add", e_other: req.body.json, e_username: req.params.cred_user, e_user_id: account.u_id})

                  req.result = {"username": username}
                  next()
               })
          })
     }))
     .get(expressGetAccount(function(account, req,res,next) {
         return account.GetCredentials()
           .then((creds)=>{
               req.result = creds.Cleanup()
               next()
           })
     }))


  router.post("/account/:account_id/force_password",
    expressGetAccount(function(account, req,res,next) {

        var credential
        var data
        return vali.async(req.body.json, {
           "new_password": Common.passwordEnforcements
        })
        .then((aData)=>{
           data = aData

           return account.GetDefaultCredential()
        })
        .then((aCredential)=>{
           credential = aCredential

           return credential.ChangePassword(data["new_password"])
        })
         .then(r=>{
              app.InsertEvent(req, {e_event_type: "force-password"});
              return r;
         })

    }))

  router.post("/account/:account_id/change_password",
    expressGetAccount(function(account, req,res,next) {

        var credential
        var data
        return vali.async(req.body.json, {
           "old_password": {presence: true, isString: {trim:true}},
           "new_password": Common.passwordEnforcements
        })
        .then((aData)=>{
           data = aData

           return account.GetDefaultCredential()
        })
        .then((aCredential)=>{
           credential = aCredential

           return credential.Authenticate(data["old_password"])
        })
        .then(()=>{
           return credential.ChangePassword(data["new_password"])
        })
         .then(r=>{
              app.InsertEvent(req, {e_event_type: "change-password"});
              return r;
         })

    }))


  router.route("/account/:account_id")
    .get(expressGetAccount(function(account, req,res,next) {
           // we do not call cleanup here on purpose
           // any sensitive data should be hidden by the BL layer
           req.result = account
           next()

    }))
    .post(expressGetAccountTrx(function(trx, account, req,res,next) {
       return account.ChangeDetails(req.body.json)
         .then(r=>{
              console.log("Account change details succeeded");
              app.InsertEvent(req, {e_event_type: "account-change"});
              return r;
         })

     }))
    .delete(expressGetAccountTrx(function(trx, account, req,res,next) {
        return account.Delete()

    }))



  function expressTelCommon(telCallback) {
     return expressGetAccountTrx((trx, account, req, res, next)=>{

        return account.FetchTels(req.params.tel)
          .then(tels=>{

              var tel = Common.EnsureSingleRowReturned(tels, "TEL_NOT_FOUND")
              return telCallback(trx, account, tel, req, res, next)

          })

     })

  }

  function expressTelCommonNoTrx(telCallback) {
     return expressGetAccount((account, req, res, next)=>{

        return account.FetchTels(req.params.tel)
          .then(tels=>{

              var tel = Common.EnsureSingleRowReturned(tels, "TEL_NOT_FOUND")
              return telCallback(account, tel, req, res, next)

          })

     })

  }



  function emailCommon(trx, account, email, req, res, next, verifiedState, emailCallback) {
      if((verifiedState == EMAIL_SHOULD_BE_VERIFIED)&&(!email.e_confirmed)) throw new MError("ACCOUNT_EMAIL_NOT_VERIFIED")
      if((verifiedState == EMAIL_SHOULD_NOT_BE_VERIFIED)&&(email.e_confirmed)) throw new MError("ACCOUNT_EMAIL_ALREADY_VERIFIED")

      return emailCallback(trx, account, email, req, res, next)

  }

  function expressEmailCommonViaTransaction(verifiedState, emailCallback) {
     // dont use this one, it locks the database for too long
     return expressGetAccountTrx((trx, account, req, res, next)=>{

        return account.FetchEmails(req.params.email)
          .then(emails=>{

              var email = Common.EnsureSingleRowReturned(emails, "EMAIL_NOT_FOUND")
              return emailCommon(trx, account, email, req, res, next, verifiedState, emailCallback)

          })

     })

  }

  function expressEmailCommon(verifiedState, emailCallback) {
    // these methods invoke synchronous email sending, for this reason we cannot hold a database lock here
     return expressGetAccount((account, req, res, next)=>{

        return account.FetchEmails(req.params.email)
          .then(emails=>{

              var email = Common.EnsureSingleRowReturned(emails, "EMAIL_NOT_FOUND")
              return emailCommon(knex, account, email, req, res, next, verifiedState, emailCallback)

          })

     })

  }

  function emailSendConfirmation(trx,account,email, req){
     return email.SendVerification(app, req)
  }
  function emailVerifyConfirmation(trx,account,email, req){
     return email.Verify(req.body.json, app, req)
  }


  router.post("/account/:account_id/email/:email/categories", expressEmailCommon(EMAIL_NOT_CARE, function(trx,account,email, req){
     return email.SetCategories(req.body.json)
  }))

  router.post("/account/:account_id/email/:email/send/registration", expressEmailCommon(EMAIL_SHOULD_NOT_BE_VERIFIED, function(trx,account,email, req){
     return email.SendRegistration(app, req)
  }))

  router.post("/account/:account_id/email/:email/send/confirmation", expressEmailCommon(EMAIL_SHOULD_NOT_BE_VERIFIED, emailSendConfirmation))

  router.post("/account/:account_id/email/:email/verify", expressEmailCommon(EMAIL_SHOULD_NOT_BE_VERIFIED, emailVerifyConfirmation))


  router.post("/account/:account_id/email/:email/set_primary", expressEmailCommon(EMAIL_SHOULD_BE_VERIFIED, function(trx,account,email, req){
     return account.SetPrimaryEmail(email, app, req)
         .then(r=>{
              app.InsertEvent(req, {e_event_type: "email-primary-changed"});
              return r;
         })
  }))

  router.delete("/account/:account_id/email/:email", expressEmailCommon(EMAIL_NOT_CARE, function(trx,account,email, req){
     return email.Delete()
         .then(r=>{
              app.InsertEvent(req, {e_event_type: "email-removed"});
              return r;
         })

  }))



  router.post("/accounts/send_forgotten_password", function(req,res,next){

      var acc = new Account(knex)
      return req.sendOk(
        acc.LookupAccountByPrimaryEmail(req.body.json)
        .then(account=>{
            return account.GetPrimaryEmail()
        })
        .then(email=>{

            return email.SendForgotten(app, req)
        })
         .then(()=>{
              app.InsertEvent(req, {e_event_type: "forgotten-password-email-send"});
         })

      );

  })




  router.route("/accounts")
    .get(function (req, res, next) {
        return knex.select().table('users')
          .then(function(rows) {
            req.result = rows
            next()
          })

    })
    .put(function (req, res, next) {

       var j = req.body.json
       var acc = new Account(knex)
       return acc.CreateAccount(j, req.query.ts, app.config.get("tel_should_be_confirmed"), app.config)
       .then((uid) => {
            // transaction finished, everything succeeded

            req.sendResponse({"u_id": uid});

            app.InsertEvent(req, {e_event_type: "account-insert"});
       })


    })


  // note this is done here with regexp, since the token itself might contain / character (base64)
  router.delete(/^\/token\/(.+)/,function (req, res, next) {
       return knex("sessions").whereRaw("s_authtoken=?", [req.params[0]]).del()
          .then((affected_rows)=>{
             req.sendResponse({ "deleted": affected_rows });
             app.InsertEvent(req, {e_event_type: "token-deleted"});
          })

  })

  router.route("/email/categories")
    .get(function (req, res, next) {
        var  email = new Email()
        req.result = email.GetEmailCategories()
        next()

    })


  router.route("/tel/country_codes")
    .get(function (req, res, next) {
        var  tel = new Tel()
        req.result = tel.GetAcceptedCountryCodes()
        next()

    })



function getServerExpress(cb){
   return function(req,res,next){
        return Server(knex).FetchServer(req.params.server)
            .then(server=>{
                return cb(server, req, res, next)
            })
            .then(()=>{
                if(!req.result)
                      req.result = "ok"
                      next()
            })

   }
}

  router.route("/server/:server/roles/:role")
     .delete(getServerExpress((server, req)=>{
          return server.RemoveRole(req.params.role);
     }))

  router.route("/server/:server")
     .delete(getServerExpress((server)=>{
          return server.Delete()
     }))
     .post(getServerExpress((server, req)=>{
          return server.Change(req.body.json)
            .then(r=>{
               app.InsertEvent(req, {e_event_type: "server-changed"});
               return r;
            })
     }))
     .get(function(req,res,next){

        return Server(knex).FetchServer(req.params.server)
            .then((row)=>{
                req.result = row.Cleanup()
                next()
            })


     })
  router.route("/servers/full")
     .get(function(req,res,next){

        return req.sendPromResultAsIs(Server(knex).FetchServersWithAuthCode());

     })

  router.route("/servers")
     .get(function(req,res,next){

        return Server(knex).FetchServers()
            .then((rows)=>{
                req.sendResponse(rows.Cleanup());
            })


     })
     .put(function(req,res,next){

        return req.sendPromResultAsIs(
          Server(knex).Insert(req.body.json, req.query.ts)
            .then(r=>{
               app.InsertEvent(req, {e_event_type: "server-insert"});
               return r;
            })
        );
     })


  router.route("/webhostingtemplate/:template")
    .get(function(req,res,next){
        return WebhostingTemplates(knex).GetWebhostingTemplateByName(req.params.template)
            .then((template)=>{
                req.result = template.Cleanup()
                next()
            })
    })
    .post(function(req,res,next){
        return req.sendOk(
          WebhostingTemplates(knex).GetWebhostingTemplateByName(req.params.template)
            .then((template)=>{
                return template.Change(req.body.json)
            })
            .then(()=>{
               app.InsertEvent(req, {e_event_type: "webhosting-template-change"});
            })
        );
    })
    .delete(function(req,res,next){

        return req.sendOk(
          WebhostingTemplates(knex).GetWebhostingTemplateByName(req.params.template)
            .then((template)=>{
                return template.Delete()
            })
            .then(()=>{
               app.InsertEvent(req, {e_event_type: "webhosting-template-removed"});
            })

        );

  })
  router.route("/webhostingtemplates")
    .search(function(req,res,next){
        return WebhostingTemplates(knex).GetWebhostingTemplateBy(req.body.json)
            .then((template)=>{
                req.result = template.Cleanup()
                next()
            })
    })
     .get(function(req,res,next){

        return WebhostingTemplates(knex).FetchWebhostingTemplates()
            .then((rows)=>{
                req.result = rows.Cleanup()
                next()
            })


     })
     .put(function(req,res,next){

        return req.sendOk(
            WebhostingTemplates(knex).Insert(req.body.json, req.query.ts)
            .then(()=>{
               app.InsertEvent(req, {e_event_type: "webhosting-template-insert"});
            })

        );
     })



  require("account-tgz")(router, app)

  app.GetSmsLib = function(){
     return require("sms-lib.js")(app.knex, app)
  }

  app.Prepare = function() {
     var knexLib = require("MonsterKnex");
     knex = knexLib(config)

     app.knex = knex;

     Account = require("models/account.js")
     Credential = require("models/credential.js")
     Tel = require("models/tel.js")
     Email = require("models/email.js")
     Server = require("models/server.js")
     WebhostingTemplates = require("models/webhostingtemplates.js")

     app.smsLib = require("sms-lib.js")(knex, app);

     Token.GetTokenAsync().then(t =>{
        txtRecordToken = t;
     })


     require("account-account-search.js")(router, knex, vali)
     require("account-loginlog-router.js")(router, app)
     require("account-sms-router.js")(router, app)
     require("account-escalations-router.js")(router, app)

     return knex.migrate.latest()
       .then(()=>{

           var ll = require("loginlog-notify.js")(knex, app);
           return ll.Start();

       })
       .then(()=>{
          knexLib.installCleanup(knex, app.config.get("loginlog_cleanup"));
       })

  }

  app.GetRawCredentials = function(provider){
    return app.config.get("volume")[provider] || {};
  }


  app.Cleanup = function() {

     config.saveVolume({"api_secret": config.getApiSecret()})

     var fn = config.get("db").connection.filename
     if(!fn) return Promise.resolve()

     // the database might not exists, which is not an error in this case
     return fs.unlinkAsync(fn).catch(ex=>{})
  }

    app.getSmsLib=function (){
      return  smsLib(knex, app)
    }

    app.getLoginlog=function (){
      return  loginLogLib(knex, app)
    }

  setInterval(function(){
    return knex("sessions")
             .whereRaw("s_expires<=?", [moment.now()])
             .del()
             .return() // this one is needed in order to execute the query

  }, config.get("session_cleanup_interval_seconds")*1000)


  app.getBareDomain = function(hostname){
       var r = new RegExp('([a-z\-0-9]+)\\.([a-z0-9]+)$');
       var m = r.exec(hostname);
       if(!m) return;

       var bareDomain;
       if(Array("co").indexOf(m[1]) > -1) {
          r = new RegExp('([a-z\-0-9]+\\.[a-z\-0-9]+\\.[a-z0-9]+)$');
          m = r.exec(hostname);
          if(!m) return;
          return m[1];
       } else {
          bareDomain = m[1]+"."+m[2];    
       }

       return bareDomain;
  }


  return app

  function getExpirationDate(is_primary) {
     return moment().add(is_primary ? config.get("session_expiration") : config.get("session_expiration_subcredential"),"second").toISOString()
  }

function flatten(arr) {
  if(!Array.isArray(arr)) {
    return [arr];
  }

  var array = [];
  for(var i = 0; i < arr.length; i++) {
    array = array.concat(flatten(arr[i]));
  }
  return array;
}

    function buildSmsProviders(){
        var r = fs.readdirSync(__dirname)
        var RegExp = /^sms-provider-(.*)\.js$/
        var re = {}
        r.forEach(x=>{
          var m = RegExp.exec(x);
          if((!m)||(!m[1])) return;

          re[m[1]] = require(x)(app);
        })

        // console.log(re);process.reallyExit()
        return re
    }

}
