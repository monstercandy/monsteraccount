
const ValiLib = require("MonsterValidators")
var av = ValiLib.array()

const MError = require('MonsterError')



var re = module.exports = {}

re.serverNameEnforcements = {presence: true, isString: {strictName: true, lowerCase: true}, exclusion: ["backup"]}
re.passwordEnforcements = ValiLib.passwordEnforcements
re.emailEnforcements = {presence: true, email: true}
re.emailConfirmedEnforcements = {"u_email_confirmed": {isBoolean: true}}

re.IntKeyConstraint = {presence: true, isInteger: true}

re.getSupportedLanguages = function(){
  return ["en","hu"]
}

Object.keys(av).forEach(q=>{
   re[q] = av[q]
})
