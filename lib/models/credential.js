var lib = module.exports = function(knex, app) {

	var vali = require("MonsterValidators").ValidateJs()
    const MError = require('MonsterError')

	var re = {}
    var bcrypt = require('bcrypt-promise.js');
    var Common = require("models/common.js")

    const supportedPermissions = lib.supportedPermissions



    function getCredentialTrailer(promise){
    	 return promise.then(function(rows) {

         	return credentialSingleRowToPromise(rows)

        })

    }

    re.GetAccountCredentials = function(account){
         return knex.select()
	         .table("credentials")
			.whereRaw("c_user_id=?", [account["u_id"]])
	         .then(function(rows) {
	         	return credentialRowsToPromise(rows, account)
	         })

    }
    re.GetAccountCredentialByUsername = function(account, username){
         return knex.select()
	         .table("credentials")
			.whereRaw("c_user_id=? AND c_username=?", [account["u_id"],username])
			 /*
	         .where({
	         	 "c_user_id": account["u_id"],
	         	 "c_username": account["u_email"]
	          })
			  */
	         .then(function(rows) {
	         	return credentialSingleRowToPromise(rows, account)
	         })
    }

    re.GetAccountDefaultCredential = function(account){
    	 return re.GetAccountCredentialByUsername(account, account["u_primary_email"])
	}


    re.GetAccountCredentialByUuid = function(account, uuid){
	    var p = knex.select().table("credentials").whereRaw("c_user_id=? AND c_uuid=?", [account.u_id, uuid]);
        return getCredentialTrailer(p)
    }
    re.GetCredentialById = function(id){

        var j = {"c_id": id}
		var p = vali.async(j, {
		         "c_id": Common.IntKeyConstraint
		      })
		.then(function(data){

	         return knex.select()
	             .table("credentials")
				 .whereRaw("c_id=?", [data["c_id"]])
				 /*
	             .where("c_id", data["c_id"])
				 */

		})
        return getCredentialTrailer(p)
    }

    re.GetAccountsByRole = function(role, filters){

    	filters

        var p = knex.select("u_id", "u_name")
	         .table("credentials")
	         .leftJoin("users", "u_id", "c_user_id")
			 .whereRaw("c_grants LIKE ?", ["% "+role+" %"])
			 .groupBy("c_user_id");
		if(filters.limit)
			p = p.limit(filters.limit);
		if(filters.where)
			p = p.whereRaw("u_name LIKE ?", ["%"+filters.where+"%"]);

		return p.then(rows=>{
			 	 return rows;
			 })
    }

	re.GetCredentialByUsername = function(username){

        var j = {"username": username}
		var p = vali.async(j, {
		         "username": {presence: true, isString: {trim:true}}
		      })
		.then(function(data){

	         return knex.select()
	             .table("credentials")
				 .whereRaw("c_username=?", [data["username"]])

		})


        return getCredentialTrailer(p)

	}

	re.Validate = function(data){
		return vali.async(data, {
          "c_password": Common.passwordEnforcements
        })
	}

	re.InsertAdditional = function(account, username, credData) {
        return re.CredendialToHash(credData)
 	      .then(credential_data=>{
               return re.Insert(account.u_id, username, credential_data["c_password"], credData.c_grants, credData.c_uuid, credData.c_server)
 	      })
 	      .then(()=>{
 	      	 return Promise.resolve(username)
 	      })
	}

	function validateGrantsAsync(grants){
    	   var grantsArr = Common.ForceEnumerableToList(grants)

    	   if(grantsArr.length <= 0) throw new MError("VALIDATION_ERROR")

	       return vali.async({c_grants:grantsArr}, {c_grants: { inclusionCombo: supportedPermissions  }})
	}

	re.Insert = function(uid, username, hash, c_grants, c_uuid, c_server) {
		// we dont validate these parameters here since we expect a hash, login would not be possible anyway
		// username existence is checked via sql contraints

		var credential_data = {}
       credential_data.c_user_id = uid;
       credential_data.c_username = username;
       credential_data.c_password = hash;
       credential_data.c_uuid = c_uuid;
       credential_data.c_server = c_server || null;


       var max_number_of_credentials_per_account
       if((app)&&(app.config)) {
       	  max_number_of_credentials_per_account = app.config.get("max_number_of_credentials_per_account")
       }

       return vali.async(credential_data, {
       	  c_uuid: {isString: true},
       	  c_server: {isString: {strictName: true}},
       })
       .then(()=>{
          return validateGrantsAsync(c_grants)
       })
       .then(d=>{
           credential_data["c_grants"] = Common.ForceEnumerableToString(d.c_grants);

           if(!credential_data.c_uuid) return [];

           return knex("credentials").select().whereRaw("c_user_id=? AND c_uuid=?", [uid, credential_data.c_uuid])
        })
       .then(sameUuids=>{

       	   if(sameUuids.length > 0) throw new MError("UUID_ALREADY_EXISTS");

       	   if(!c_server) return;

       	   const Server = require("models/server.js")
       	   return Server(knex).FetchServer(c_server)
       	})
       .then(server=>{

           if(max_number_of_credentials_per_account) {

		         return knex("credentials").count("* AS c").whereRaw("c_user_id=?", [uid])

           }
           else
             return Promise.resolve([{c:0}])
        })
       .then(numberOfCredentialsRows=>{
       	   if((max_number_of_credentials_per_account)&&(numberOfCredentialsRows[0].c >= max_number_of_credentials_per_account))
       	   	  throw new MError("TOO_MANY_CREDENTIALS")

	       return knex
	         .insert(credential_data)
	         .into("credentials")
	         .catch(ex=>{
	         	console.error("error while insert", ex)
	         	throw new MError("CREDENTIAL_ALREADY_EXISTS")
	         })

       })


       // console.log("inserting", credential_data)
	}

	re.DeleteCredentialRowsOfAccount= function(account_id){
           return knex("credentials")
              .whereRaw("c_user_id=?",[account_id])
              .del()
	}

	re.CredendialToHash = function(data){

        var cre
        return re.Validate(data)
	    .then((aCredential_data)=>{
           cre = aCredential_data

          return vali.async(data, {
             "password_type": {inclusion: ["clear","encrypted"], default: "clear"}
          })

	    })
	    .then((aPasswordType)=>{
	         if(aPasswordType["password_type"] == "encrypted") return Promise.resolve(cre["c_password"])

	         return bcrypt.genSaltAsync(10)
	            .then((salt)=>{
	                 return bcrypt.hashAsync(cre["c_password"], salt)
	            })

	    })
	    .then((hash)=>{
	    	cre["c_password"] = hash

	    	return Promise.resolve(cre)
        })

	}

	re.FromConstants=function(h){
       return credentialSingleRowToPromise([h]);
	}


    function credentialRowsToPromise(rows, account) {

    	for(var q of rows) {
           credentialRowFunctions(q, account)

    	}

        Common.AddCleanupSupportForArray(rows)

        return Promise.resolve(rows)
    }

    function credentialSingleRowToPromise(rows, account){
        var credential = Common.EnsureSingleRowReturned(rows, "AUTH_FAILED")

        credentialRowFunctions(credential, account)

        return Promise.resolve(credential)
    }


    function credentialRowFunctions(credential, account) {

        credential.c_grants = Common.ForceEnumerableToList(credential.c_grants)

        credential.is_primary = ((credential.c_username) && (credential.c_username.indexOf("@") > -1))
        credential.can_be_deleted = !credential.is_primary

        credential.Cleanup = function () {
        	delete credential["c_password"]
        	delete credential["c_id"]
        	delete credential["c_user_id"]
        	return credential
        }

        credential.ResolveAccount = function(){
        	if(account) return Promise.resolve(account)

            var acc = new require("models/account.js")(knex)
            return acc.GetAccountById(credential.c_user_id)
        }

        credential.Authenticate = function (password) {
		    var j = {"password": password}

            var data
		    return vali.async(j, {
		       "password": {presence: true, isString: {trim:true}}
		    })
		    .then((aData)=>{
		    	data = aData
		    	const pwauth = require("password-auth.js")
		    	return pwauth(credential["c_password"], data["password"])
		    })
		    .then(authResult=>{

		        if(!authResult.result)
		          throw new MError("AUTH_FAILED")

                var p
                if(authResult.type == "mysql") {
                  p = credential.ChangePassword(data["password"])
                    .catch(ex=>{
                    	console.log("auto changing account password failed:", ex)
                    	// we swallow here, any failure during this pw change should not affect the original login operation
                    })
                    .then(()=>{
                    	return Promise.resolve(authResult)
                    })
                } else
                  p = Promise.resolve(authResult)

                return p


		    })


		}


		credential.RenameUsername = function (newUsername) {

		    	return knex('credentials')
				  .whereRaw('c_id=?', [credential["c_id"]])
				/*
		    	  .where('c_id', credential["c_id"])
				  */
		    	  .update({
		    	    "c_username": newUsername
		    	  })
		}

		credential.Delete = function () {
			if(!credential.can_be_deleted)
				throw new MError("DEFAULT_CREDENTIAL_CANNOT_BE_DELETED")

           return knex("credentials")
              .whereRaw("c_id=?",[credential.c_id])
              .del()

		}


		credential.ChangePassword = function (newPassword) {



		    var j = {"password": newPassword}
		    var data

		    return vali.async(j, {
		       "password": Common.passwordEnforcements
		    })
		    .then((aData)=>{

		    	data = aData


                return bcrypt.genSaltAsync(10)
		    })
		    .then((salt)=>{

                return bcrypt.hashAsync(data["password"], salt)
		    })
		    .then((hash)=>{


		    	return knex('credentials')
				  .whereRaw('c_id=?', [credential["c_id"]])
				/*
		    	  .where('c_id', credential["c_id"])
				  */
		    	  .update({
		    	    "c_password": hash
		    	  })

		    })

		}

		credential.UpdateGrants = function(grantsData) {

		       return validateGrantsAsync(grantsData.c_grants)
		       .then(d=>{
		           d["c_grants"] = Common.ForceEnumerableToString(d.c_grants)

			       return knex
			         .update(d)
			         .into("credentials")
			         .whereRaw("c_id=?", [credential.c_id])
		       })

		}


		return credential

    }

    return re
}

lib.supportedPermissions = 	       	  [
		       	  "SUPERUSER",

		       	  "ADMIN_DOCROOTAPI",
		       	  "ADMIN_WEBHOSTING",
		       	  "ADMIN_GIT",
		       	  "ADMIN_EVENTAPI",
		       	  "ADMIN_ACCOUNTAPI",
		       	  "ADMIN_TAPI",
		       	  "ADMIN_DBMS",
		       	  "ADMIN_FILEMAN",
		       	  "ADMIN_FTP",
		       	  "ADMIN_INSTALLATRON",
		       	  "ADMIN_EMAIL",
		       	  "ADMIN_MAILER",
		       	  "ADMIN_CERT",
		       	  "ADMIN_IPTABLES",
		       	  "ADMIN_SMS",
		       	  "ADMIN_WIE",
		       	  "ADMIN_TIMEMACHINE",
		       	  "ADMIN_DOCKER",

		       	  "SERVER_OWNER",

		       	  "ADMIN_TALLY",

		       	  "DOCROOT_ENTRIES",
		       	  "MAIL_SERVICE_PASSWORD_LOOKUP",

		       	  "FLUSH_CACHE",

		       	  "CERT_ATTACH",

		       	  "TAPI_BUILD",

                  "SEND_EMAIL",
                  "SEND_SMS",
		       	  "SEND_EMAIL_TO_USER",

		       	  "FILEMAN_UPLOAD_DBBACKUP",
		       	  "FILEMAN_MAILDIRMAKE",

		       	  "ACCOUNT_OWNER",
		       	  "ACCOUNT_DNS",

		       	  "FEED_HOSTSSH_IP",
		       	  "FEED_LOGIN_IP",

                  "EMIT_LOG",
		       	  "LOGINLOG_EVENT_REPORT",
		       	  "ESCALATE_EXCEPTION",

		       	  "BAN_IP",

		       	  "ACCOUNT_LOCAL_WEBSTORES",

                  "INFO_GEOIP",
                  "INFO_ACCOUNT",
                  "INFO_EMAIL",
                  "INFO_WEBHOSTING_TEMPLATE",
                  "INFO_SERVERS",
                  "INFO_WEBHOSTING",
                  "INFO_SESSION",

		       	  "ACCOUNT_FORCE_PASSWORD"
	       	  ]
