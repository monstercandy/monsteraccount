const ValiLib = require("MonsterValidators")
var lib = module.exports = function(knex) {

  	var vali = ValiLib.ValidateJs()
    const MError = require('MonsterError')

    var Common = require("models/common.js")

    var validators = ValiLib.forcePresence(ValiLib.WebhostingTemplateValidators, true)

    var re = {}


    re.Insert = function(in_server_data, ts){
      return vali.async(in_server_data, validators)
    	.then(d=>{

         d.t_bandwidth_limit = d.t_bandwidth_limit ? JSON.stringify(d.t_bandwidth_limit) : "{}";

         if(d.t_storage_max_quota_hard_mb < d.t_storage_max_quota_soft_mb)
            throw new MError("VALIDATION_ERROR", "soft should be less than hard");

         d.t_allowed_dynamic_languages = Common.ForceEnumerableToString(d.t_allowed_dynamic_languages);

         return knex
           .insert(d)
           .into("webhostingtemplates")
           .return()
           .catch(ex=>{
              if(ex.code  == "SQLITE_CONSTRAINT")
                throw new MError("TEMPLATE_ALREADY_EXISTS");
              throw ex;
           })

      })


	}

	re.FetchWebhostingTemplates = function() {

		return knex.select().from("webhostingtemplates")
		   .then(rows=>{
		   	   return templateRowsToObjects(rows)
		   })

	}

  re.GetWebhostingTemplateBy = function(filters) {
    return re.FetchWebhostingTemplates()
       .then(rows=>{
           var re = []
           rows.forEach(row=>{
              var match = true
              Object.keys(filters).some(k =>{
                 if(row[k] != filters[k]) {
                     match = false
                     return true
                 }
              })
              if(match) re.push(row)
           })

           Common.AddCleanupSupportForArray(re);
           return Promise.resolve(re);
       })
  }

  re.GetWebhostingTemplateByName = function(name) {

    return re.GetWebhostingTemplateBy({t_name: name})
      .then(rows=>{
           if(rows.length != 1)
              throw new MError("TEMPLATE_NOT_FOUND")

           return rows[0]
      })

  }

    function templateRowsToObjects(rows) {
    	  "use strict"


    	  for(let template of rows) {

            // note: we dont drop this column in knex, since it invalidates the unique column indexes
            delete template.t_max_number_of_python_apps;

            template.t_allowed_dynamic_languages = Common.ForceEnumerableToList(template.t_allowed_dynamic_languages)
            template.t_bandwidth_limit = JSON.parse(template.t_bandwidth_limit);

      		  template.Cleanup = function(){
      		  	  delete template.t_id
      		  	  return template
      		  }
            template.Change = function(in_data){
                var validators = ValiLib.WebhostingTemplateValidators
                 var d
                return vali.async(in_data, validators)
                  .then(ad=>{
                      d = ad
                      if(Object.keys(d).length <= 0) throw new MError("NOTHING_TO_CHANGE")

                      if(d.t_name == template.t_name) return Promise.resolve([])

                      return knex("webhostingtemplates").select("t_id").whereRaw("t_name=?",[d.t_name])
                   })
                  .then((rows)=>{
                      if(rows.length > 0) throw new MError("ALREADY_EXISTS")

                      if(d.t_bandwidth_limit)
                        d.t_bandwidth_limit = JSON.stringify(d.t_bandwidth_limit);

                      d.t_allowed_dynamic_languages = Common.ForceEnumerableToString(d.t_allowed_dynamic_languages)

                      return knex.update(d)
                         .into("webhostingtemplates")
                         .whereRaw("t_id=?", [template.t_id])

                  })

            }
            template.Delete = function(){

                return knex("webhostingtemplates")
          			 .whereRaw("t_id=?", [template.t_id])
          			 .del()

            }

    	  }

        Common.AddCleanupSupportForArray(rows)

        return Promise.resolve(rows)

    }


    return re

}


lib.dynamicLanguages = ValiLib.WebhostingTemplateValidators.t_allowed_dynamic_languages.inclusionCombo
