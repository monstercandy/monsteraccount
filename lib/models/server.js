var lib = module.exports = function(knex) {

  	var vali = require("MonsterValidators").ValidateJs()
    const MError = require('MonsterError')

    const dotq = require("MonsterDotq");

    // var DomainLib = require("models/domain.js")
    var WebhostingLib = require("models/webhosting.js")

    const accountLib = require("models/account.js");

    var Common = require("models/common.js")

    const crypto = require('crypto');

    var validators = {
        "s_name": Common.serverNameEnforcements,
        "s_secret": {presence: true, isString: {noXssFiltering: true}},
        "s_roles": {inclusionCombo: lib.SERVER_ROLES, isArray: true},
        "s_users": {isArray: {lazyString: true}, validUsers: true},
      }


      vali.validators.validUsers = function(value, options, key, attributes) {
        if(!value) return

        return new vali.Promise(function(resolve) {

            var accounts = accountLib(knex);
            return dotq.linearMap({array: value, action: function(userId){
                return accounts.GetAccountById(userId);
            }})
            .then(()=>{
               return resolve();
            })
            .catch(exMsg=>{
               console.error("Invalid userId supplied", exMsg);
               resolve("invalid userid");
            });

        })
      }

    var re = {}
    re.Validate = function(in_server_data) {
	    return vali.async(in_server_data, validators)

    }


    re.Insert = function(in_server_data, ts){

      var d

    	return re.Validate(in_server_data)
    	.then(ad=>{
         d = ad

         d.s_roles = Common.ForceEnumerableToString(d.s_roles);
         d.s_users = Common.ForceEnumerableToString(d.s_users);

         return knex
           .insert(d)
           .into("servers")

      })
      .then(()=>{
         return {auth_code: d.s_secret};
      })


	}

	re.FetchServers = function() {

		return knex.select().from("servers")
		   .then(rows=>{
		   	   return serverRowsToPromise(rows)
		   })
	}

  re.FetchServerNamesOfAccount = function(u_id) {

    return knex.select("s_name").from("servers").whereRaw("s_users LIKE ?", ["%"+u_id+"%"])
       .then(rows=>{
           return rows.map(x => x.s_name);
       })
  }


  re.FetchServersWithAuthCode = function(){
     return re.FetchServers()
       .then(servers=>{

          servers.forEach(server=>{

              server.auth_code = server.s_secret;

          })

          return servers;
       })
  }

  re.FetchServer = function(name) {

    return re.FetchServers()
       .then(rows=>{
           var re
           rows.some(row=>{
              if(row.s_name == name) {
                re = row
                return true
              }
           })

           if(!re)
              return Promise.reject(new MError("SERVER_NOT_FOUND"))

           return Promise.resolve(re)
       })
  }

    function serverRowsToPromise(rows) {
    	"use strict"


    	for(let server of rows) {

          server.s_roles = Common.ForceEnumerableToList(server.s_roles)
          server.s_users = Common.ForceEnumerableToList(server.s_users)

    		  server.Cleanup = function(){
    		  	  delete server.s_id;
              delete server.s_secret;
              delete server.s_auth_code;
    		  	  return server;
    		  }

          server.CalculateDerivedToken = function(primary_token, source_server) {
              // sha256(sessionRow.s_authtoken + server.s_name + server.s_secret + source_server) == derived_token

              const crypto = require('crypto');
              const hash = crypto.createHash('sha256');

              hash.update(primary_token + server.s_name + server.s_secret + (source_server||"") );
              return hash.digest('base64');
          }

          server.Delete = function(){

              return WebhostingLib(knex).GetCountByServerName(server.s_name)
                .then(c=>{
                    if(c > 0) throw new MError("SERVER_HAS_ACTIVE_WEBHOSTINGS")

                    // TODO: same with domains (as soon as we got the table which binds domains to servers)


                    return knex("servers")
                     .whereRaw("s_id=?", [server.s_id])
                     .del()
                     .then(()=>{
                        return Promise.resolve()
                     })
                })


          }

          server.Change = function(in_data) {
              return vali.async(in_data, {
                "s_roles": validators.s_roles,
                "s_users": validators.s_users,
              })
              .then(d=>{
                 var toUpdate = {};
                 if(d.s_roles) {
                    toUpdate.s_roles = Common.ForceEnumerableToString(d.s_roles);
                 }
                 if(d.s_users) {
                    toUpdate.s_users = Common.ForceEnumerableToString(d.s_users);
                 }
                 if(Object.keys(toUpdate).length <= 0)
                   return; // nothing to update


                 return knex.update(toUpdate)
                   .into("servers")
                   .whereRaw("s_id=?", [server.s_id])

              })

          }

          server.RemoveRole = function(role) {
              var i = server.s_roles.indexOf(role);
              if(i < 0) throw new MError("ROLE_NOT_FOUND");
              server.s_roles.splice(i, 1);
              var roles_str = Common.ForceEnumerableToString(server.s_roles);

              return knex.update({s_roles: roles_str})
                 .into("servers")
                 .whereRaw("s_id=?", [server.s_id])
                 .return()


          }

    	}

        Common.AddCleanupSupportForArray(rows)

        return Promise.resolve(rows)

    }


    return re

}


lib.SERVER_ROLES = [
  "accountapi","tapi","webhosting","database","mysql","geoip","installatron", "email"
]
