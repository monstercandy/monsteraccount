(function(){

  const KnexLib = require("MonsterKnex");

  const cleanedUpCountryCodes = cleanupHash(  require("phone.json") )
  const countryCodes = getValues( cleanedUpCountryCodes )

  var lib = module.exports = function(knex) {

    var vali = require("MonsterValidators").ValidateJs()
      const MError = require('MonsterError')

    var re = {}
      var Common = require("models/common.js")

    const moment = require("MonsterMoment")

      const defaultTelCategories = ["HOME", "WORK", "MOBILE", "FAX"]

      re.Validate = function(in_tel_data) {
        return vali.async(in_tel_data, {
          "t_country_code": lib.validator_country_code,
          "t_area_code": lib.validator_area_code,
          "t_phone_no": lib.validator_phone_no,
          "t_confirmed": {isBoolean: true},
          "t_primary": {isBoolean: true},
          "t_category": { inclusion: defaultTelCategories, default: "[[empty]]" },
        })

      }

      re.GetAcceptedCountryCodes = function() {
         return cleanedUpCountryCodes
      }

      re.Insert = function(uid, in_tel_data){
          var tel_data
          var human
        return re.Validate(in_tel_data)
        .then(aTel_data=>{
          tel_data = aTel_data

          human = lib.TelToHumanString(tel_data)

        return knex.select().from("tels")
           .whereRaw("t_user_id=? AND t_human=?", [uid, human])

          })
        .then(rows=>{
          if(rows.length != 0) throw new MError("TEL_ALREADY_EXISTS")

          if(!tel_data["t_primary"]) return Promise.resolve([])

              // check if account already has a primary telephone number set
        return knex.select().from("tels")
           .whereRaw("t_user_id=? AND t_primary=1", [uid])

          })
        .then((rows)=>{

           if(rows.length != 0) throw new MError("TEL_ALREADY_EXISTS")


             tel_data["t_user_id"] = uid
             tel_data["t_human"] = human

           if(tel_data["t_confirmed"])
                 tel_data["t_confirmed_at"] = moment().toISOString()

           return knex
             .insert(tel_data)
             .into("tels")

        })


    }

    re.SetEscalate=function(in_data) {
      return knex("tels").update({t_escalate: in_data.t_escalate ? true : false}).whereRaw("t_human=?",[in_data.t_human]).return();
    }

    re.ListBy = function(filters, account) {
      return vali.async(filters, {
         t_user_id: {},
         t_human: {},
         t_escalate: {},
         t_primary: {},
         t_confirmed: {},
      })
      .then((d)=>{
        if(d.t_human) {
          d["~t_human"] = d.t_human;
          delete d.t_human;
        }
        var f = KnexLib.filterHash(d);
        return knex.select().from("tels")
           .whereRaw(f.sqlFilterStr, f.sqlFilterValues)
      })
       .then(rows=>{
           return telRowsToObject(account, rows)
       })
    }

    re.FetchAccountTels = function(account,only_tel) {
      var d = {t_user_id: account["u_id"]};
      if(only_tel){
          d.t_human=only_tel;
      }

      return re.ListBy(d, account)
    }

      function telRowsToObject(account, rows) {
        "use strict"


        for(let tel of rows) {

            tel.Cleanup = function(){
                delete tel.t_id;
                return tel;
            }


          tel.GenerateNewToken = function(){

                var token
                const Token = require("Token")
                return Token.GetTokenAsync(4, "hex")
                 .then((aToken)=>{
                    token = aToken
                    return knex("tels").update({t_token: token, t_token_generated_at: moment.now()}).whereRaw("t_id=?", [tel.t_id])
                 })
                 .then(()=>{
                    tel.t_token = token
                    return Promise.resolve(token)
                 })

           }

                tel.Delete = function(){

                    if(tel.t_primary) throw new MError("PRIMARY_TEL_CANNOT_BE_DELETED")

                    return knex("tels").whereRaw("t_id=?", [tel.t_id]).del()
                }


              tel.SendCode = function(in_data, app, req){
                 return tel.GenerateNewToken()
                   .then((token)=>{

                      var templateLib = require("MonsterTemplates")(app.config.get("i18n-2"))
                      return templateLib.renderAsync(
                        '{{ "%s: Your telephone verification code is: %s"|__(service, token) }}',
                        {
                           service: app.config.get("service_name"),
                           token: token,
                        },
                        {locale: account.u_language}
                      )
                    })
                   .then(msg=>{

                      return app.GetSmsLib().Insert({
                        s_country_code: tel.t_country_code,
                        s_area_code: tel.t_area_code,
                        s_phone_no: tel.t_phone_no,
                        s_message: msg.html,
                      }, req)

                   })
                   .then(()=>{
                      app.InsertEvent(req, {e_event_type: "tel-code-sent", e_other: true});
                   })

              }

              tel.Verify = function(data, app, req){

                   return Promise.resolve()
                     .then(()=>{
                         if((!tel.t_token)||(!tel.t_token_generated_at)) throw new MError("NO_TEL_TOKEN")

                         if(tel.t_token_generated_at < app.getOldestPossibleTelToken()) throw new MError("EMAIL_TOKEN_TOO_OLD")

                         if(tel.t_token != data.confirmation_token) throw new MError("TOKEN_MISMATCH")

                   })
                   .then(()=>{

                      return knex("tels")
                        .update({t_token: "", t_confirmed: true, t_confirmed_at: moment.now()})
                        .whereRaw("t_id=?", [tel.t_id])

                   })
                   .then(()=>{
                      if(tel.t_primary)
                         return account.SetPrimaryTelToVerified(true)

                      app.InsertEvent(req, {e_event_type: "tel-verify", e_other: {"t_human": tel.t_human}, e_username: account.u_primary_email, e_user_id: account.u_id})
                   })

              }


        }

          Common.AddCleanupSupportForArray(rows)

          return Promise.resolve(rows)

      }


      return re

  }

  lib.validator_country_code = {presence: true, inclusion: countryCodes};
  lib.validator_area_code = {presence: true, numericality: {onlyInteger: true, strict: true, greaterThan: 0, lessThanOrEqualTo: 1000}};
  lib.validator_phone_no = {presence: true, numericality: {onlyInteger: true, strict: true, greaterThan: 10000, lessThanOrEqualTo: 10000000} };


  lib.TelToHumanString = function(tel_data, prefix){
      if(!tel_data) return "";
      if(!prefix) prefix = "t";

      var human = tel_data[prefix+"_country_code"] + "-" + tel_data[prefix+"_area_code"] + "-" + tel_data[prefix+"_phone_no"];
      if(human.substring(0,1) != "+") human = "+" + human;

      return human;
  }

  function cleanupHash(obj) {
    "use strict"

    var re = {}
    for (let key of Object.keys(obj)) {
        let val = obj[key];
        if(val == '') continue;
        re[key] = val
    }
    return re

  }

  function getValues(obj) {
    "use strict"

    var re = []
    for (let key of Object.keys(obj)) {
        re.push(obj[key])
    }
    return re
  }

})()
