var Account = module.exports = function(knex, app) {

	const loginSubsystemCategories = require("account-config-router.js").LOGINLOG_SUPPORTED_SUBSYSTEMS;

	var vali = require("MonsterValidators").ValidateJs()
    const MError = require('MonsterError')

    const speakeasy = require("speakeasy");


	var re = {}
    var bcrypt = require('bcrypt-promise.js');
    const moment = require('MonsterMoment');

    var Common = require("models/common.js")

    var Credential =  require("models/credential.js")
    var Email =  require("models/email.js")
    var Tel =  require("models/tel.js")
    var Domain=  require("models/domain.js")
    var Webhosting=  require("models/webhosting.js")
    var WebhostingDomain=  require("models/webhostingdomain.js")

    const Token = require("Token");


    var valis = {
	      "u_id": {numericality: {onlyInteger: true, strict: true}},
	      "u_callin": {numericality: {onlyInteger: true, lessThanOrEqualTo: 99999}}, // strict: true, // we accept leading zeros and "malformed numbers" until migration is completed
	      "u_name": {presence: true, isString: {trim:true}},
	      "u_comment": {isString: {trim:true}},
	      "u_language": {presence: true, inclusion: Common.getSupportedLanguages()},
	      "u_email_login_ok": {isBooleanLazy: true},
	      "u_email_login_fail": {isBooleanLazy: true},
	      "u_sms_notification": {isBooleanLazy: true},
	      "created_at": {datetime: true},
	      "u_email_login_ok_subsystems": {inclusionCombo: loginSubsystemCategories, isArray: {minLength: 1}},
    }


    re.GetNextAccountIdAsync = function(){
			return knex("users").max("u_id AS m")
			  .then(d=>{
			  	  if(Array.isArray(d)) d = d[0]
			  	  if(typeof d === "object") d = d.m

			  	  // console.log("D",d)
			  	  d = (parseInt(d||0, 10)) + 1
			  	  return Promise.resolve(d.toString()) // note the string casting here (it is important for being compatible with uuid )
			  })
    }


    re.CreateAccount=function(in_user_data, ts, phoneNumberRequired, config){
		    var uid
		    var user_data
		    var email_data
		    var tel_data
		    var credential_data

		    return vali.async(in_user_data, valis)
		    .then((aUser_data)=>{
		        user_data = aUser_data
		    	console.log("inserting account", user_data);
		        return Credential().CredendialToHash(in_user_data)

		    })
		    .then((aCredential_data)=>{
		          credential_data = aCredential_data

		          return Email().Validate(in_user_data)
		    })
		    .then((aEmail_data)=>{
		          email_data = aEmail_data
		          email_data["e_primary"] = true

		          return Tel().Validate(in_user_data)
		            .catch(ex=>{
		            	if(in_user_data.t_phone_no) throw ex // some details are wrong, lets pass the exception through

		            	// no phone number was provided at all
		    	        if(phoneNumberRequired)
		    	  	         throw ex

                        // phone number is not required anyway, so lets just continue
		            	return Promise.resolve()
		            })
		    })
		    .then((aTel_data)=>{
		    	  tel_data = aTel_data
		    	  if(tel_data)
		    	     tel_data["t_primary"] = true

                  if(user_data.u_id) return Promise.resolve(user_data.u_id)


                  	if(config.get("linear_numeric_account_ids"))
                  		return re.GetNextAccountIdAsync()

                  return Token.GetPseudoTokenAsync(email_data["e_email"], credential_data["c_password"], ts, 8, "hex")

            })
		    .then(uuid=>{

		    	  uid = uuid

		          return knex.transaction(function(trx){


		          	  user_data["u_id"] = uuid
		          	  user_data["u_primary_email"] = email_data["e_email"]
		          	  user_data["u_primary_email_stripped"] = Email.emailStrip(email_data["e_email"])
		          	  user_data["u_primary_email_confirmed"] = email_data["e_confirmed"] || false

		          	  user_data["u_primary_tel"] = Tel.TelToHumanString(tel_data)

	                   return trx
	                     .insert(user_data, "u_id")
	                     .into("users")
		                .then(function(){
 		          	           var email = new Email(trx)
	                           return email.Insert(uuid, email_data)
		                })
		                .then(function(){
		                	   if(!tel_data) return Promise.resolve()

 		          	           var tel = new Tel(trx)
	                           return tel.Insert(uuid, tel_data)
		                })
		                .then(function(){
	                           var cred = new Credential(trx)
	                           return cred.Insert(uid, email_data["e_email"], credential_data["c_password"], "ACCOUNT_OWNER")
		                })
				         .catch(ex=>{
				         	if(ex.message == "CREDENTIAL_ALREADY_EXISTS")
				         	   throw new MError("EMAIL_ALREADY_EXISTS")
				         	throw ex
				         })

		         })

		     })
		    .then(()=>{
                return Promise.resolve(uid)
		    })
    }


    function getAccountById(account_id) {

		var j = {"account_id": account_id}

		return vali.async(j, {
		         "account_id": {presence: true, isString: {trim:true}}
		      })
		.then(function(data){
			var p = knex.select()
             .table("users")
			 .whereRaw("u_id=?", [account_id])

             return p
        })

    }

    re.GetAccountById = function(account_id) {

        return getAccountById(account_id)
	        .then(function(rows){
	        	// console.log("foo", rows)
	         	return accountSingleRowToPromise(rows)

	        })

    }

    re.LookupAccountByPrimaryEmail = function(in_data) {
		return vali.async(in_data, {
		         "email": {presence: true, email: true},
		      })
	      .then(d=>{
	      	var email = Email.emailStrip(d.email)
			var p = knex.select()
             .table("users")
			 .whereRaw("u_primary_email_stripped=? AND u_deleted=0", [email])

             return p
	      })
	      .then(function(rows){
	         	return accountSingleRowToPromise(rows)

	      })

    }


    function accountSingleRowToPromise(rows){
        var account = Common.EnsureSingleRowReturned(rows, "ACCOUNT_NOT_FOUND")

        accountRowFunctions(account)

        return Promise.resolve(account)
    }


    function accountRowsToPromise(rows) {

    	for(var q of rows) {
           accountRowFunctions(q)

    	}

        Common.AddCleanupSupportForArray(rows)

        return Promise.resolve(rows)
    }

    function accountRowFunctions(account) {

        account.u_email_login_ok_subsystems = Common.ForceEnumerableToList(account.u_email_login_ok_subsystems);

		  account.SetLoginOkSubsystems = function(in_data) {
		  	    const loginSubsystemCategories = require("account-config-router.js").LOGINLOG_SUPPORTED_SUBSYSTEMS;
			    return vali.async(in_data, {
			      "u_email_login_ok_subsystems": {presence: true, inclusionCombo: loginSubsystemCategories, isArray: {minLength: 1}},
			    })
			    .then(d=>{
			    	d["u_email_login_ok_subsystems"] = Common.ForceEnumerableToString(d["u_email_login_ok_subsystems"])

	                return knex.update(d)
	                     .into("users")
						 .whereRaw("u_id=?", [account.u_id])

			    })
		  }

        account.Cleanup = function () {
        	delete account.u_comment
        	delete account.u_primary_email_stripped
        	// this one is actually needed by the relayer API
        	// delete account.u_id
        	return account
        }

        account.IsNotificationEmailWanted = function(successful, subsystem) {
        	// console.log("checking", successful, account)
        	return account[successful ? 'u_email_login_ok' : 'u_email_login_fail']
        }

	    account.ChangeDetails = function(details){

	    	var avalis ={
		      "u_name": valis["u_name"],
		      "u_language": valis["u_language"],
		      "u_callin": valis["u_callin"],
		      "u_comment": valis["u_comment"],
		      "u_sms_notification": valis["u_sms_notification"],
	          "u_email_login_ok": valis["u_email_login_ok"],
	          "u_email_login_fail": valis["u_email_login_fail"],
	          "u_email_login_ok_subsystems": valis["u_email_login_ok_subsystems"],
		    }
		    for(var q of Object.keys(avalis)) {
		    	delete avalis[q].presence
		    }

		    return vali.async(details, avalis)
		    .then((aUser_data)=>{

		    	   if(Object.keys(aUser_data).length <= 0) throw new MError("NOTHING_TO_CHANGE")

         		   if(aUser_data["u_email_login_ok_subsystems"])
	             	   aUser_data["u_email_login_ok_subsystems"] = Common.ForceEnumerableToString(aUser_data["u_email_login_ok_subsystems"])
	             	
		    	   aUser_data["updated_at"] = moment.now()

		           return knex
		             .update(aUser_data)
		             .into("users")
					 .whereRaw("u_id=?",[account.u_id])

		    })

	    }

	    account.SetPrimaryTelToVerified = function(verified){
           return knex
             .update({"u_primary_tel_confirmed": verified})
             .into("users")
			 .whereRaw("u_id=?",[account.u_id])
	    }

	    account.SetPrimaryEmailToVerified = function(){
           return knex
             .update({"u_primary_email_confirmed": true})
             .into("users")
			 .whereRaw("u_id=?",[account.u_id])
			 /*
             .where("u_id", account.u_id)
			 */
	    }

	    account.GetWebhostingDomains = function() {
            var domain = new WebhostingDomain(knex)
            return domain.GetWebhostingDomainsByAccount(account)
	    }

	    account.GetDomains = function(onlyDomainId, onlyDomainName) {
            var domain = new Domain(knex)
            return domain.GetAccountDomains(account,onlyDomainId, onlyDomainName)
	    }

	    account.AddDomain = function(domainData) {
            var domain = new Domain(knex)
            return domain.Insert(account.u_id, domainData)
	    }

	    account.GetWebhostings = function(server_name, webhosting_id) {
            var webhosting = new Webhosting(knex)
            return webhosting.GetAccountWebhostings(account, server_name, webhosting_id)
	    }

	    account.AddWebhosting = function(webhostingData) {
            var webhosting = new Webhosting(knex)
            return webhosting.Insert(account.u_id, webhostingData)
	    }


	    account.AddCredential = function(username, cred_data) {
            var cred = new Credential(knex, app)
            return cred.InsertAdditional(account, username, cred_data)
	    }

	    account.AddTel = function(tel_data) {
            var tel = new Tel(knex)
            return tel.Insert(account.u_id, tel_data)
	    }

	    account.AddEmail = function(email_data) {
            var email = new Email(knex)
            return email.Insert(account.u_id, email_data)
	    }

	    account.FetchEmails = function(onlyEmail, onlyCategory) {
            var email = new Email(knex)
            return email.FetchAccountEmails(account, onlyEmail, onlyCategory)
	    }
	    account.FetchEmailsOnly = function(onlyEmail, onlyCategory) {
	    	return account.FetchEmails(onlyEmail, onlyCategory)
	    	  .then(emails=>{
			    	return emails.map(function(q){
			    		return q.e_email
			    	})
	    	  });
	    }

	    account.FetchTels = function(only_tel) {
            var tel = new Tel(knex)
            return tel.FetchAccountTels(account,only_tel)
	    }


	    account.GetPrimaryEmail = function() {
	    	return account.FetchEmails(account["u_primary_email"])
	    	  .then(emails=>{
	    	  	  return Promise.resolve(emails[0])
	    	  })
	    }
        account.GetCredentials = function() {
            var cred = new Credential(knex)
            return cred.GetAccountCredentials(account)
        }

        account.GetCredentialByUuid = function(uuid) {

            var cred = new Credential(knex)
            return cred.GetAccountCredentialByUuid(account, uuid)

        }

        account.GetCredentialByUsername = function(username) {

            var cred = new Credential(knex)
            return cred.GetAccountCredentialByUsername(account, username)

        }
        account.GetDefaultCredential = function() {
        	var cred = new Credential(knex)
        	return cred.GetAccountDefaultCredential(account)

        }

        account.Delete = function() {
        	  var cred
		      return account.getEverythingForAccount()
		         .then((belongings)=>{
		             if(belongings.domains.length > 0)
		               throw new MError("ACCOUNT_HAS_ACTIVE_SERVICES")

		             if(belongings.webhostings.length > 0)
		               throw new MError("ACCOUNT_HAS_ACTIVE_SERVICES")

		               return knex
		                  .update({u_deleted: 1})
		                  .into("users")
		                  .whereRaw("u_id=?",[account.u_id])
		         })
		         .then(()=>{
		            cred = new Credential(knex)
		            return cred.DeleteCredentialRowsOfAccount(account.u_id)

		         })
		         .then(()=>{
		         	 return cred.Insert(account.u_id, account.u_id, "invalid_hash_wont_ever_match", "ACCOUNT_OWNER")
		         })

        }

        account.getEverythingForAccount = function() {
        	var re = {}

            return account.GetDomains()
              .then((domains)=>{
                re.domains = domains
                return account.GetWebhostings()
              })
              .then((webhostings)=>{
                re.webhostings = webhostings
                return account.GetServers();
              })
              .then((servers)=>{
                re.servers = servers;
                return re;
              })
        }

        account.GetServers = function(){
        	const serversLib = require("models/server.js");
        	var servers = serversLib(knex);
        	return servers.FetchServerNamesOfAccount(account.u_id);
        }


        account.SetGrant=function(account_grant_access_for){

           // console.log("granting access to", account.u_id, "for", account_grant_access_for.u_id)

           return knex
             .update({
             	"u_grant_access_for": account_grant_access_for.u_id,
             })
             .into("users")
			 .whereRaw("u_id=?",[account.u_id])

        }

        account.GetAccountsThisHasAccessFor = function(){

			return knex.select()
	             .table("users")
				 .whereRaw("u_grant_access_for=?", [account.u_id])
			  .then(function(rows){
	         	return accountRowsToPromise(rows)

	          })

        }
        account.GetAccountsHavingAccessToThis = function(){
        	if(!account.u_grant_access_for)
               return accountRowsToPromise([])

			return getAccountById(account.u_grant_access_for)
			  .then(function(rows){
	         	return accountRowsToPromise(rows)
	          })
        }

          /// this method should be called in a transaction!
          account.SetPrimaryEmail = function(email, app, req){

          	  if(!email.e_confirmed) throw new MError("EMAIL_NOT_CONFIRMED")
          	  if(email.e_primary) throw new MError("EMAIL_ALREADY_PRIMARY")

              var cred = new Credential(knex)
              return cred.GetAccountDefaultCredential(account)
                .then(credential=>{
                	return credential.RenameUsername(email.e_email)
                })
                .then(()=>{
		           return knex
		             .update({
		             	"e_primary": false,
		             })
		             .into("emails")
					 .whereRaw("e_user_id=?",[account.u_id])

                })
                .then(()=>{
		           return knex
		             .update({
		             	"e_primary": true,
		             })
		             .into("emails")
					 .whereRaw("e_user_id=? AND e_id=?",[account.u_id, email.e_id])

                })
                .then(()=>{
		           return knex
		             .update({
		             	"u_primary_email": email.e_email,
		             	"u_primary_email_stripped": email.e_email_stripped,
		             	"u_primary_email_confirmed": true
		             })
		             .into("users")
					 .whereRaw("u_id=?",[account.u_id])
                })
                .then(()=>{
                   account.old_primary_email = account.u_primary_email
                   account.u_primary_email = email.e_email

                   return email.SendPrimaryChanged(app, req)
                })


          }

          account.ThrowIfInvalidToken = function(userToken){
		      return vali.async({userToken:userToken}, {userToken: {presence: true, isString: {trim:true}}})
		        .then(d=>{

		        	if(!account.u_totp_secret){
		        		throw new MError("NOT_ACTIVATED");
		        	}

		            var verified = speakeasy.totp.verify({ secret: account.u_totp_secret,
		                                                   encoding: 'base32',
		                                                   token: d.userToken });

		            if(!verified) throw new MError("INVALID_TOTP_TOKEN");
		        })

          }
          account.RegenerateTotpToken = function(){
		      var secret = speakeasy.generateSecret();
		      account.u_totp_secret = secret.base32;
		      return knex("users").update({u_totp_secret: account.u_totp_secret}).whereRaw("u_id=?",[account.u_id]).return();
          }
          account.InactivateTotp = function(){
		      return knex("users").update({u_totp_active: false, u_totp_secret: ""}).whereRaw("u_id=?",[account.u_id]).return();
          }
          account.ActivateTotp = function(){
		      return knex("users").update({u_totp_active: true}).whereRaw("u_id=?",[account.u_id]).return();
          }

          /// this method should be called in a transaction!
          account.SetPrimaryTel = function(tel, config){

          	  if((config.get("tel_should_be_confirmed"))&&(!tel.t_confirmed)) throw new MError("TEL_NOT_CONFIRMED")
          	  if(tel.t_primary) throw new MError("TEL_ALREADY_PRIMARY")

              return knex
		             .update({
		             	"t_primary": false,
		             })
		             .into("tels")
					 .whereRaw("t_user_id=?",[account.u_id])

                .then(()=>{
		           return knex
		             .update({
		             	"t_primary": true,
		             })
		             .into("tels")
					 .whereRaw("t_user_id=? AND t_id=?",[account.u_id, tel.t_id])

                })
                .then(()=>{
		           return knex
		             .update({
		             	"u_primary_tel": tel.t_human,
		             	"u_primary_tel_confirmed": tel.t_confirmed,
		             })
		             .into("users")
					 .whereRaw("u_id=?",[account.u_id])

                })


          }

    	return account
    }


    return re
}
