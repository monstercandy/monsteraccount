var lib = module.exports = function(knex) {

    const KnexLib = require("MonsterKnex");

	var vali = require("MonsterValidators").ValidateJs()
    const MError = require('MonsterError')


      vali.validators.trLowerCase = function(value, options, key, attributes, glob) {
          if(!value) return
          value = attributes[key] = value.toLowerCase();
          return;
      }

	var re = {}
    var Common = require("models/common.js")
    const moment = require("MonsterMoment")

    const defaultEmailCategories = ["TECH","BILLING","PROMOTION","OTHER"]

    re.GetEmailCategories = function(){
    	return defaultEmailCategories

    }

    re.Validate = function(in_email_data) {
	    return vali.async(in_email_data, {
	      "e_email": {presence: true, trLowerCase: true, email: true},
        "e_confirmed": {isBoolean: true},
        "e_primary": {isBoolean: true},
	      "e_categories": {default: defaultEmailCategories, inclusionCombo: defaultEmailCategories, isArray: {minLength: 1}},
	    })

    }

    re.Insert = function(uid, in_email_data){
        var email_data
    	return re.Validate(in_email_data)
    	.then(aEmail_data=>{
    		email_data = aEmail_data

			return knex.select().from("emails")
			   .whereRaw("e_user_id=? AND e_email=?", [uid, email_data["e_email"]])

        })
    	.then(rows=>{
    		if(rows.length != 0) throw new MError("EMAIL_ALREADY_EXISTS")

    		if(!email_data["e_primary"]) return Promise.resolve([])

            //we check if the current user already has a primary email set
			return knex.select().from("emails")
			   .whereRaw("e_user_id=? AND e_primary=1", [uid])

        })
    	.then((rows)=>{

    		if(rows.length != 0) throw new MError("EMAIL_ALREADY_EXISTS")

    	   email_data["e_categories"] = Common.ForceEnumerableToString(email_data["e_categories"])

           email_data["e_user_id"] = uid

           email_data["e_email_stripped"] = lib.emailStrip(email_data["e_email"])

	       if(email_data["e_confirmed"])
	             email_data["e_confirmed_at"] = moment().toISOString()

	       return knex
	         .insert(email_data)
	         .into("emails")

    	})


	}

	re.SetEscalate=function(in_data) {
		var e_email_stripped = lib.emailStrip(in_data["e_email"]);
		return knex("emails").update({e_escalate: in_data.e_escalate ? true : false}).whereRaw("e_email_stripped=?",[e_email_stripped]).return();
	}

    re.ListBy = function(filters, account, joinAccount) {
      return vali.async(filters, {
         e_user_id: {},
         e_email: {},
         e_email_stripped: {},
         e_confirmed: {},
         e_categories: {},
         e_escalate: {},
         e_primary: {},
      })
      .then((d)=>{

        Array("e_email","e_email_stripped").forEach(x=>{
           if(!d[x]) return;
           d["~"+x] = d[x];
           delete d[x];
        });

        var f = KnexLib.filterHash(d);
        var r = knex.select().from("emails")

        if(joinAccount)
            r = r.leftJoin("users", "u_id", "e_user_id")

        r = r.whereRaw(f.sqlFilterStr, f.sqlFilterValues)
        return r;
      })
       .then(rows=>{
           return emailRowsToObject(account, rows)
       })
    }

	re.FetchAccountEmails = function(account, onlyEmail, onlyCategory) {

		var base = {e_user_id: account["u_id"]};
		var d = extend({}, base);

		if(onlyEmail){
			d.e_email_stripped = lib.emailStrip(onlyEmail);
		}
		if(onlyCategory){
			d.e_confirmed=1;
			d["~e_categories"] = '% '+onlyCategory+' %';
		}

		return re.ListBy(d, account)
		   .then(rows=>{
		   	   if((rows.length == 0)&&(!onlyEmail)&&(onlyCategory)) {
		   	   	 // if a specific category was requested but we did not find any emails then we return the primary email.

					return re.ListBy(extend(base, {e_primary:1}), account)
		   	   }
		   	   else
		   	   	  return rows
		   })
	}

    function emailRowsToObject(account, rows) {
    	"use strict"


    	for(let email of rows) {

            email.e_categories = Common.ForceEnumerableToList(email.e_categories)

    		  email.Cleanup = function(){
    		  	  delete email.e_id;
    		  	  delete email.e_token;
    		  	  return email;
    		  }

    		  email.SetCategories = function(in_data) {
				    return vali.async(in_data, {
				      "e_categories": {presence: true, inclusionCombo: defaultEmailCategories, isArray: {minLength: 1}},
				    })
				    .then(email_data=>{
				    	email_data["e_categories"] = Common.ForceEnumerableToString(email_data["e_categories"])

		                return knex.update(email_data)
		                     .into("emails")
							 .whereRaw("e_id=?", [email.e_id])

				    })
    		  }

    		  email.GenerateNewToken = function(fakeToken){

    		  	if(fakeToken) return Promise.resolve("")

                var token
                const Token = require("Token")
      	        return Token.GetTokenAsync(16)
		           .then((aToken)=>{
		                token = aToken

		                return knex.update({e_token: token, e_token_generated_at: moment.now()})
		                     .into("emails")
							 .whereRaw("e_id=?", [email.e_id])
							 /*
		                     .where("e_id" ,"=", email.e_id)
							 */
                   })
                   .then(()=>{
                   	  email.e_token = token
                   	  return Promise.resolve(token)
                   })

    		  }

              email.SendTemplate = function(app, template, skipTokenGeneration, cc, uri_op, req) {

                   return email.GenerateNewToken(skipTokenGeneration)
			           .then((token)=>{
			               var mailer = app.GetMailer()


	                       app.InsertEvent(req, {e_event_type: "email-notification", e_other: {"e_email": email["e_email"], "template": template}, e_username: account.u_primary_email, e_user_id: account.u_id})

			               var x = {}
			               x.to = email["e_email"]
			               if(cc)
			               	 x.cc = cc
			               x.template = template
			               x.locale = account.u_language
			               x.context = {"user":account, "email": email, "validity_hours": Math.round(app.config.get("email_token_validity") / 3600) }
			               if(uri_op) {
			               	 var uri = "/index.php?op=ng&amp;state="+uri_op;
			               	 uri += "&amp;account_id="+encodeURIComponent(account.u_id)
			               	 uri += "&amp;email="+encodeURIComponent(email.e_email)
			               	 uri += "&amp;confirmation_token="+encodeURIComponent(email.e_token)
			               	 x.context.uri = uri
			               }

			               return mailer.SendMailAsync(x)

			           })

              }

              email.SendForgotten = function(app, req) {

              	   return email.SendTemplate(app, "account/forgotten_password", false, null, "mc-change-password&amp;forgotten=1", req)
              }
              email.SendVerification = function(app, req) {

              	   return email.SendTemplate(app, "account/email_confirmation", false, null, "mc-email-verify", req)
              }

              email.SendRegistration = function(app, req) {

              	   return email.SendTemplate(app, "account/registration", false, null, "mc-email-verify", req)
              }
              email.SendPrimaryChanged = function(app, req) {

              	   return email.SendTemplate(app, "account/primary_email_change_notification", true, account.old_primary_email, req)
              }
              email.Delete = function(){

              	  if(email.e_primary) throw new MError("PRIMARY_EMAIL_CANNOT_BE_DELETED")

                  return knex("emails")
					 .whereRaw("e_id=?", [email.e_id])
					 /*
                     .where("e_id" ,"=", email.e_id)
					 */
					 .del()
              }



              email.Verify = function(data, app, req){

              	   return new Promise(function(resolve,reject){

			           if((!email.e_token)||(!email.e_token_generated_at)) reject(new MError("NO_EMAIL_TOKEN"))

			           if(email.e_token_generated_at < app.getOldestPossibleEmailToken()) reject(new MError("EMAIL_TOKEN_TOO_OLD"))

			           if(email.e_token != data["confirmation_token"]) reject(new MError("TOKEN_MISMATCH"))

			           resolve()
              	   })
              	   .then(()=>{
				           return knex.update({e_token: "", e_confirmed: true, e_confirmed_at: moment.now()})
				                 .into("emails")
				           .whereRaw("e_id=?", [email.e_id])
				           /*
				                     .where("e_id", email.e_id)
				           */

              	   })
		           .then(()=>{
		           	  if(email.e_primary)
		           	     return account.SetPrimaryEmailToVerified()

                      app.InsertEvent(req, {e_event_type: "email-verify", e_other: {"e_email": email.e_email}, e_username: account.u_primary_email, e_user_id: account.u_id})

		           	  return Promise.resolve()
		           })

              }

    	}

        Common.AddCleanupSupportForArray(rows)

        return Promise.resolve(rows)

    }



    return re



}


lib.emailStrip=function(email){
	return email.replace(".","")
}
