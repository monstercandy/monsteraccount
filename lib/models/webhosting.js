module.exports = function(knex) {

	var vali = require("MonsterValidators").ValidateJs()
    const MError = require('MonsterError')

	var re = {}
    var Common = require("models/common.js")
    var Server = require("models/server.js")

    re.Insert = function(account_id, webhostingData) {


      var wh
      return vali.async(webhostingData, {
         "w_server_name": Common.serverNameEnforcements,
         "w_webhosting_id": {presence: true, isInteger: {lazy:true}},
      })
      .then(aWh=>{
      	   wh = aWh

           wh.w_user_id = account_id

          var q = knex.select("w_id").from("webhostings")
             .whereRaw("w_user_id=? AND w_server_name=? AND w_webhosting_id=?", [account_id, wh.w_server_name, wh.w_webhosting_id])
          return q

      })
      .then((rows)=>{
          if(rows.length > 0)
             return Promise.resolve([rows[0].w_id])

           return Server(knex).FetchServer(wh.w_server_name)
             .then(x=>{
                 if(x.s_roles.indexOf("webhosting") <= -1) throw new MError("SERVER_IS_NOT_WEBHOSTING_CAPABLE")

                 return knex.insert(wh).into("webhostings", "w_id")

             })

       })
       .then((wh_ids)=>{
          wh.w_id = wh_ids[0]

          // why did we resolve this?
          return Promise.resolve()
       })

    }

  re.GetCountByServerName = function(server_name) {
    return knex("webhostings").count("* AS c")
       .whereRaw("w_server_name=?", server_name)           
       .then(rows=>{
           return Promise.resolve(rows[0].c)
       })
  }

	re.GetAccountWebhostings = function(account, server_name, webhosting_id) {

		var queryConds = ["w_user_id=?"]
		var queryParams = [account["u_id"]]

    if(server_name) {
       queryConds.push("w_server_name=?")
       queryParams.push(server_name)
    }
    if(webhosting_id) {
       queryConds.push("w_webhosting_id=?")
       queryParams.push(webhosting_id)
    }

    var queryStr = queryConds.join(" AND ")

    // console.log(queryStr, queryParams)

		return knex.select().from("webhostings")
		   .whereRaw(queryStr, queryParams)           
		   .then(rows=>{
		   	   return webhostingRowsToPromise(account, rows)
		   })
	}

  function webhostingRowFunctions(account, webhosting){
              webhosting.Cleanup = function() {
                 delete webhosting.w_id
                 delete webhosting.w_user_id
                 return webhosting
              }


              webhosting.Delete = function(){

                  return knex("webhostings")
                     .whereRaw("w_id=?", [webhosting.w_id])
                     .del()
              }

              return webhosting

  }

    function webhostingRowsToPromise(account, rows) {
    	"use strict"

    	for(let webhosting of rows) {

         webhostingRowFunctions(account, webhosting)

    	}

      Common.AddCleanupSupportForArray(rows)


        return Promise.resolve(rows)

    }


    return re

}

