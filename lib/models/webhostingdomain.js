module.exports = function(knex) {

	var vali = require("MonsterValidators").ValidateJs()
    const MError = require('MonsterError')

	var re = {}
    var Common = require("models/common.js")


    re.Insert = function(domainData) {

      var dom

      // helper feature for easy wie restoration
      if((!domainData.domain)&&(domainData.wd_domain_canonical_name)) {
         domainData.domain = domainData.wd_domain_canonical_name;        
      }

      return validate(domainData)
      .then(aDom=>{
      	  dom = aDom

          return GetWebhostingDomain(dom.domain.d_domain_canonical_name, dom.wd_server, dom.wd_webhosting)
      })
      .then((rows)=>{
          if(rows.length > 0)
             return Promise.resolve([rows[0].wd_id])

           // we still need to check whether an entry already exists
           return GetWebhostingDomainWoWebhosting(dom.domain.d_domain_canonical_name, dom.wd_server) // without webhosting id
             .then(function(rows){
                 if(dom.wd_webhosting) {
                    // the user specified a webhosting id in this new request. lets see if there are any matches which do not have a webhosting id

                    var match;
                    rows.some(row=>{
                      if(!row.wd_webhosting) {
                         match = row;
                         return true;
                      }
                    })

                    if(match) {
                       // this match needs to be removed/upgraded; sending the promise into the background
                       match.Delete().catch(ex=>{
                         console.error("We were unable to delete this match", ex);
                       })
                    }

                 }
                 else {
                    // the user did not specify any webhosting id in this request. lets see if there are any matches which do have a webhosting id already
                    var match;
                    rows.some(row=>{
                      if(row.wd_webhosting) {
                         match = row;
                         return true;
                      }
                    })

                    // there was a match, we dont need to insert anything
                    if(match)
                      return [match.wd_id];
                 }

                 dom.wd_domain_canonical_name = dom.domain.d_domain_canonical_name;
                 dom.wd_domain_name = dom.domain.d_domain_name;
                 delete dom.domain;
                 return knex.insert(dom).into("webhostingdomains", "wd_id")
             })

       })
       .then((d_ids)=>{
          var d_id = d_ids[0]

          return d_id;
       })

    }

  re.GetWebhostingDomain = function(domainData) {
     return validate(domainData)
       .then(dom=>{
          return GetWebhostingDomain(dom.domain.d_domain_canonical_name, dom.wd_server, dom.wd_webhosting)
       })
       .then(rows=>{
          var wd = Common.EnsureSingleRowReturned(rows, "DOMAIN_NOT_FOUND");
          return wd;
       })
  }

  function validate(domainData){
      return vali.async(domainData, {
         "domain": {presence: true, isDomain: true},
         "wd_server": Common.serverNameEnforcements,
         "wd_webhosting": {isInteger: {lazy:true}, default: {value: 0}},
      })    
  }

  function GetWebhostingDomainWoWebhosting(canon, server){
     return knex.select().from("webhostingdomains").whereRaw("wd_server=? AND wd_domain_canonical_name=?", [server, canon])
       .then(rows=>{
           return domainRowsToObject(rows)
       });    
  }
  function GetWebhostingDomain(canon, server, wh_id) {
     if(!wh_id) wh_id = 0;
     return knex.select().from("webhostingdomains").whereRaw("wd_server=? AND wd_webhosting=? AND wd_domain_canonical_name=?", [server, wh_id, canon])
       .then(rows=>{
           return domainRowsToObject(rows)
       });
  }

  function validateByWebhosting(in_data){
     return vali.async(in_data, {
        wd_server: {presence: true, isString: true},
        wd_webhosting: {presence: true, isInteger: {lazy:true}},
     })    
  }
  re.GetByWebhosting = function(in_data) {
     return validateByWebhosting(in_data)
     .then(d=>{
        return knex.select().from("webhostingdomains").whereRaw("wd_server=? AND wd_webhosting=?", [d.wd_server, d.wd_webhosting])      
     })
     .then(rows=>{
         return domainRowsToObject(rows)
     });
  }
  re.DeleteByWebhosting = function(in_data) {
     return validateByWebhosting(in_data)
     .then(d=>{
        return knex("webhostingdomains").whereRaw("wd_server=? AND wd_webhosting=?", [d.wd_server, d.wd_webhosting]).delete().return();
     });
  }

  re.GetAll = function() {
     return knex.select().from("webhostingdomains")
       .then(rows=>{
           return domainRowsToObject(rows)
       });
  }
  re.GetWebhostingDomainsByAccount = function(account) {
     return knex.select("webhostingdomains.*").from("domains").leftJoin("webhostingdomains", "webhostingdomains.wd_domain_canonical_name", "domains.d_domain_canonical_name")
       .whereRaw("d_user_id=? AND wd_id IS NOT NULL", [account.u_id])
       .then(rows=>{
           return domainRowsToObject(rows)
       });
  }
  re.GetWebhostingDomainsByWebhosting = function(server, wh_id) {
     return knex.select().from("webhostingdomains")
       .whereRaw("wd_server=? AND wd_webhosting=?", [server, wh_id])
       .then(rows=>{
           return domainRowsToObject(rows)
       });
  }


  function domainRowFunctions(domain){
        domain.Cleanup = function() {
           delete domain.wd_id
           return domain
        }


        domain.Delete = function(){

            return knex("webhostingdomains")
             .whereRaw("wd_id=?", [domain.wd_id])
             .del()
        }

        domain.Move = function(data){
            return knex("webhostingdomains")
             .whereRaw("wd_id=?", [domain.wd_id])
             .update({wd_webhosting: data.dest_wh_id})             
        }

        return domain
  }

    function domainRowsToObject(rows) {
    	"use strict"

    	for(let domain of rows) {

         domainRowFunctions(domain)

    	}

      Common.AddCleanupSupportForArray(rows)


        return Promise.resolve(rows)

    }


    return re

}

