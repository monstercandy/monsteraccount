module.exports = function(knex, app) {

	var vali = require("MonsterValidators").ValidateJs()
    const MError = require('MonsterError')

	var re = {}
    var Common = require("models/common.js")


    re.Insert = function(account_id, domainData) {

      var dom
      return vali.async(domainData, {
         "domain": {presence: true, isDomain: true},
      })
      .then(aDom=>{
      	   dom = aDom

           dom.domain.d_user_id = account_id

          var q = knex.select("d_id").from("domains")
             .whereRaw("d_user_id=? AND d_domain_canonical_name=?", [account_id, dom.domain.d_domain_canonical_name])
          return q

      })
      .then((rows)=>{
          if(rows.length > 0)
             return Promise.resolve([rows[0].d_id])

           return knex.insert(dom.domain).into("domains", "d_id")

       })
       .then((d_ids)=>{
          dom.domain.d_id = d_ids[0]

          // why did we resolve this?
          return domainRowFunctions(null, dom.domain);
       })

    }

  re.ThrowWhenBareDomainIsPresentForOthers=function(account_id, wd_domain_name) {
     return Promise.resolve()
       .then(()=>{
           var bareDomain = app.getBareDomain(wd_domain_name);
           // console.log("bare domain is", wd_domain_name, "x", bareDomain)
           if(!bareDomain) throw new MError("INVALID_DOMAIN_SYNTAX");

           return knex("domains").select().whereRaw("d_user_id!=? AND d_domain_canonical_name=?", [account_id, bareDomain])           
       })
       .then(rows=>{
          if(rows.length > 0) throw new MError("DOMAIN_ALREADY_PRESENT");
       })
  }

	re.GetAccountDomains = function(account, onlyDomainId, onlyDomainName) {

		var queryStr = "d_user_id=?"
		var queryParams = [account["u_id"]]
		if(onlyDomainId){
			queryStr += " AND d_id=?"
			queryParams.push(onlyDomainId)
		}
    if(onlyDomainName){
      queryStr += " AND (d_domain_name=? OR d_domain_canonical_name=?)"
      queryParams.push(onlyDomainName)
      queryParams.push(onlyDomainName)
    }

		return knex.select().from("domains")
		   .whereRaw(queryStr, queryParams)           
		   .then(rows=>{
		   	   return domainRowsToObject(account, rows)
		   })
	}

  function domainRowFunctions(account, domain){
              domain.Cleanup = function() {
                 delete domain.d_id
                 delete domain.d_user_id
                 return domain
              }


              domain.Delete = function(){

                  return knex("domains")
           .whereRaw("d_id=?", [domain.d_id])
           .del()
              }

              return domain

  }

    function domainRowsToObject(account, rows) {
    	"use strict"

    	for(let domain of rows) {

         domainRowFunctions(account, domain)

    	}

      Common.AddCleanupSupportForArray(rows)


        return Promise.resolve(rows)

    }


    return re

}

