
var fs = require("fs")

module.exports = function(router, app) {
   
   var config = app.config
   const path = require("path")
   const fs = require("fs")
   const MError = require('MonsterError')
   const Validators = require('MonsterValidators')

	router.post( "/import/tgz", function (req, res, next) {

    return preRequisites()
      .then(()=>{

          "use strict"

          app.InsertEvent(req, {e_event_type: "data-import"})


          var streams = require("MonsterStreams")

          return new Promise(function(resolve,reject){

                let buf = streams.Base64ToBuffer(req.body.json.database)

                let tgzInput = streams.ExtractTgz(config.get("db").connection.filename)
                let tarStream = tgzInput.tarStream

                var bs = new streams.BufferStream(buf)
                bs.pipe(tgzInput)

                tgzInput.on("error", function(err){
                      reject(err)
                })


                tarStream.on("finish", function(){
                      resolve()
                })

            })
            .catch((ex)=>{
               throw new MError("ERROR_WHILE_EXTRACTING", null, ex)

            })          

      })
      .then(()=>{

         // "invalid": filesInvalid,
         req.result = "ok"

         next()

      })


	})



	router.get( "/export/tgz", function (req, res, next) {


      return preRequisites()
        .then(()=>{
            var streams = require("MonsterStreams")

              app.InsertEvent(req, {e_event_type: "data-export"})

              streams.StreamToBase64( streams.PackTgz(config.get("db").connection.filename), function(b64) {
                 req.result = {"database": b64}
                 next()

            })

        })

	});

  function preRequisites() {
    return new Promise(function(resolve, reject){
       if((!config.get("db")) || (!config.get("db").connection) || (!config.get("db").connection.filename))
         reject(new MError("IMPORT_EXPORT_FOR_SQLITE_ONLY"))
       resolve()
    })

  }


}
