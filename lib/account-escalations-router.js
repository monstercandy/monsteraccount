(function(){

  var lib = module.exports = function(router, app) {

    const Tel = require("models/tel.js")
    const Email = require("models/email.js")

    const moment = require("MonsterMoment");
    const dotq = require("MonsterDotq");

    const escalate_sms_interval_seconds = app.config.get("escalate_sms_interval_seconds");
    const escalation_interval_second = app.config.get("escalation_interval_second");
    const escalation_email_same_event_type_delay_second = app.config.get("escalation_email_same_event_type_delay_second");
    const sms_exceptions = app.config.get("escalate_exceptions_via_sms");
    const silent_hours = app.config.get("escalate_sms_silent_hours");

    const vali = require("MonsterValidators").ValidateJs();

    if(!app.exceptionsToEscalate)
       app.exceptionsToEscalate = {};

    if(!app.exceptionsEmailDelayCache)
      app.exceptionsEmailDelayCache = {};

    router.post("/escalations/process", function(req,res,next){
       var re = {};
       return process(re)
         .then(()=>{
            return req.sendResponse(re);
         })
    })
    router.put("/escalations/", function(req,res,next){
       return vali.async(req.body.json, {server: {presence: true, isString: true}, events:{presence:true, isArray:true}})
         .then(d=>{

             req.sendOk();

             d.events.forEach(event=>{
                if(!app.exceptionsToEscalate[event.type])
                   app.exceptionsToEscalate[event.type] = {};
                if(!app.exceptionsToEscalate[event.type][d.server])
                  app.exceptionsToEscalate[event.type][d.server] = [];

                event.received = moment.now();

                app.exceptionsToEscalate[event.type][d.server].push(event);
                delete event.type;
             });


         })
    })

    if(!app.escalationTimer)
       reschedule();

    var lastSms;

    function process(status) {
          status.events = 0;
          return Promise.resolve()
            .then(()=>{
               var now = moment();

               var smsEvents = getEventsToDispatchViaSms(now);
               // this is done as second, as getEventsToDispatchViaEmail modifies the source
               var mailEvents = getEventsToDispatchViaEmail(now);


               if(smsEvents) {
                  status.events += Object.keys(smsEvents).length;
                  sendSms(smsEvents, now);
               }

               if(mailEvents) {
                  status.events += Object.keys(mailEvents).length;
                  return sendEmail(mailEvents, now);
               }

               return;
            })
    }

    function reschedule(){
       app.escalationTimer = setTimeout(function(){
          var status = {};
          return process(status)
            .catch(ex=>{
               console.error("Couldnt escalate exceptions", ex);
            })
            .then(()=>{
               reschedule();
            })

       }, escalation_interval_second*1000);
    }

    function getEventsToDispatchViaEmail(now){
        var before = moment().add(-1*escalation_email_same_event_type_delay_second, "second");
        return lib.getEventsToDispatchViaEmail(app, before, now);
    }

    function getEventsToDispatchViaSms(now){
      if((isSilentHours(now))||(!wasLastSmsSentOutOfInterval()))
          return;

      var re = {};
      Object.keys(app.exceptionsToEscalate).forEach(function(eventType){
          if(sms_exceptions.indexOf(eventType) < 0) return;

          var serverNames = Object.keys(app.exceptionsToEscalate[eventType]);
          re[eventType] = (re[eventType]||[]).concat(serverNames);
      });

      if(!Object.keys(re).length) return;

      return re;
    }

    function isSilentHours(now){
       var h = now.hours();
       return (silent_hours.indexOf(h) > -1) ;

    }

    function wasLastSmsSentOutOfInterval(){
       if(!lastSms) return true;
       var before = moment().add(-1*escalate_sms_interval_seconds, "second");
       return lastSms.isBefore(before);
    }

    function sendEmail(eventsByType){
        const Email = require("models/email.js")
        return Email(app.knex).ListBy({e_escalate:true}, null, true)
          .then(rows=>{

               var mailer = app.GetMailer()

               var x = {}
               x.template = "misc/escalate-batch";

               var seenAlready = {};
               return dotq.linearMap({array: rows, action: function(rcpt){
                  if(seenAlready[rcpt.e_email]) return;
                  seenAlready[rcpt.e_email] = true;

                  return dotq.linearMap({array: Object.keys(eventsByType), catch: true, action: function(eventType){
                     var events = eventsByType[eventType]
                     x.context = {type: eventType, events:events};
                     x.to = rcpt.e_email;
                     x.locale = rcpt.u_language;

                     return mailer.SendMailAsync(x)
                  }});

               }});

          });
    }

    function groupByType(events){
        var re = {};
        Object.keys(events).forEach(server=>{
           Object.keys(events[server]).forEach(eventType=>{
              if(!re[eventType]) re[eventType] = {};

              re[eventType][server] = (re[eventType][server] || []).concat(events[server][eventType]);
           })
        })
        return re;
    }

    function sendSms(events, now){
        lastSms = now;

        var sms = app.GetSmsLib();
        var msg = getTextMessageShort(events);

        const Tel = require("models/tel.js")
        return Tel(app.knex).ListBy({t_escalate:true})
          .then(rows=>{

             var seenAlready = {};

             return dotq.linearMap({array: rows, action: function(rcpt){
                if(seenAlready[rcpt.t_human]) return;
                seenAlready[rcpt.t_human] = true;

                var d = {
                   s_country_code: rcpt.t_country_code,
                   s_area_code: rcpt.t_area_code,
                   s_phone_no: rcpt.t_phone_no,

                   s_message: msg,
                }
                return sms.Insert(d);
             }})

          });
    }

    function getTextMessageShort(events){
       var r = [];
       Object.keys(events).forEach(eventType=>{
          r.push( eventType+": "+ events[eventType].join(", ") );

       })
       return r.join("; ");
    }


}


lib.getEventsToDispatchViaEmail = function(app, before, now) {

        var re = {};

        Object.keys(app.exceptionsToEscalate).forEach(function(eventType){
            var wasSentLast = app.exceptionsEmailDelayCache[eventType];
            if(wasSentLast){
                if(wasSentLast.isAfter(before))
                  return;
            }

            app.exceptionsEmailDelayCache[eventType] = now;

            // this can be/should be escalated.
            re[eventType] = app.exceptionsToEscalate[eventType];
            delete app.exceptionsToEscalate[eventType];
        });

      if(!Object.keys(re).length) return;

      return re;

}


})()
