module.exports = function(router, app) {

    router.put("/loginlog/events", function(req,res,next){
       return req.sendOk(app.getLoginlog().Insert(req.body.json));
    })

      router.post("/loginlog/count", doCount);
      router.post("/loginlog/search", doSearch)


    router.post("/account/:account_id/loginlog/count", forceUserId, doCount)

    router.post("/account/:account_id/loginlog/search", forceUserId, doSearch)

    function doCount(req,res,next){
         return req.sendPromResultAsIs(app.getLoginlog().Count(req.body.json))
    }

    function doSearch(req,res,next){
         return req.sendPromResultAsIs(
            app.getLoginlog().Query(req.body.json)
          )
    }

    function forceUserId(req,res,next){
       req.body.json.l_user_id = req.params.account_id;
       return next();
    }



}
