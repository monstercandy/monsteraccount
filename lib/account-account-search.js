module.exports = function(router, knex, vali) {

    function accountQueryParams(req){
          return vali.async(req.body.json, {
               "limit": {default: 100, numericality:{onlyInteger: true, strict: true, lessThanOrEqualTo: 100}},
               "offset": {default: {value:0}, numericality:{onlyInteger: true, strict: true}},
               "order": {inclusion: ["u_id","u_name","u_primary_email","u_primary_tel","u_comment","created_at"], default: "u_name"},
               "desc": {isBoolean: true},
               "where": {isString: {trim:true}},
               "deleted": {isBooleanLazy: true},
          })  
    }
    function accountQueryWhere(d, knex){
       var qStr = "1"
       var qArr = []
       if(d.where) {
         whereStr = "%"+d.where+"%"
         qStr += " AND (u_name LIKE ? OR u_comment LIKE ? OR u_primary_email LIKE ? OR u_primary_tel LIKE ?)"
         qArr.push(whereStr,whereStr,whereStr,whereStr)
       }
       if(typeof d.deleted !== "undefined") {
         qStr += " AND u_deleted=?"
         qArr.push(d.deleted ? 1 : 0)
       }
       // console.error("FOO!", d, qStr, qArr)
       return knex.whereRaw(qStr, qArr)
    }

    router.post("/accounts/count", function (req, res, next) {
           return accountQueryParams(req)
              .then(d=>{
                 var s = accountQueryWhere(d, knex('users').count("*"))
                 // console.log(s.toString())
                 return s
              })
              .then(function(rows) {
                req.result = {"count": rows[0]["count(*)"] }
                next()
              })

      })

    router.post("/accounts/search",
      function (req, res, next) {

          return accountQueryParams(req)
          .then(d=>{
             // console.log(d)
             var s = accountQueryWhere(d,knex.select()
               .table('users')
               .limit(d.limit)
               .offset(d.offset)
               .orderBy(d.order, d.desc?"desc":undefined))
             // console.log(s.toString())
             return s
          })
          .then(function(rows) {
            req.result = {"accounts": rows }
            next()
          })
           
      })

}
