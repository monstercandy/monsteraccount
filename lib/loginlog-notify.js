module.exports = function(knex, app) {


    if(!app.GetMailer) throw new Error("GetMailer not provided")


    var AccountLib = require("models/account.js")(knex)

    var re = {};
    re.doWork = function(successful, mail_template){


      return knex("loginlog").distinct("l_user_id").whereRaw("l_processed=0 AND l_successful=?", [successful])
        .then(function(distinctRows){
           "use strict"


          var ps = []
          // Using Promise.map:
          distinctRows.forEach(line=>{

              // console.log("this is a row from a promise", row)

              var p = Promise.resolve()

              if(line.l_user_id) {
                 var account
                 var emails
                 p = p.then(function(){
                     return AccountLib.GetAccountById(line.l_user_id)
                 })
                 .then(aaccount=>{
                     account = aaccount

                     if(!account.IsNotificationEmailWanted(successful)) return Promise.resolve([])

                     return account.FetchEmailsOnly(null, "TECH")
                 })
                 .then(aemails=>{
                     emails = aemails
                     if(emails.length <= 0) return Promise.resolve()

                     var p
                       if(successful) {
                          p = knex("loginlog").select().whereRaw("l_user_id=? AND l_processed=0 AND l_successful=1 AND l_event_type!='superuser-switch-user'", [line.l_user_id])
                            .then(rows=>{
                              //account.u_email_login_ok_subsystems
                                var re = rows.filter(x=>{
                                   // console.log("shit", x.l_subsystem, account.u_email_login_ok_subsystems.indexOf(x.l_subsystem))
                                   return account.u_email_login_ok_subsystems.indexOf(x.l_subsystem) > -1
                                })
                                // console.log("returned!", account.u_email_login_ok_subsystems, rows, re)

                                return re;
                            })

                       } else {

                         p = knex("loginlog").count("* AS c").min("l_timestamp AS mints").max("l_timestamp AS maxts").select("l_timestamp", "l_ip", "l_agent", "l_country", "l_subsystem", "l_username" )
                            .whereRaw("l_user_id=? AND l_processed=0 AND l_successful=0",[line.l_user_id])
                            .then(rows=>{
                               // moment.ApplyTimezoneOnArrayOfHashes(rows, ["mints", "maxts", "e_timestamp"], d.result_apply_timezone)

                               return rows[0];
                            })

                       }

                      return p
                 })
                 .then(data=>{
                     // console.log("email sending", emails, data)
                     if(emails.length <= 0) return;
                     if(data.length <= 0) return;

                     console.log("sending loginlog notification emails to", emails)

                     var mailer = app.GetMailer()

                     var x = {}
                     x.to = emails
                     x.template = mail_template
                     x.locale = account.u_language
                     x.context = {"user":account.Cleanup(),  data: data, }
                     x.context.timezone = "" // timezone: line.e_user_timezone // for display purposes

                     return mailer.SendMailAsync(x)
                 })
              }



              p.then(function(){
                 // console.log("flipping processed");
                 return knex('loginlog').update({'l_processed':1}).whereRaw("l_user_id=? AND l_successful=?", [line.l_user_id, successful]).return()
              })


              ps.push(p)
          })


          return Promise.all(ps)
       });
    }


    re.Start = function(){
         if(!app.config.get('loginlog_active')) return 0

         if(app.loginLogNotify) return;
         app.loginLogNotify = true;

         var croner = require("Croner");
         Array(
            {successful:0, cron_key: 'cron_loginlog_notify_unsuccessful', mail: 'account/loginlog-unsuccessful'},
            {successful:1, cron_key: 'cron_loginlog_notify_successful', mail: 'account/loginlog-successful'}
         ).forEach(x=>{
            var csp = app.config.get(x.cron_key);
            if(!csp) return;

             croner.schedule(csp, function(){
                return re.doWork(x.successful, x.mail);
             });


         })

    }


    return re;

}
