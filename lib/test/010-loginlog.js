require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var TestCommonLib = require("test/000-common.js")

var mockedCountResponse = 9921312

var mockedEventClient = {postParams:[]}
mockedEventClient.LogAsync = function(d) {
    console.log("remocked event client lib!", d)
    if(d.e_event_type == "auth-credential")
       mockedEventClient.logCredential++

   return Promise.resolve()
}
mockedEventClient.CountAsync = function(d) {
    console.log("CountAsync was called", d)
    mockedEventClient.countParams = d
    return Promise.resolve({"count":mockedCountResponse})
}
mockedEventClient.QueryAsync = function(d) {
    console.log("QueryAsync was called", d)
    mockedEventClient.queryParams = d
    return Promise.resolve({"events":[{e_ip: '::ffff:127.0.0.1', e_other: " ACCOUNT_OWNER "}]})
}

var app = require( "../account-app.js")({EventClientLib: function(){
    return mockedEventClient
}})

require("ExpressTester")(app, function(mapi, assert){


    var userIds = {}
    var TestCommon = TestCommonLib(mapi, assert, userIds)
    var email = "loginlog@email.ee"

    describe('prepare', function() {

        TestCommon.CreateAccount({e_email:email,u_email_login_ok: true, u_email_login_fail: true}, "l")

    })

	describe('loginlog tests', function() {

        it('login should trigger auth-credential logging', function(done) {

             mockedEventClient.logCredential = 0
             mapi.post( "/authentication/credential", {username:email,password:TestCommon.DefaultPassword}, function(err, result, httpResponse){
                TestCommon.ValidTokenResponse(err, result, httpResponse, "main")

                assert.equal(mockedEventClient.logCredential, 1)

                done()
             })
        })


        it('number of loginlog entries (should be relayed to event server)', function(done) {

             mapi.post( "/loginlog/count", {"l_user_id": userIds.l, "e_event_type": "fooo!"}, function(err, result, httpResponse){

                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)
                assert.propertyVal(result, "count", 0 )
                done()

             })
        })

        it('loginlog entries (response from the event server)', function(done) {

             mapi.post( "/loginlog/search", {"l_user_id": userIds.l}, function(err, result, httpResponse){

                console.log("shit", err, result)

                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)
                assert.property(result, "events")
                assert.equal(Array.isArray(result.events), true)
                assert.equal(result.events.length, 0)
                done()

             })
        })

        const userEvent = {
                        l_id: 'foo',
                        l_user_id: 'bar',
                        l_timestamp: "2017-09-14T18:58:13Z",
                        l_username: 'foobar@foo.com',
                        l_ip: '::ffff:127.0.0.1',
                        l_subsystem: 'accountapi',
                        l_event_type: 'auth-credential',
                        l_agent: 'Firefox',
                        l_successful: 1,
                        l_other: '{"foo":"bar"}',
                        l_country: 'hu'
                      }

        it('uploading some events', function(done) {

            // this one should be a real user id  so that later the notifier can work
            userEvent.l_user_id = userIds.l;

             mapi.put( "/loginlog/events", {"server": "server", events: [userEvent]}, function(err, result, httpResponse){

                assert.equal(result, "ok")
                done()

             })
        })


        it('loginlog entries via account route should enforce filtering', function(done) {

             mapi.post( "/account/bar/loginlog/search", {}, function(err, result, httpResponse){

                assert.isNull(err);
                assert.equal(Array.isArray(result.events), true);
                assert.equal(result.events.length, 0);
                done();

             })
        })

        it('loginlog entries (response from the event server)', function(done) {

             mapi.post( "/account/"+userIds.l+"/loginlog/search", {}, function(err, result, httpResponse){

                userEvent.l_server = "server";
                userEvent.l_processed = 0;

                assert.isNull(err);
                assert.equal(httpResponse.statusCode, 200);
                assert.property(result, "events");
                assert.equal(Array.isArray(result.events), true);
                assert.equal(result.events.length, 1);
                assert.deepEqual(result.events[0], userEvent);
                done();

             })
        })


        it('uploading an unsuccessful one as well', function(done) {

            // this one should be a real user id  so that later the notifier can work
            userEvent.l_successful = 0;

             mapi.put( "/loginlog/events", {"server": "server", events: [userEvent]}, function(err, result, httpResponse){

                assert.equal(result, "ok")
                done()

             })
        })

   })



    describe("notifyier tests", function(){


        var sendMailCalls = 0


        var expectedTemplate;
        var expectedSuccessful = true;

        var mockedGetMailer = function(){

            var re = {}
            re.SendMailAsync = function(x){
                // this method will be called only for the loginlog account, since the notification feature is not enabled for any of the others!

                // console.log(x.context)

                assert.propertyVal(x, "template", expectedTemplate)
                assert.property(x, "locale")
                assert.deepEqual(x.to, [ 'loginlog@email.ee' ])

                assert.property(x, "context")
                assert.property(x.context, "data")
                if(expectedSuccessful) {
                    assert.equal(Array.isArray(x.context.data), true)
                    assert.equal(x.context.data.length, 1)
                } else {
                    assert.propertyVal(x.context.data, "c", 1);
                    assert.propertyVal(x.context.data, "mints", '2017-09-14T18:58:13Z');
                    assert.propertyVal(x.context.data, "maxts", '2017-09-14T18:58:13Z');
                    assert.propertyVal(x.context.data, "l_subsystem", "accountapi");
                }

                sendMailCalls++

                return Promise.resolve()
            }

            return re

        }

        var notifierLib = require( "loginlog-notify.js" )

        it("notifier stuffs, successful first", function(){

            var loginLogNotifier = notifierLib(app.knex, {config:app.config, GetMailer: mockedGetMailer})

            sendMailCalls = 0;
            expectedTemplate = 'account/loginlog-successful';
            return loginLogNotifier.doWork(1, expectedTemplate)
              .then(()=>{
                    assert.equal(sendMailCalls, 1)
              })

        })

        it("the second time no emails should be sent", function(){

            var loginLogNotifier = notifierLib(app.knex, {config:app.config, GetMailer: mockedGetMailer})

            sendMailCalls = 0;
            return loginLogNotifier.doWork(1, 'doesnt matter')
              .then(()=>{
                    assert.equal(sendMailCalls, 0)

              })
        })

        it("unsuccessful notifications should work first as well", function(){

            expectedSuccessful = false;
            var loginLogNotifier = notifierLib(app.knex, {config:app.config, GetMailer: mockedGetMailer})

            sendMailCalls = 0;
            expectedTemplate = 'account/loginlog-unsuccessful';
            return loginLogNotifier.doWork(0, expectedTemplate)
              .then(()=>{
                    assert.equal(sendMailCalls, 1)

              })

        })

        it("the second time no emails should be sent", function(){

            var loginLogNotifier = notifierLib(app.knex, {config:app.config, GetMailer: mockedGetMailer})

            sendMailCalls = 0;
            return loginLogNotifier.doWork(0, 'doesnt matter')
              .then(()=>{
                    assert.equal(sendMailCalls, 0)
              })
        })


    })



}, {timeout: 10000, noGetEventMocking: true})

