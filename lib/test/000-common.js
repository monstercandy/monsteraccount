var ore = function(mapi, assert, userIds, tokens) {


   var re = {}

   re.DefaultPassword = "easy1234"

   re.CreateAccountParams = function(params) {
   	    if(typeof params.c_password === "undefined")
   	    	params.c_password = re.DefaultPassword

   	    if(typeof params.u_name === "undefined")
   	    	params.u_name = "u_name"

        if(typeof params.u_language === "undefined")
          params.u_language = "en"

   	    if(typeof params.t_country_code === "undefined")
   	    	params.t_country_code = "36"

   	    if(typeof params.t_area_code === "undefined")
   	    	params.t_area_code = "30"

   	    if(typeof params.t_phone_no === "undefined")
   	    	params.t_phone_no = "1234567"

   	    return params
   }

   re.CredentialAuthShouldWork = function(email, password, tokenName, additionalValidation, additionalInit) {
      it('good password', function(done) {
          if(additionalInit)
            additionalInit()

          mapi.post( "/authentication/credential", {username:email,password:password}, function(err, result, httpResponse){
                re.ValidTokenResponse(err, result, httpResponse, tokenName)

                if(additionalValidation)
                   return additionalValidation(err, result, httpResponse, done)

                done()
          })
      })

   }
   re.RegisterInnerSuccessful = function(app, params, callback) {
      return  re.RegisterInner(app, params, function(err, result, httpResponse){
                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)
                assert.property(result, "u_id" ) // u_id: 4


                callback(err, result, httpResponse)


             })

   }

   re.RegisterInner = function(app, params, callback) {

            var oGetMailer = app.GetMailer
             app.GetMailer = function() {
                 var re = {}
                 re.SendMailAsync = function(data) {
                     assert.propertyVal(data, "template", "account/registration")

                     return Promise.resolve("ok")
                 }
                 return re
             }

             mapi.post( "/accounts/registration", params, function(err, result, httpResponse){

                app.GetMailer = oGetMailer

                callback(err, result, httpResponse)

             })
   }

   re.CreateAccount = function(params, userIdsKey, additionalValidatorCallback) {

        it('creating account '+userIdsKey, function(done) {

             mapi.put( "/accounts", re.CreateAccountParams(params), function(err, result, httpResponse){

                if(err) console.log(err)

                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)
                assert.property(result, "u_id" ) // u_id: 1
                if((userIds)&&(userIdsKey))
                  userIds[userIdsKey] = result.u_id

                if(additionalValidatorCallback) additionalValidatorCallback(err, result, httpResponse)

                done()

             })
        })
   }

   re.DeleteAccount = function(userIds, userIdsKey) {
        it('delete account '+userIdsKey, function(done) {
             mapi.delete( "/account/"+userIds[userIdsKey], {}, function(err, result, httpResponse){
                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)
                assert.equal(result, "ok" )

                done()

             })
        })

   }

  re.TokenTestWithAuthCode= function(grants,tokenName, extraParams, additionalTests){
        it("token test", function(done) {
             var payload = extend({}, extraParams, {token:tokens[tokenName]});
             console.log("sending token test", payload)
             mapi.post( "/authentication/token", payload, function(err, result, httpResponse){
                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)
                assert.deepProperty(result, "session.s_expires" )
                assert.deepProperty(result, "session.s_grants" )
                assert.notProperty(result.session, "s_unique_session_identifier" )

                assert.deepProperty(result, "account.u_primary_email" )
                assert.deepProperty(result, "account.u_id" ) // this one is needed by the relayer API
                assert.deepEqual(result.session.s_grants,  grants)

                if(additionalTests)
                   additionalTests(result)

                done()
             })
        })
  }

  re.TokenTest= function(grants,tokenName, additionalTests){
      re.TokenTestWithAuthCode(grants, tokenName, {}, additionalTests);
  }

  re.ValidTokenResponse=function(err, result, httpResponse, tokenName){
    assert.isNull(err)
    assert.equal(httpResponse.statusCode, 200)
    assert.property(result, "u_name" )
    assert.property(result, "u_primary_email")
    assert.property(result, "token" )
    if((tokenName)&&(tokens))
    	tokens[tokenName] = result.token
  }


   return re
}


module.exports = ore
