require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var TestCommonLib = require("test/000-common.js")

var ExpressTesterLib = require("ExpressTester")
var app = require( "../account-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    var userIds = {};
    var tokens = {};
    var TestCommon = TestCommonLib(mapi, assert, userIds, tokens)

    var main_email = "escalate@escalate.ee";
    var main_telno;

    describe('caching', function() {
        var moment = require("MonsterMoment");
        var escalateLib = require("account-escalations-router.js");
        var before = moment().add(-3600, "second");

        it("should relay when cache is empty", function(){

            var app = {
               exceptionsToEscalate: {
                  "TYPE": ["anything1","anything2"],
               },
               exceptionsEmailDelayCache: {}
            }
            var now = moment();
            var r = escalateLib.getEventsToDispatchViaEmail(app, before, now);

            assert.deepEqual(r, {"TYPE": ["anything1","anything2"]});
            assert.propertyVal(app.exceptionsEmailDelayCache, "TYPE", now);
            assert.deepEqual(app.exceptionsToEscalate, {});

        })

        it("should not relay when cache is close", function(){


            var now = moment();
            var cache = moment().add(-3500, "second");
            console.log(now.toISOString(), cache.toISOString());

            var app = {
               exceptionsToEscalate: {
                  "TYPE": ["anything1","anything2"],
               },
               exceptionsEmailDelayCache: {"TYPE": cache}
            }
            var r = escalateLib.getEventsToDispatchViaEmail(app, before, now);

            assert.isUndefined(r);
            assert.propertyVal(app.exceptionsEmailDelayCache, "TYPE", cache);
            assert.deepEqual(app.exceptionsToEscalate, {"TYPE": ["anything1","anything2"]});

        })

        it("should not relay when cache is old", function(){


            var now = moment();
            var cache = moment().add(-3700, "second");
            console.log(now.toISOString(), cache.toISOString());

            var app = {
               exceptionsToEscalate: {
                  "TYPE": ["anything1","anything2"],
               },
               exceptionsEmailDelayCache: {"TYPE": cache}
            }
            var r = escalateLib.getEventsToDispatchViaEmail(app, before, now);

            assert.deepEqual(r, {"TYPE": ["anything1","anything2"]});
            assert.propertyVal(app.exceptionsEmailDelayCache, "TYPE", now);
            assert.deepEqual(app.exceptionsToEscalate, {});

        })

    });


    describe('preparation', function() {

        TestCommon.CreateAccount({e_email:main_email}, "main");

        it("setting an escalation tel", function(done){
             mapi.post( "/tels/search", {t_user_id: userIds.main}, function(err, result, httpResponse){

                main_telno = result[0];

                mapi.post( "/tels/set-escalate", {t_human: main_telno.t_human, t_escalate: true}, function(err, result, httpResponse){
                    assert.equal(result, "ok");
                    done();
                });

             })

        });

        it("setting an escalation email", function(done){
            mapi.post( "/emails/set-escalate", {e_email: main_email, e_escalate: true}, function(err, result, httpResponse){
                assert.equal(result, "ok");
                done();
            });

        });
    });

    describe('basic', function() {

        it("submitting an exception", function(done){

             mapi.put( "/escalations", {server: "s1", events: [{type:"FOOBAR",p:1}]}, function(err, result, httpResponse){
                assert.equal(result, "ok");
                done();

             })

        });

        it("and one more", function(done){

             mapi.put( "/escalations", {server: "s1", events: [{type:"FOOBAR",p:2},{type:"WEBSERVER_REHASH_FAILED"}]}, function(err, result, httpResponse){
                assert.equal(result, "ok");
                done();

             })

        });

        it("and one with another server", function(done){

             mapi.put( "/escalations", {server: "s2", events: [{type:"FOOBAR"}]}, function(err, result, httpResponse){
                assert.equal(result, "ok");
                done();

             })

        });

        it("and now processing them", function(done){

             var postReturned = false;
             var smsWasCalled = 0;
             var sendMailCalled = 0;

             app.GetSmsLib = function(){
                return {
                    Insert: function(params){
                         // console.log("sms",params);
                         assert.deepEqual(params, { s_country_code: '36',
  s_area_code: '30',
  s_phone_no: '1234567',
  s_message: 'WEBSERVER_REHASH_FAILED: s1' })
                         smsWasCalled++;
                         readyLogic();
                         return Promise.resolve();
                    }
                }
             }
             app.GetMailer = function(){
                return {
                    SendMailAsync: function(params){
                        // console.log("email", params, params.context.events);
                        if(sendMailCalled == 0) {
                            Object.keys(params.context.events).forEach(server=>{
                              params.context.events[server].forEach(x=>{
                                assert.ok(x.received);
                                delete x.received
                              })
                            })
                            assert.deepEqual(params, { template: 'misc/escalate-batch',
                              context: { type: "FOOBAR", events: { s1: [ { p: 1 }, { p: 2 } ], s2: [ {} ] } },
                              locale: "en",
                              to: main_email })
                        }
                        else
                        if(sendMailCalled == 1) {
                            params.context.events.s1.forEach(x=>{
                                assert.ok(x.received);
                                delete x.received
                            })
                            assert.deepEqual(params, { template: 'misc/escalate-batch',
                              context: { type: "WEBSERVER_REHASH_FAILED", events: { s1: [ {} ] }},
                              locale: "en",
                              to: main_email })
                        }
                        else
                            throw new Error("Unexpected");

                        sendMailCalled++;

                        readyLogic();

                        return Promise.resolve();
                    }
                }
             }

             mapi.post( "/escalations/process", {}, function(err, result, httpResponse){
                // console.log("returned", result)
                assert.deepEqual(result, {events:3});
                postReturned = true;
                readyLogic();
             })

             function readyLogic(){
                // console.log("readyLogic", postReturned, smsWasCalled, sendMailCalled)
                if(!postReturned)return;
                if(smsWasCalled != 1)return;
                if(sendMailCalled != 2)return;

                done();

             }

        });

        it("and second process should not do anything", function(done){

             app.GetSmsLib = function(){
                return {
                    Insert: function(params){
                         throw new Error("should not be called")
                    }
                }
             }
             app.GetMailer = function(){
                return {
                    SendMailAsync: function(params){
                         throw new Error("should not be called")
                    }
                }
             }

             mapi.post( "/escalations/process", {}, function(err, result, httpResponse){
                // console.log("returned", result)
                assert.deepEqual(result, {events:0});
                done();
             })

        });

   })


}, 10000)

