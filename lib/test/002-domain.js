require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var TestCommonLib = require("test/000-common.js")

var ExpressTesterLib = require("ExpressTester")
var app = require( "../account-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    var userIds = {}
    var TestCommon = TestCommonLib(mapi, assert, userIds)

	describe('prepare', function() {

        TestCommon.CreateAccount({e_email:"domainuser@email.ee"}, "domain")

   })

    describe('adding domains', function() {

        it('trying to add domain of invalid syntax', function(done) {

             mapi.put( "/account/"+userIds.domain+"/domains", {"domain": "fooooooo"}, function(err, result, httpResponse){

                assert.validation_error(err, result, httpResponse, done) 

             })
        })    


        for(var i = 0; i < 3; i++) {
            it('adding domain of correct syntax multiple times (next test group should validate if it was added twice or not)', function(done) {

                 mapi.put( "/account/"+userIds.domain+"/domains", {"domain": "fooooooo.tld"}, function(err, result, httpResponse){

                    assert.deepEqual(result, { d_domain_canonical_name: 'fooooooo.tld',
         d_domain_name: 'fooooooo.tld' })

                    done(err)
                    
                 })
            })    
        }

        it('adding domain with unicode letters', function(done) {

             mapi.put( "/account/"+userIds.domain+"/domains", {"domain": "fooáooooo.tld"}, function(err, result, httpResponse){

                assert.deepEqual(result, { d_domain_name: 'fooáooooo.tld',
     d_domain_canonical_name: 'xn--fooooooo-bza.tld' })

                done(err)
                
             })
        })    

    })


    describe('querying and deleting domains', function() {

        it('fetching domains', function(done) {

             mapi.get( "/account/"+userIds.domain+"/domains", function(err, result, httpResponse){

                assert.deepEqual(result, [ { 
    d_domain_canonical_name: 'fooooooo.tld',
    d_domain_name: 'fooooooo.tld' },
  { 
    d_domain_canonical_name: 'xn--fooooooo-bza.tld',
    d_domain_name: 'fooáooooo.tld' } ])

                done(err)

             })
        })    

        it('fetching a domain by name', function(done) {

             mapi.get( "/account/"+userIds.domain+"/domain/fooáooooo.tld", function(err, result, httpResponse){

                assert.deepEqual(result, 
  { 
    d_domain_canonical_name: 'xn--fooooooo-bza.tld',
    d_domain_name: 'fooáooooo.tld' } )

                done(err)

             })
        })    


        it('deleting a domain', function(done) {

             mapi.delete( "/account/"+userIds.domain+"/domain/fooooooo.tld", {}, function(err, result, httpResponse){

                assert.propertyVal(result, "deleted", 1)

                done(err)

             })
        })    

        it('fetching domains again', function(done) {

             mapi.get( "/account/"+userIds.domain+"/domains", function(err, result, httpResponse){

                assert.deepEqual(result, [ 
  { 
    d_domain_canonical_name: 'xn--fooooooo-bza.tld',
    d_domain_name: 'fooáooooo.tld' } ])

                done(err)

             })
        })    

    })



    describe('and bonus', function() {

        it('adding domain with punycode', function(done) {

             mapi.put( "/account/"+userIds.domain+"/domains", {"domain": "xn--foooo2-sta.tld"}, function(err, result, httpResponse){

                assert.deepEqual(result, { d_domain_name: 'foooáo2.tld',
     d_domain_canonical_name: 'xn--foooo2-sta.tld' })

                done(err)
                
             })
        })    

    })


}, 10000)

