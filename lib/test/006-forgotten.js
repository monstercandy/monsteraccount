require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var TestCommonLib = require("test/000-common.js")

var ExpressTesterLib = require("ExpressTester")
var app = require( "../account-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    var primaryEmail = "forgottentest@email.ee"
    var secondaryEmail = "forgottentest.secondary@email.ee"

    var tokens = {}
    var userIds = {}
    var TestCommon = TestCommonLib(mapi, assert, userIds, tokens)


    describe("preparation", function(){

        TestCommon.CreateAccount({e_email:primaryEmail}, "main")

        it('adding secondary', function(done) {

             mapi.put( "/account/"+userIds.main+"/emails", {"e_email": secondaryEmail,"e_confirmed": true}, function(err, result, httpResponse){

                assert.equal(result, "ok")

                done()
             })
        })

    })


    describe("forgotten password related stuffs", function(){

        it('sending with non-existent email', function(done) {
             
             mapi.post( "/accounts/send_forgotten_password", {"email": "should.not.be.found@foo.hh"}, function(err, result, httpResponse){
                assert.property(err, "message", "ACCOUNT_NOT_FOUND")

                done()
             })
        })

        it('sending with secondary email', function(done) {
             
             mapi.post( "/accounts/send_forgotten_password", {"email": secondaryEmail}, function(err, result, httpResponse){
                assert.property(err, "message", "ACCOUNT_NOT_FOUND")

                done()
             })
        })

        var e_token
        it('sending with primary email', function(done) {
             var oGetMailer = app.GetMailer
             app.GetMailer = function() {
                return app.MonsterMailer()
             }
             var oMonsterMailer = app.MonsterMailer
             app.MonsterMailer = function() {
                 var re = {}
                 re.SendMailAsync = function(data) {
                     assert.deepProperty(data, "context.email.e_token")
                     assert.deepProperty(data, "context.user.u_id")

                     e_token = data.context.email.e_token
                     
                     return Promise.resolve("ok")
                 }
                 return re
             }

             mapi.post( "/accounts/send_forgotten_password", {"email": primaryEmail}, function(err, result, httpResponse){
                console.log(result)
                assert.equal(result, "ok")

                app.GetMailer = oGetMailer
                app.MonsterMailer = oMonsterMailer

                done()
             })
        })

        it('verifying with invalid email', function(done) {

             mapi.post( "/authentication/forgotten", {account_id: userIds.main, email: "invalid@inv.ii", confirmation_token: "foo"}, function(err, result, httpResponse){
                assert.propertyVal(err, "message", "AUTH_FAILED")

                done()
             })
        })

        it('verifying with correct email and invalid token', function(done) {

             mapi.post( "/authentication/forgotten", {account_id: userIds.main, email: primaryEmail, confirmation_token: "foo"}, function(err, result, httpResponse){
                assert.propertyVal(err, "message", "AUTH_FAILED")

                done()
             })
        })

        it('verifying with correct email and correct token', function(done) {

             mapi.post( "/authentication/forgotten", {account_id: userIds.main, email: primaryEmail, confirmation_token: e_token}, function(err, result, httpResponse){
                TestCommon.ValidTokenResponse(err, result, httpResponse, "forgotten")
                done()
             })
        })


        TestCommon.TokenTest(["ACCOUNT_FORCE_PASSWORD"], "forgotten")

    })



}, 10000)

