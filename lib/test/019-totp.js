require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var TestCommonLib = require("test/000-common.js")

var ExpressTesterLib = require("ExpressTester")
var app = require( "../account-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    var tokens = {}
    var userIds = {}
    var TestCommon = TestCommonLib(mapi, assert, userIds, tokens)

    var speakeasy = require("speakeasy");

    var secret;

    var email = "totp@email.ee";


	describe('totp tests', function() {


        TestCommon.CreateAccount({e_email:email}, "first")


        it('grabbing a secret code', function(done) {

             mapi.post( "/account/"+userIds.first+"/totp/status", {}, function(err, result, httpResponse){
                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)

                assert.propertyVal(result, "u_totp_active", 0);
                assert.property(result, "u_totp_url");

                secret = result.u_totp_secret;

                done();

             })
        })

        it('trying to deactivate totp', function(done) {


             mapi.post( "/account/"+userIds.first+"/totp/inactivate", {"userToken": "123456"}, function(err, result, httpResponse){

                assert.app_error(err, result, httpResponse, "NOT_ACTIVATED", done)

             })
        })

        it('trying to activate totp with invalid code', function(done) {


             mapi.post( "/account/"+userIds.first+"/totp/activate", {"userToken": "123456"}, function(err, result, httpResponse){

                assert.app_error(err, result, httpResponse, "INVALID_TOTP_TOKEN", done)

             })
        })

        it('activating totp with correct code', function(done) {

            var token = speakeasy.totp({
              secret: secret,
              encoding: 'base32'
            });

             mapi.post( "/account/"+userIds.first+"/totp/activate", {"userToken": token}, function(err, result, httpResponse){
                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)
                assert.equal(result, "ok");

                done()  

             })
        })


        it('status page shall return already activate', function(done) {
             mapi.post( "/account/"+userIds.first+"/totp/status", {}, function(err, result, httpResponse){
                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)
                assert.deepEqual(result, {u_totp_active: 1})

                done()  

             })
        })


        it('trying to activate totp again', function(done) {

             mapi.post( "/account/"+userIds.first+"/totp/activate", {"userToken": "123456"}, function(err, result, httpResponse){

                assert.app_error(err, result, httpResponse, "ALREADY_ACTIVATED", done)

             })
        })

          it('invalid password shall behave as originally', function(done) {

              mapi.post( "/authentication/credential", {username:email,password:"wrongx"}, function(err, result, httpResponse){

                    assert.app_error(err, result, httpResponse, "AUTH_FAILED", done)
              })
          })   
          it('proper login shall return a hint', function(done) {

              mapi.post( "/authentication/credential", {username:email,password:TestCommon.DefaultPassword}, function(err, result, httpResponse){

                    assert.app_error(err, result, httpResponse, "TOTP_REQUIRED", done)
              })
          })        

          it('trying to provide an invalid token', function(done) {

              mapi.post( "/authentication/credential", {username:email,password:TestCommon.DefaultPassword, userToken: "123456"}, function(err, result, httpResponse){

                    assert.app_error(err, result, httpResponse, "INVALID_TOTP_TOKEN", done)
              })
          })        


          it('providing a correct token', function(done) {

              var token = speakeasy.totp({
                  secret: secret,
                  encoding: 'base32'
              });

              mapi.post( "/authentication/credential", {username:email,password:TestCommon.DefaultPassword, userToken:token}, function(err, result, httpResponse){
                    assert.isNull(err)
                    assert.equal(httpResponse.statusCode, 200)
                    assert.property(result, "u_name" )
                    assert.property(result, "u_primary_email")
                    assert.property(result, "token" )

                    tokens.main = result.token;

                    done()
              })
          })   
          // session shall have been upgraded
          TestCommon.TokenTest(["ACCOUNT_OWNER"], "main");


        it('switching to a totp enabled user without credential specified', function(done) {

             mapi.post( "/authentication/switch_user", {account_id: userIds.first}, function(err, result, httpResponse){
                validTokenResult(err, result, done)
    
             })
        })


        it('trying to deactivate totp with invalid code again', function(done) {


             mapi.post( "/account/"+userIds.first+"/totp/inactivate", {"userToken": "123456"}, function(err, result, httpResponse){

                assert.app_error(err, result, httpResponse, "INVALID_TOTP_TOKEN", done)

             })
        })

        it('deactivating totp with correct code', function(done) {

              var token = speakeasy.totp({
                  secret: secret,
                  encoding: 'base32'
              });

             mapi.post( "/account/"+userIds.first+"/totp/inactivate", {"userToken": token}, function(err, result, httpResponse){

                assert.equal(result, "ok");

                done();

             })
        })

        TestCommon.DeleteAccount(userIds, "first")
    })


   function validTokenResult(err, result, done) {
       assert.isNull(err)
       assert.property(result, "token" )
       done()  
 
   }


}, 10000)

