require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var TestCommonLib = require("test/000-common.js")

var ExpressTesterLib = require("ExpressTester")
var app = require( "../account-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    const templateData = {
                 "t_name": "WEB10000",
                 "t_storage_max_quota_hard_mb": 10002,
                 "t_storage_max_quota_soft_mb": 10001,
                 "t_domains_max_number_attachable": 10,

                 "t_git_max_repos": 10,
                 "t_git_max_branches": 10,

                 "t_email_max_number_of_accounts": 101,
                 "t_email_max_number_of_aliases": 102,
                 "t_email_max_number_of_bccs": 105,

                 "t_ftp_max_number_of_accounts": 103,
                 "t_awstats_allowed": true,
                 "t_cron_max_entries": 103,

                 "t_allowed_dynamic_languages": ["php",".net"],
                 "t_max_container_apps": 0,

                 "t_bandwidth_limit": {},

                 "t_webapp_email_quota_policy": "foobar",

                 "t_db_max_number_of_databases": 11,
                 "t_db_max_databases_size_mb": 101,
                 "t_db_max_number_of_users": 111,
                 "t_db_keep_backup_for_days": 112,

                 "t_max_number_of_firewall_rules": 103,

                "t_x509_certificate_external_allowed": true,
                "t_x509_certificate_letsencrypt_allowed": true,

                "t_installatron_allowed": true,

                "t_public": true,
               }
    const templateFromDatabase = { t_name: 'WEB10000',
    t_storage_max_quota_hard_mb: 10002,
    t_storage_max_quota_soft_mb: 10001,
    t_domains_max_number_attachable: 10,
    t_email_max_number_of_accounts: 101,
    t_email_max_number_of_aliases: 102,
    t_email_max_number_of_bccs: 105,
    t_webapp_email_quota_policy: "foobar",
    t_ftp_max_number_of_accounts: 103,
    t_git_max_repos: 10,
    t_git_max_branches: 10,
    t_awstats_allowed: 1,
    t_cron_max_entries: 103,
    t_bandwidth_limit: {},
    t_allowed_dynamic_languages: ["php",".net"],
    t_max_container_apps: 0,
    t_container_cmd_timeout_s: 0,
    t_db_max_number_of_databases: 11,
    t_db_max_databases_size_mb: 101,
    t_db_max_number_of_users: 111,
    t_db_keep_backup_for_days: 112,
    t_max_number_of_firewall_rules: 103,
    t_x509_certificate_external_allowed: 1,
    t_x509_certificate_letsencrypt_allowed: 1,
    t_installatron_allowed: 1,
    t_public: 1 }


    describe('webhostingtemplates', function() {


        it("getting template list", function(done){

             mapi.get( "/webhostingtemplates",function(err, result, httpResponse){
                assert.deepEqual(result, [])
                done()

             })
        })

        it("adding template with invalid params", function(done){

             mapi.put( "/webhostingtemplates", {"s_name": "the rapy", "s_secret": "lie down on the coach"}, function(err, result, httpResponse){
                assert.validation_error(err, result, httpResponse, done)

             })
        })

        it("adding template", function(done){

             mapi.put( "/webhostingtemplates", templateData,
             function(err, result, httpResponse){
                console.log("err", err)
                assert.equal(result, "ok")
                done()
             })
        })


        it("second attempt should fail", function(done){
             mapi.put( "/webhostingtemplates", templateData, function(err, result, httpResponse){
                console.log("shit", err, result)
                assert.propertyVal(err, "message", "TEMPLATE_ALREADY_EXISTS");
                done();

             })

        })


        it("invalid soft/hard values", function(done){
             templateData.t_name = "foobar"
             templateData.t_storage_max_quota_hard_mb = templateData.t_storage_max_quota_soft_mb - 5
             mapi.put( "/webhostingtemplates", templateData, function(err, result, httpResponse){
                assert.validation_error(err, result, httpResponse, done)

             })

        })

        it("getting list of supported dynamic languages", function(done){

             mapi.get( "/config/webhostingtemplates/dynamiclanguages", function(err, result, httpResponse){
                assert.ok(Array.isArray(result))
                assert.ok(result.length > 0)
                done()

             })
        })

        it("getting template list", function(done){

             mapi.get( "/webhostingtemplates", function(err, result, httpResponse){
                assert.ok(Array.isArray(result))
                var nresult = result.map(x=>{delete x.created_at; return x})
                assert.deepEqual(nresult, [ templateFromDatabase ])
                done()

             })
        })

        it("getting template by name", function(done){

             mapi.get( "/webhostingtemplate/WEB10000", function(err, result, httpResponse){
                delete result.created_at;
                assert.deepEqual(result, templateFromDatabase )
                done()

             })
        })

        it("modifying some of the parameters", function(done){

             mapi.post( "/webhostingtemplate/WEB10000", {t_name:"WEB10002"}, function(err, result, httpResponse){
                console.log(err)
                assert.equal(result, "ok")
                done()

             })
        })

        it("delete template, invalid name", function(done){

             mapi.delete( "/webhostingtemplate/WEB10001", {}, function(err, result, httpResponse){
                assert.app_error(err, result, httpResponse, "TEMPLATE_NOT_FOUND", done)

             })
        })

        it("delete template, correct name", function(done){

             mapi.delete( "/webhostingtemplate/WEB10002", {}, function(err, result, httpResponse){
                assert.equal(result, "ok")
                done()
             })
        })


        it("getting template list", function(done){

             mapi.get( "/webhostingtemplates",function(err, result, httpResponse){
                assert.deepEqual(result, [])
                done()

             })
        })

    })


}, 10000)

