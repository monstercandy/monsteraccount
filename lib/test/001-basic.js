require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var TestCommonLib = require("test/000-common.js")

var ExpressTesterLib = require("ExpressTester")
var app = require( "../account-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    var tokens = {}
    var userIds = {}
    var TestCommon = TestCommonLib(mapi, assert, userIds, tokens)

	describe('create account', function() {
	    it('post instead of put', function(done) {

             mapi.post( "/accounts", {}, function(err, result, httpResponse){
                // we dont use assert.internal_error here since we don't have a response code 

                assert.isNotNull(err)
                assert.equal(err.message, "INTERNAL_ERROR")

                done()  
             })
        })

	    it('json parsing error', function(done) {
             mapi.put( "/accounts", "{\"username\"}", function(err, result, httpResponse){
             	assert.app_error(err, result, httpResponse, "JSON_PAYLOAD_ERROR", done)
             })
        })

	    it('empty object', function(done) {
             mapi.put( "/accounts", {}, function(err, result, httpResponse){
             	assert.validation_error(err, result, httpResponse, done)
             })
        })

	    it('no password', function(done) {
             mapi.put( "/accounts", {u_name:"u_name",e_email:"email@email.ee"}, function(err, result, httpResponse){
             	assert.validation_error(err, result, httpResponse, done)

             })
        })

	    it('weak password', function(done) {
             mapi.put( "/accounts", {u_name:"u_name",e_email:"email@email.ee",c_password:"easy"}, function(err, result, httpResponse){
             	assert.validation_error(err, result, httpResponse, done)

             })
        })


        it('phone number missing', function(done) {
             var orig = app.config.get("tel_should_be_confirmed")
             app.config.set("tel_should_be_confirmed", true)

             mapi.put( "/accounts", {u_name:"u_name",e_email:"email@email.ee",c_password:"easy1234"}, function(err, result, httpResponse){

                app.config.set("tel_should_be_confirmed", orig)

                assert.validation_error(err, result, httpResponse, done)

             })
        })
        
        app.config.set("linear_numeric_account_ids", true)

        TestCommon.CreateAccount({e_email:"email@email.ee"}, "first", function(err, result){

            if (isNaN(userIds.first)) throw new Error("ID_IS_NOT_NUMERICAL", userIds.first)
            assert.equal('1', userIds.first)

        })

        TestCommon.CreateAccount({e_email:"email-append@email.ee"}, "first_append", function(err, result){
            assert.equal('2', userIds.first_append)

            app.config.set("linear_numeric_account_ids", false)            
        })


        it('account fetch', function(done) {

             mapi.get( "/account/"+userIds.first, function(err, result, httpResponse){
                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)
                assert.propertyVal(result, "u_id", userIds.first)
                assert.propertyVal(result, "u_name", "u_name")
                assert.propertyVal(result, "u_comment", "")
                assert.propertyVal(result, "u_primary_email_confirmed", 0)

                assert.property(result, "u_email_login_ok_subsystems")
                assert.ok(Array.isArray(result.u_email_login_ok_subsystems));
                assert.ok(result.u_email_login_ok_subsystems.length);
                assert.ok(result.u_email_login_ok_subsystems.indexOf("email") < 0);
                done()  

             })
        })

        it('changing account login notify subsystems', function(done) {

             mapi.post( "/account/"+userIds.first, {"u_email_login_ok_subsystems":["email"]}, function(err, result, httpResponse){
                assert.equal(result, "ok");
                done()  

             })
        })

        it('change in subsystems should be reflected', function(done) {

             mapi.get( "/account/"+userIds.first, function(err, result, httpResponse){
                assert.deepEqual(result.u_email_login_ok_subsystems, ["email"]);
                done()  

             })
        })

        it('account change details', function(done) {
             mapi.post( "/account/"+userIds.first, {"u_name":"Tell your children","u_comment": "FOOOOO!","u_language":"hu"}, function(err, result, httpResponse){

                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)
                assert.equal(result, "ok")
                done()  
             })
        })


        it('modified account fetch', function(done) {

             mapi.get( "/account/"+userIds.first, function(err, result, httpResponse){

                // console.log("!!!", err, result)

                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)
                assert.propertyVal(result, "u_name", "Tell your children")
                assert.propertyVal(result, "u_comment", "FOOOOO!")
                assert.propertyVal(result, "u_language", "hu")
                done()  

             })

        })

        it('account change details, single key only', function(done) {
             mapi.post( "/account/"+userIds.first, {"u_language":"en"}, function(err, result, httpResponse){

                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)
                assert.equal(result, "ok")
                done()  
             })
        })


        it('account modify (nothing to change)', function(done) {
             mapi.post( "/account/"+userIds.first, {}, function(err, result, httpResponse){

                assert.app_error(err, result, httpResponse, "NOTHING_TO_CHANGE", done)
             })
        })

        it('account modify (invalid data)', function(done) {
             mapi.post( "/account/"+userIds.first, {"u_name":{}}, function(err, result, httpResponse){

                assert.validation_error(err, result, httpResponse, done)
             })
        })
        

	    it('email already exists', function(done) {
             mapi.put( "/accounts", TestCommon.CreateAccountParams({e_email:"email@email.ee"}), function(err, result, httpResponse){
             	assert.app_error(err, result, httpResponse, "EMAIL_ALREADY_EXISTS", done)

             })
        })

        it('email already exists (different casing!)', function(done) {
             mapi.put( "/accounts", TestCommon.CreateAccountParams({e_email:"Email@email.ee"}), function(err, result, httpResponse){
                assert.app_error(err, result, httpResponse, "EMAIL_ALREADY_EXISTS", done)

             })
        })

        TestCommon.CreateAccount({e_email:"email.should.be.confirmed@email.ee","e_confirmed": true}, "emailConfirmed")

        it('account fetch, primary email should be confirmed', function(done) {

             mapi.get( "/account/"+userIds.emailConfirmed, function(err, result, httpResponse){
                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)
                assert.propertyVal(result, "u_id", userIds.emailConfirmed)
                assert.propertyVal(result, "u_name", "u_name")
                assert.propertyVal(result, "u_comment", "")               
                assert.propertyVal(result, "u_primary_email_confirmed", 1)
                done()  

             })
        })



        it('email details', function(done) {
             mapi.get( "/account/"+userIds.emailConfirmed+"/emails", function(err, result, httpResponse){

                assert_email_confirmed(err, result, httpResponse, done, 1)

             })
        })




        TestCommon.CreateAccount({e_email:"email.with.encrypted.password@email.ee","password_type":"encrypted",c_password:"$salt/hash"}, "encrypted")



        it('account registration', function(done) {

             TestCommon.RegisterInnerSuccessful(app, TestCommon.CreateAccountParams({u_comment: "should_be_blanked",e_confirmed: true, e_email:"regemail@email.ee"}), function(err, result, httpResponse){
                userIds.reg = result.u_id

                done()                  
             })
             
        })

        it('requesting callback', function(done) {

            var oGetMailer = app.GetMailer
             app.GetMailer = function() {
                 var re = {}
                 re.SendMailAsync = function(data) {
                     assert.propertyVal(data, "template", "account/callback")

                     return Promise.resolve("ok")
                 }
                 return re
             }

             mapi.post( "/account/"+userIds.reg+"/callback_request", {"message": "hello world"}, function(err, result, httpResponse){
                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)

                app.GetMailer = oGetMailer
                done()                  

             })
        })


        it('registered account fetch', function(done) {
             mapi.get( "/account/"+userIds.reg, function(err, result, httpResponse){
                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)
                assert.propertyVal(result, "u_comment", "")
                done()  

             })
        })


        it('email details', function(done) {
             mapi.get( "/account/"+userIds.reg+"/emails", function(err, result, httpResponse){

                assert_email_confirmed(err, result, httpResponse, done, 0)

             })
        })        


        TestCommon.CreateAccount({e_email:"email100@email.ee", u_id: 100}, "withId", function(err, result, httpResponse){

                assert.propertyVal(result, "u_id", 100 ) // u_id: 100
         })
        

        it('fetching account created with id', function(done) {
             mapi.get( "/account/"+userIds.withId, function(err, result, httpResponse){
                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)
                assert.property(result, "u_id")
                assert.equal(result.u_id, userIds.withId)
                done()  

             })
        })

        it('number of accounts', function(done) {

             mapi.post( "/accounts/count", {}, function(err, result, httpResponse){


                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)
                assert.propertyVal(result, "count", 6 )
                done()  

             })
        })

        it('number of accounts with filters', function(done) {

             mapi.post( "/accounts/count", {"where":"email100@"}, function(err, result, httpResponse){

                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)
                assert.propertyVal(result, "count", 1 )
                done()  

             })
        })

        it('list accounts', function(done) {

             mapi.post( "/accounts/search", {"limit":1,"offset":4}, function(err, result, httpResponse){

                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)
                assert.property(result, "accounts")
                assert.equal(result.accounts.length, 1)
                assert.property(result.accounts[0], "u_id", 100)
                assert.property(result.accounts[0], "u_name", "u_name")
                assert.property(result.accounts[0], "u_primary_email", "email100@email.ee")

                done()  

             })
        })


        it('list deleted accounts', function(done) {

             mapi.post( "/accounts/search", {"limit":1,"offset":4,deleted:0}, function(err, result, httpResponse){

                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)
                assert.property(result, "accounts")
                assert.equal(result.accounts.length, 1)

                done()  

             })
        })


        var created_at = "2000-01-01 00:00:00"
        TestCommon.CreateAccount({e_email:"emailca@email.ee","created_at":created_at}, "createdAt")


        it('account fetch, created_at should match', function(done) {

             mapi.get( "/account/"+userIds.createdAt, function(err, result, httpResponse){
                assert.propertyVal(result, "created_at", created_at)
                done()  

             })
        })



	})

	describe('credential based authentication', function() {


	    it('wrong password', function(done) {
             mapi.post( "/authentication/credential", {username:"email@email.ee",password:"Xeasy1234"}, function(err, result, httpResponse){

             	assert.app_error(err, result, httpResponse, "AUTH_FAILED", done)

             })
        })

        TestCommon.CredentialAuthShouldWork("email@email.ee","easy1234","main")

        it('sessionips should return it', function(done) {
             mapi.get( "/authentication/sessionips", function(err, result, httpResponse){
                // console.log(err, result)
                assert.isNull(err)
                assert.isOk(Array.isArray(result))
                result.forEach(x=>{
                    assert.property(x, "s_ip")
                    assert.property(x, "s_user_id")
                    assert.property(x, "s_expires")
                })
                done()
             })
        })


	    it('unknown username', function(done) {
             mapi.post( "/authentication/credential", {username:"xemail@email.ee",password:"Xeasy1234"}, function(err, result, httpResponse){
             	assert.app_error(err, result, httpResponse, "AUTH_FAILED", done)

             })
        })        


	})

    describe('token based authentication', function() {


        it('invalid token', function(done) {
             mapi.post( "/authentication/token", {token:"123456"}, function(err, result, httpResponse){
                assert.app_error(err, result, httpResponse, "AUTH_FAILED", done)

             })
        })

        TestCommon.TokenTest(["ACCOUNT_OWNER"], "main");


    })




	describe('change password', function() {

	    it('wrong password', function(done) {
             mapi.post( "/account/"+userIds.first+"/change_password", {new_password:"easy1234",old_password:"Xeasy1234"}, function(err, result, httpResponse){

             	assert.app_error(err, result, httpResponse, "AUTH_FAILED", done)

             })
        })


	    it('weak new password', function(done) {
             mapi.post( "/account/"+userIds.first+"/change_password", {new_password:"easy",old_password:"easy1234"}, function(err, result, httpResponse){
             	assert.validation_error(err, result, httpResponse,  done)

             })
        })


	    it('changing password', function(done) {
             mapi.post( "/account/"+userIds.first+"/change_password", {new_password:"easy12345",old_password:"easy1234"}, function(err, result, httpResponse){

			 	assert.isNull(err)
			    assert.equal(httpResponse.statusCode, 200)
			    assert.equal(result, "ok" )
			    done()  

             })
        })

        var user ="email@email.ee"
        authTest(user, "easy1234", false)
        authTest(user, "easy12345", true)


        it('forcing new password', function(done) {
             mapi.post( "/account/"+userIds.first+"/force_password", {new_password:"easy123456"}, function(err, result, httpResponse){

                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)
                assert.equal(result, "ok" )
                done()  

             })
        })

        authTest(user, "easy12345", false)
        authTest(user, "easy123456", true)



        TestCommon.CreateAccount({e_email:"emailsmsnot@email.ee", "u_sms_notification": true }, "smsnotification")

	})



    describe('lookups', function() {

        it('lookup accounts by id', function(done) {
             mapi.post( "/accounts/lookup/id", {ids:[userIds.first_append, userIds.first]}, function(err, result, httpResponse){
                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)
                assert.deepEqual(result, { '1': 'Tell your children', '2': 'u_name' })
                done()  

             })
        })

    })

    describe('delete account', function() {

        var user = "regemail@email.ee"
        var pass = "easy1234"
        authTest(user, pass, true)

        TestCommon.DeleteAccount(userIds, "reg")

        authTest(user, pass, false)

    })




function authTest(username, password, isItValid) {
   it('auth for '+username+' with '+password, function(done) {
     mapi.post( "/authentication/credential", {username:username,password:password}, function(err, result, httpResponse){
        if(isItValid) {
            assert.isNull(err)
            assert.equal(httpResponse.statusCode, 200)
            assert.property(result, "token" )
            done()  
        }else{
            assert.app_error(err, result, httpResponse, "AUTH_FAILED", done)            
        }
     })
   })
}

function assert_email_confirmed(err, result, httpResponse, done, expected){

    assert.isNull(err)
    assert.equal(httpResponse.statusCode, 200)

    assert.isArray(result)
    assert.equal(result.length, 1)
    assert.equal(result[0].e_confirmed, expected)

    assert.notProperty(result[0], "e_token")

    done()  

}


}, 10000)

