require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var TestCommonLib = require("test/000-common.js")

var ExpressTesterLib = require("ExpressTester")
var app = require( "../account-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    const default_telno = '+36-30-1234567'
    const default_callin = "12345"

    var userIds = {}
    var TestCommon = TestCommonLib(mapi, assert, userIds)


    var secondPhone = { t_country_code: '36',
    t_area_code: '30',
    t_phone_no: '7654321',
    t_category: "WORK"}
    var secondPhoneHuman = "+36-30-7654321"


    describe('phone number required tests', function() {

        var orig = app.config.get("telephone_required")


        it("creating account without phone number should be accepted when it is not required", function(done){

            app.config.set("telephone_required", false)


             TestCommon.RegisterInnerSuccessful(app, TestCommon.CreateAccountParams({e_email:"tel_without_number@email.ee", "t_phone_no": ""}), function(err, result, httpResponse){

                done()
             })


        })

        it("creating account with incorrect phone number should still be rejected even if it is not required", function(done){

            app.config.set("telephone_required", false)

             TestCommon.RegisterInner(app, TestCommon.CreateAccountParams({e_email:"tel_with_half_number@email.ee", "t_phone_no": "crap"}), function(err, result, httpResponse){

                assert.validation_error(err, result, httpResponse, done)

             })

        })

        it("creating account without phone number should be rejected when it is required", function(done){
            app.config.set("telephone_required", true)

             TestCommon.RegisterInner(app, TestCommon.CreateAccountParams({"e_email":"tel_without_t_phone_no_shoud_be_rejectd@email.ee","t_phone_no": ""}), function(err, result, httpResponse){

                app.config.set("telephone_required", orig)

                assert.validation_error(err, result, httpResponse, done)

             })

        })

    })


    describe('callin related tests', function() {

      it("text callin should be rejected", function(done){

             mapi.put( "/accounts", TestCommon.CreateAccountParams({"e_email":"tel_shoud_be_rejectd@email.ee","u_callin": "foobar"}), function(err, result, httpResponse){
                assert.validation_error(err, result, httpResponse, done)

             })

      })

        TestCommon.CreateAccount({e_email:"tel@email.ee", "u_callin": default_callin}, "tel")

    })


	describe('telno tests', function() {
        it('fetching account (comparing primary tel no)', function(done) {
             mapi.get( "/account/"+userIds.tel, function(err, result, httpResponse){

                assert.propertyVal(result, "u_callin", default_callin)
                assert.propertyVal(result, "u_primary_tel", default_telno)
                done()

             })

        })

        it('listing telephone numbers', function(done) {
             mapi.get( "/account/"+userIds.tel+"/tels", function(err, result, httpResponse){

                assert.equal(Array.isArray(result), true)
                assert.equal(result.length, 1)
                assert.propertyVal(result[0], "t_human", default_telno)
                done()

             })

        })


        it('adding a phone number', function(done) {
             mapi.put( "/account/"+userIds.tel+"/tels", secondPhone, function(err, result, httpResponse){

                assert.equal(result, "ok")
                done()

             })

        })



        it('trying to add the same phone number', function(done) {
             mapi.put( "/account/"+userIds.tel+"/tels", secondPhone, function(err, result, httpResponse){

                assert.propertyVal(err, "message", "TEL_ALREADY_EXISTS")

                done()

             })

        })


        it('trying to add one more primary phone number', function(done) {
             mapi.put( "/account/"+userIds.tel+"/tels", { t_country_code: '36',
    t_area_code: '30',
    t_phone_no: '7654329',
    t_category: "WORK",
    t_primary: true}, function(err, result, httpResponse){

                assert.propertyVal(err, "message", "TEL_ALREADY_EXISTS")

                done()

             })

        })

        it('listing telephone numbers', function(done) {
             mapi.get( "/account/"+userIds.tel+"/tels", function(err, result, httpResponse){

                assert.equal(Array.isArray(result), true)
                assert.equal(result.length, 2)
                assert.propertyVal(result[1], "t_human", '+36-30-7654321')
                done()

             })

        })


        it('fetching a phone number', function(done) {
             mapi.get( "/account/"+userIds.tel+"/tel/"+default_telno, function(err, result, httpResponse){

                assert.propertyVal(result, "t_human", default_telno)
                assert.propertyVal(result, "t_confirmed", 0)
                done()

             })

        })

        it('trying to delete primary tel', function(done) {
             mapi.delete( "/account/"+userIds.tel+"/tel/"+default_telno, {}, function(err, result, httpResponse){

                assert.propertyVal(err, "message", "PRIMARY_TEL_CANNOT_BE_DELETED")
                done()

             })

        })

        var sms_token;
        it('getting a confirmation sms sent', function(done) {
             var oGetSms = app.GetSmsLib;
             var smsCalls = 0;
             const expectedPrefix = /Foobar!: Your telephone verification code is: (.+)/;
             app.GetSmsLib = function(){
                return {
                    Insert: function(params){
                        smsCalls++;
                        console.log(params);
                        var m = expectedPrefix.exec(params.s_message);
                        assert.ok(m);
                        sms_token = m[1];
                        assert.ok(sms_token);
                        delete params.s_message;

                        assert.deepEqual(params, { s_country_code: '36',
  s_area_code: '30',
  s_phone_no: '7654321',
   });
                        return Promise.resolve();
                    }
                }
             }
             mapi.post( "/account/"+userIds.tel+"/tel/"+secondPhoneHuman+"/send", {}, function(err, result, httpResponse){
                app.GetSmsLib = oGetSms;

                assert.equal(smsCalls, 1);
                assert.equal(result, "ok")
                done()

             })

        })

        it('confirming a phone', function(done) {
             mapi.post( "/account/"+userIds.tel+"/tel/"+secondPhoneHuman+"/verify", {confirmation_token: sms_token}, function(err, result, httpResponse){

                assert.equal(result, "ok")
                done()

             })

        })

        it('fetching that phone number (should be confirmed)', function(done) {
             mapi.get( "/account/"+userIds.tel+"/tel/"+secondPhoneHuman, function(err, result, httpResponse){

                assert.propertyVal(result, "t_human", secondPhoneHuman)
                assert.propertyVal(result, "t_confirmed", 1)
                assert.isNotNull(result.t_confirmed_at)
                done()

             })

        })

        it('switching primary', function(done) {
             mapi.post( "/account/"+userIds.tel+"/tel/"+secondPhoneHuman+"/set_primary", {}, function(err, result, httpResponse){

                assert.equal(result, "ok")
                done()

             })

        })

        it('fetching account (secondary tel no should be here now)', function(done) {
             mapi.get( "/account/"+userIds.tel, function(err, result, httpResponse){

                assert.propertyVal(result, "u_primary_tel", secondPhoneHuman)
                done()

             })

        })

        it('deleting old primary tel', function(done) {
             mapi.delete( "/account/"+userIds.tel+"/tel/"+default_telno, {}, function(err, result, httpResponse){

                assert.equal(result, "ok")
                done()

             })

        })


        it('fetching a phone number (should be confirmed)', function(done) {
             mapi.get( "/account/"+userIds.tel+"/tel/"+secondPhoneHuman, function(err, result, httpResponse){

                assert.propertyVal(result, "t_human", secondPhoneHuman)
                assert.propertyVal(result, "t_confirmed", 1)
                assert.isNotNull(result.t_confirmed_at)
                done()

             })

        })

        it('fetching account (tel no should be confirmed)', function(done) {
             mapi.get( "/account/"+userIds.tel, function(err, result, httpResponse){

                assert.propertyVal(result, "u_primary_tel_confirmed", 1)
                done()

             })

        })


        it('list of supported country codes is returned', function(done) {
             mapi.get( "/tel/country_codes", function(err, result, httpResponse){
                assert.propertyVal(result, "HU", "36")
                done()

             })

        })




   })

    describe('global route should return the tel numbers', function() {

        var first;
        it("list tels", function(done){
             mapi.post( "/tels/search", {}, function(err, result, httpResponse){
                // console.log(result)
                assert.ok(Array.isArray(result));
                assert.ok(result.length > 0);
                first = result[0].t_human;
                done();

             })

        });

        it("setting escalate flag to true", function(done){

             mapi.post( "/tels/set-escalate", {t_human:first, t_escalate:true}, function(err, result, httpResponse){
                // console.log(result)
                assert.equal(result, "ok");
                done();

             })

        });

        it("list tels", function(done){
             mapi.post( "/tels/search", {}, function(err, result, httpResponse){
                // console.log(result)
                assert.propertyVal(result[0], "t_escalate", 1);
                done();

             })

        });


        it("setting escalate flag to false", function(done){
             mapi.post( "/tels/set-escalate", {t_human:first, t_escalate:false}, function(err, result, httpResponse){
                // console.log(result)
                assert.equal(result, "ok");
                done();

             })

        });

        it("list tels ", function(done){
             mapi.post( "/tels/search", {}, function(err, result, httpResponse){
                // console.log(result); process.reallyExit();
                assert.propertyVal(result[0], "t_escalate", 0);
                done();

             })

        });
    });


}, 10000)

