require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var TestCommonLib = require("test/000-common.js")

var ExpressTesterLib = require("ExpressTester")
var app = require( "../account-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    var userIds = {}
    var TestCommon = TestCommonLib(mapi, assert, userIds)

   var email = "switcher@email.ee"

  describe('create account', function() {

        TestCommon.CreateAccount({e_email:email}, "main")
        TestCommon.CreateAccount({e_email:"deleted@email.ee"}, "deleted")

        it('delete account', function(done) {
             mapi.delete( "/account/"+userIds.deleted, {}, function(err, result, httpResponse){
                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)
                assert.equal(result, "ok" )

                done()                  

             })
        })

   })

	describe('switching user', function() {
        it('switching to user without credential specified', function(done) {

             mapi.post( "/authentication/switch_user", {account_id: userIds.main}, function(err, result, httpResponse){
                validTokenResult(err, result, done)
    
             })
        })

        it('switching to user with credential specified', function(done) {

             mapi.post( "/authentication/switch_user", {account_id: userIds.main, username: email}, function(err, result, httpResponse){
                validTokenResult(err, result, done)
    
             })
        })

        it('switching to unknown user', function(done) {

             mapi.post( "/authentication/switch_user", {account_id: "9999999"}, function(err, result, httpResponse){

                assert.propertyVal(err, "message", "ACCOUNT_NOT_FOUND")
                done()  

             })
        })

        it('trying to switch to deleted user', function(done) {

             mapi.post( "/authentication/switch_user", {account_id: userIds.deleted}, function(err, result, httpResponse){
                assert.propertyVal(err, "message", "AUTH_FAILED")
                done()  
    
             })
        })

        it('switching to deleted user (via credential)', function(done) {

             mapi.post( "/authentication/switch_user", {account_id: userIds.deleted, username: userIds.deleted}, function(err, result, httpResponse){
                validTokenResult(err, result, done)
    
             })
        })

   })

   function validTokenResult(err, result, done) {
       assert.isNull(err)
       assert.property(result, "token" )
       done()  
 
   }


}, 10000)

