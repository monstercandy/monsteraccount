require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var TestCommonLib = require("test/000-common.js")

var ExpressTesterLib = require("ExpressTester")
var app = require( "../account-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    var userIds = {}
    var TestCommon = TestCommonLib(mapi, assert, userIds)

    const password = "easy1234";

    var email="creduser@email.ee"
    var email_mysql="creduser_mysql@email.ee"
    var mysql_clear_password = "xxxxxx"
    var mysql_encrypted_password = '*4661D72F443CFC758BECA246B5FA89525BF23E91'

    describe('create account with mysql based ', function() {

        TestCommon.CreateAccount({e_email:email_mysql, c_password:mysql_encrypted_password,'password_type':'encrypted'}, "cred_mysql")

        TestCommon.CredentialAuthShouldWork(email_mysql,mysql_clear_password,"token_mysql_1")

        TestCommon.CredentialAuthShouldWork(email_mysql,mysql_clear_password,"token_mysql_2") // this auth should actually happen via bcrypt

   })

  	describe('create account', function() {

        TestCommon.CreateAccount({e_email:email}, "cred")

   })

    describe('credentials', function() {

        var username

        defaultCredentials()

        it('adding credential', function(done) {

             mapi.put( "/account/"+userIds.cred+"/credentials", {c_grants: ["SUPERUSER"], c_password: password},  function(err, result, httpResponse){

                  assert.property(result, "username")
                  username = result.username
                  done()
             })

        })

        it("auth with the subcredential should work", function(done){
          mapi.post( "/authentication/credential", {username:username,password:password}, function(err, result, httpResponse){
                assert.isNull(err);

                done()
          })
        })


        it('credential fetch', function(done) {

            mapi.get( "/account/"+userIds.cred+"/credential/"+email, function(err, result, httpResponse){

                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)
                assert.property(result, "c_username")
                done()

             })
        })


        it('adding credential with invalid password', function(done) {

             mapi.put( "/account/"+userIds.cred+"/credentials", {c_grants: ["SUPERUSER"], c_password: "1"},  function(err, result, httpResponse){

                  assert.validation_error(err, result, httpResponse, done)
             })

        })



        it('adding invalid credential', function(done) {

             mapi.put( "/account/"+userIds.cred+"/credentials", {c_grants: ["SOME_INVALId"], c_password: "easy12345"},  function(err, result, httpResponse){

                  assert.validation_error(err, result, httpResponse, done)
             })
        })

        it('adding without credentials', function(done) {

             mapi.put( "/account/"+userIds.cred+"/credentials", {c_grants: [], c_password: "easy12345"},  function(err, result, httpResponse){

                  assert.validation_error(err, result, httpResponse, done)
             })
        })

        it('listing credentials', function(done) {

             mapi.get( "/account/"+userIds.cred+"/credentials",  function(err, result, httpResponse){

                  assert.equal(Array.isArray(result), true)
                  assert.equal(result.length, 2)
                  assert.equal(Array.isArray(result[1].c_grants), true)
                  assert.equal(result[1].c_grants.length, 1)
                  assert.equal(result[1].c_grants[0], "SUPERUSER")
                  done()
             })
        })


        it('modifying credential', function(done) {

             mapi.post( "/account/"+userIds.cred+"/credential/"+username, {c_grants: ["ACCOUNT_DNS"]},  function(err, result, httpResponse){

                  assert.equal(result,"ok")
                  done()
             })
        })

        it('listing credentials again', function(done) {

             mapi.get( "/account/"+userIds.cred+"/credentials",  function(err, result, httpResponse){

                  assert.equal(Array.isArray(result), true)
                  assert.equal(result.length, 2)
                  assert.equal(Array.isArray(result[1].c_grants), true)
                  assert.equal(result[1].c_grants.length, 1)
                  assert.equal(result[1].c_grants[0], "ACCOUNT_DNS")
                  done()
             })
        })


        it('listing users with a given credential', function(done) {

             mapi.post( "/accounts/by-role/ACCOUNT_DNS", {}, function(err, result, httpResponse){

                  assert.deepEqual(result,{accounts: [ { u_id: userIds.cred, u_name: 'u_name' } ]})
                  done()
             })
        })

        it('deleting credential', function(done) {

             mapi.delete( "/account/"+userIds.cred+"/credential/"+username, {},  function(err, result, httpResponse){

                  assert.equal(result,"ok")
                  done()
             })
        })

        it('trying to delete default credential', function(done) {

             mapi.delete( "/account/"+userIds.cred+"/credential/"+email, {},  function(err, result, httpResponse){
                assert.propertyVal(err, "message", "DEFAULT_CREDENTIAL_CANNOT_BE_DELETED")
                done()

             })
        })

        it('changing password of a credential by username', function(done) {

             mapi.post( "/account/"+userIds.cred+"/credential/"+email+"/password", {new_password: "12345678"},  function(err, result, httpResponse){
                assert.equal(result, "ok")
                done()

             })
        })

        defaultCredentials()


    })


    describe('credential uuids', function() {
        const uuid = "123";
        var username

        it('adding credential with uuid', function(done) {

             mapi.put( "/account/"+userIds.cred+"/credentials", {c_grants: ["SUPERUSER"], c_password: password, c_uuid: uuid},  function(err, result, httpResponse){

                  assert.property(result, "username")
                  username = result.username
                  done()
             })

        })

        it('the uuid should be returned as is', function(done) {

             mapi.get( "/account/"+userIds.cred+"/credentials",  function(err, result, httpResponse){

                  assert.deepEqual(result[1], { c_username: username,
    c_grants: [ 'SUPERUSER' ],
    c_server: null,
    c_uuid: uuid,
    "can_be_deleted": true,
    "is_primary": false
     } )
                  done()
             })
        })

        it('adding credential with the same uuid should be refused', function(done) {

             mapi.put( "/account/"+userIds.cred+"/credentials", {c_grants: ["SUPERUSER"], c_password: password, c_uuid: "123"},  function(err, result, httpResponse){

                  assert.propertyVal(err, "message", "UUID_ALREADY_EXISTS")
                  done()
             })

        })

        it('should be able to find it via uuid as well (GET)', function(done) {

             mapi.get( "/account/"+userIds.cred+"/credential-uuid/"+uuid,  function(err, result, httpResponse){

                  assert.deepEqual(result, { c_username: username,
    c_grants: [ 'SUPERUSER' ],
    c_server: null,
    c_uuid: uuid,
    "can_be_deleted": true,
    "is_primary": false
     } )
                  done()
             })
        })

        it('should be able to find it via uuid as well (POST)', function(done) {

             mapi.post( "/account/"+userIds.cred+"/credential-uuid/", {c_uuid:uuid},  function(err, result, httpResponse){

                  assert.deepEqual(result, { c_username: username,
    c_grants: [ 'SUPERUSER' ],
    c_server: null,
    c_uuid: uuid,
    "can_be_deleted": true,
    "is_primary": false
     } )
                  done()
             })
        })
        it('deleting credential', function(done) {

             mapi.delete( "/account/"+userIds.cred+"/credential/"+username, {},  function(err, result, httpResponse){

                  assert.equal(result,"ok")
                  done()
             })
        })

    })

    describe('server bound credentials', function() {
        const server = "serverboundcredential";
        var username;
        var token;

        it("adding server first", function(done){

             mapi.put( "/servers", {"s_name": server, "s_secret": "lie down on the coach", "s_roles":["accountapi"]}, function(err, result, httpResponse){
                assert.property(result, "auth_code");
                assert.ok(result.auth_code);
                done()

             })
        })

        it('adding credential with server', function(done) {

             mapi.put( "/account/"+userIds.cred+"/credentials", {c_grants: ["SUPERUSER"], c_password: password, c_server: server},  function(err, result, httpResponse){

                  assert.property(result, "username")
                  username = result.username
                  done()
             })

        })

        it('the uuid should be returned as is', function(done) {

             mapi.get( "/account/"+userIds.cred+"/credentials",  function(err, result, httpResponse){

                  assert.deepEqual(result[1], { c_username: username,
    c_grants: [ 'SUPERUSER' ],
    c_server: server,
    c_uuid: null,
    "can_be_deleted": true,
    "is_primary": false
     } )
                  done()
             })
        })


        it("auth with the subcredential should be good", function(done){
          mapi.post( "/authentication/credential", {username:username,password:password}, function(err, result, httpResponse){
                assert.isNull(err);
                console.log(result);
                token = result.token;

                done()
          })
        })

        it("the token returned should not derive tokens to the other servers", function(done){
             mapi.post( "/authentication/token", {token: token}, function(err, result, httpResponse){
                assert.isNull(err);
                assert.deepEqual(result.derived_server_tokens, {});
                assert.propertyVal(result.session, "c_server", server);

                done()
             })
        })

        it('deleting credential', function(done) {

             mapi.delete( "/account/"+userIds.cred+"/credential/"+username, {},  function(err, result, httpResponse){

                  assert.equal(result,"ok")
                  done()
             })
        })

        it("delete server, correct name", function(done){

             mapi.delete( "/server/"+server, {}, function(err, result, httpResponse){
                assert.equal(result, "ok")
                done()
             })
        })


    })

    describe("max_number_of_credentials_per_account", function(){
        var max_number = app.config.get("max_number_of_credentials_per_account")

        TestCommon.CreateAccount({e_email:"max_number_of_credentials_per_account@foobar.hu"}, "max_number")

        it('adding credentials til the limit ('+max_number+') should work', function(done) {

             var callbacks = 0
             for(var i = 0; i < max_number -1; i++) {

               mapi.put( "/account/"+userIds.max_number+"/credentials", {c_grants: ["SUPERUSER"], c_password: "easy1234"+i},  function(err, result, httpResponse){

                    assert.property(result, "username")
                    callbacks++
                    if(callbacks == max_number - 1)
                      done()
               })
             }

        })



        it('additional ones should be rejected', function(done) {


               mapi.put( "/account/"+userIds.max_number+"/credentials", {c_grants: ["SUPERUSER"], c_password: "easy1234nw"},  function(err, result, httpResponse){

                    assert.isNotNull(err)
                    assert.propertyVal(err, "message", "TOO_MANY_CREDENTIALS")
                    done()
               })

        })

        it("getting list of supported permissions", function(done){

             mapi.get( "/config/credentials/permissions", function(err, result, httpResponse){
                assert.ok(Array.isArray(result))
                assert.ok(result.length > 5)
                done()

             })
        })

    })

function defaultCredentials(){
        it('listing default credentials', function(done) {

             mapi.get( "/account/"+userIds.cred+"/credentials",  function(err, result, httpResponse){

                  assert.deepEqual(result, [ { c_username: 'creduser@email.ee',
    c_grants: [ 'ACCOUNT_OWNER' ],
    c_server: null,
    c_uuid: null,
    "can_be_deleted": false,
    "is_primary": true
     } ])
                  done()
             })
        })


}


}, 10000)

