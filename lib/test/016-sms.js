require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../account-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    var sms = { s_message: "hello world", s_country_code: '36',
    s_area_code: '30',
    s_phone_no: '7654321'}
    var secondPhoneHuman = "+36-30-7654321";

    describe('basic', function() {

        it("listing sms messages sent", function(done){

             mapi.post( "/sms/search", {s_status: "sent"}, function(err, result, httpResponse){
                assert.deepEqual(result, []);
                done();

             })

        });


        it("setting api key for seeme", function(done){
             mapi.post( "/config/sms/seeme/credentials", {ApiKey: "sdfsdf"}, function(err, result, httpResponse){

                assert.equal(result, "ok");
                done();

             })

        })



        it("checking if it was set", function(done){
             mapi.get( "/config/sms/seeme/credentials", function(err, result, httpResponse){

                assert.deepEqual(result, {configured:true});
                done();

             })

        })


        it("sending an sms, all providers failing", function(done){

             var postWasCalled = false;
             app.PostForm = function(url, data, options){
                postWasCalled = true;
                return Promise.reject(new Error("fail"));
             }

             mapi.put( "/sms/", sms, function(err, result, httpResponse){
                // console.log(err, result);
                assert.isNotNull(err);
                assert.propertyVal(err, "message", "INTERNAL_ERROR");
                assert.equal(postWasCalled, true);
                done();

             })

        });



        it("sending an sms, successful", function(done){

             var postWasCalled = false;
             app.PostForm = function(url, data, options){
                postWasCalled = true;
                assert.equal(url, app.config.get("sms_seeme_api_url"));
                assert.ok(data.key);
                delete data.key;
                assert.deepEqual(data, {  message: 'hello world', number: '36307654321' });
                assert.deepEqual(options, {method: "get"});

                return Promise.resolve();
             }

             mapi.put( "/sms/", sms, function(err, result, httpResponse){
                assert.equal(result, "ok");
                assert.equal(postWasCalled, true);
                done();

             })

        });


        it("listing sms messages sent, should be there", function(done){

             mapi.post( "/sms/search", {}, function(err, result, httpResponse){
                // console.log("foo", result)
                result.forEach(x=>{
                    delete x.s_timestamp;
                })
                assert.deepEqual(result, [ { s_id: 1,
    s_provider: '',
    s_country_code: '36',
    s_area_code: '30',
    s_phone_no: '7654321',
    s_recipient: '+36-30-7654321',
    s_message: 'hello world',
    s_status: 'failed'},
  { s_id: 2,
    s_provider: 'seeme',
    s_country_code: '36',
    s_area_code: '30',
    s_phone_no: '7654321',
    s_recipient: '+36-30-7654321',
    s_message: 'hello world',
    s_status: 'sent'} ]);
                done();

             })

        });

   })


}, 10000)

