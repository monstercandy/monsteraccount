require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var TestCommonLib = require("test/000-common.js")

var ExpressTesterLib = require("ExpressTester")
var app = require( "../account-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){
 
    const email = "serverowner@email.ee";
    var tokens = {}
    var userIds = {}
    var TestCommon = TestCommonLib(mapi, assert, userIds, tokens)

    describe('servers', function() {


        it("getting server list", function(done){

             mapi.get( "/servers",function(err, result, httpResponse){
                assert.deepEqual(result, [])
                done()

             })
        })

        it("adding server with invalid params", function(done){

             mapi.put( "/servers", {"s_name": "the rapy", "s_secret": "lie down on the coach"}, function(err, result, httpResponse){
                assert.validation_error(err, result, httpResponse, done)

             })
        })

        it("adding server first", function(done){

             mapi.put( "/servers", {"s_name": "therapy", "s_secret": "lie down on the coach", "s_roles":["accountapi"]}, function(err, result, httpResponse){
                assert.property(result, "auth_code");
                assert.ok(result.auth_code);
                done()

             })
        })

        it("should be returned by name", function(done){

             mapi.get( "/server/therapy", function(err, result, httpResponse){
                assert.propertyVal(result, "s_name", "therapy")
                assert.property(result, "s_roles")
                assert.ok(Array.isArray(result.s_roles))
                assert.deepEqual(result.s_roles, ["accountapi"])
                done()

             })
        })
        it("getting server list", function(done){

             mapi.get( "/servers", function(err, result, httpResponse){
                assert.ok(Array.isArray(result))
                assert.equal(result.length, 1)
                assert.propertyVal(result[0], "s_name", "therapy")
                assert.property(result[0], "s_roles")
                assert.ok(Array.isArray(result[0].s_roles))
                assert.deepEqual(result[0].s_roles, ["accountapi"])
                done()

             })
        })

        it("second attempt should fail", function(done){
             mapi.put( "/servers", {"s_name": "therapy", "s_secret": "lie down on the coach"}, function(err, result, httpResponse){
                assert.internal_error(err, result, httpResponse, done)

             })

        })


        it("changing roles", function(done){

             mapi.post( "/server/therapy", {"s_roles":["accountapi","tapi"]}, function(err, result, httpResponse){
                assert.equal(result, "ok")
                done()

             })
        })

        TestCommon.CreateAccount({e_email:email}, "owner")

        it("changing users (negative)", function(done){

             mapi.post( "/server/therapy", {"s_users":["accountapi","tapi"]}, function(err, result, httpResponse){
                assert.propertyVal(err, "message", "VALIDATION_ERROR");
                done()

             })
        })


        it("changing users", function(done){

             mapi.post( "/server/therapy", {"s_users":[userIds.owner]}, function(err, result, httpResponse){
                assert.equal(result, "ok")
                done()

             })
        })

        it("getting server list, s_secret should not be present and users should be there", function(done){

             mapi.get( "/servers", function(err, result, httpResponse){
                assert.ok(Array.isArray(result))
                assert.equal(result.length, 1)
                assert.propertyVal(result[0], "s_name", "therapy")
                assert.property(result[0], "s_roles")
                assert.notProperty(result[0], "s_secret")
                assert.ok(Array.isArray(result[0].s_roles))
                assert.deepEqual(result[0].s_roles, ["accountapi","tapi"])
                assert.ok(Array.isArray(result[0].s_users))
                assert.deepEqual(result[0].s_users, [userIds.owner])
                done()

             })
        })

        TestCommon.CredentialAuthShouldWork(email,"easy1234","owner")
        TestCommon.TokenTest(["ACCOUNT_OWNER"], "owner", function(result){
            // console.log("FO?", result);
            assert.deepEqual(result.servers, [ 'therapy' ]);
        });


        it("deleting a role by invalid name ", function(done){

             mapi.delete( "/server/therapy/roles/xxxaccountapi", {}, function(err, result, httpResponse){
                assert.propertyVal(err, "message", "ROLE_NOT_FOUND");
                done()

             })
        });

        it("deleting a role by name", function(done){

             mapi.delete( "/server/therapy/roles/accountapi", {}, function(err, result, httpResponse){
                assert.equal(result, "ok");
                done()

             })
        });

        it("the role should have disappeared", function(done){

             mapi.get( "/servers", function(err, result, httpResponse){
                assert.ok(Array.isArray(result))
                assert.equal(result.length, 1)
                assert.ok(Array.isArray(result[0].s_roles))
                assert.deepEqual(result[0].s_roles, ["tapi"])
                done()

             })
        })

        it("delete server, invalid name", function(done){

             mapi.delete( "/server/therapyx", {}, function(err, result, httpResponse){
                assert.app_error(err, result, httpResponse, "SERVER_NOT_FOUND", done)

             })
        })

        it("delete server, correct name", function(done){

             mapi.delete( "/server/therapy", {}, function(err, result, httpResponse){
                assert.equal(result, "ok")
                done()
             })
        })


        it("getting server list", function(done){

             mapi.get( "/servers",function(err, result, httpResponse){
                assert.deepEqual(result, [])
                done()

             })
        })


        it("getting list of server roles", function(done){

             mapi.get( "/config/servers/roles", function(err, result, httpResponse){
                assert.ok(Array.isArray(result))
                assert.ok(result.length > 0)
                done()

             })
        })
    })


}, 10000)

