require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var TestCommonLib = require("test/000-common.js")

var ExpressTesterLib = require("ExpressTester")
var app = require( "../account-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){
    "use strict"

    var email = "importexport@email.ee"
    var userIds = {}
    var data
    var TestCommon = TestCommonLib(mapi, assert, userIds)

    describe('prepare', function() {

        TestCommon.CreateAccount({e_email:email}, "importexport")

        accountFetchShouldWork()

    })

    describe('exporting/importing', function() {

        it('exporting', function() {
             return assert.isFulfilled(
                 mapi.getAsync( "/export/tgz" )
                   .then((res)=>{
                    console.log(res.result)
                      assert.property(res.result,"database")
                      data = res.result
                   })

             )
        })


        TestCommon.DeleteAccount(userIds, "importexport")

        it('account should not be found', function(done) {

             mapi.get( "/account/"+userIds.first, function(err, result, httpResponse){
                assert.propertyVal(err, "message", "ACCOUNT_NOT_FOUND")
                done()  

             })
        })

        it('importing', function() {
             return assert.isFulfilled(
                 mapi.postAsync( "/import/tgz", data )
                   .then((res)=>{
                       assert.equal(res.result, "ok")
                   })

             )
        })


        accountFetchShouldWork()

    })


  function accountFetchShouldWork(){
        it('account fetch', function(done) {

             mapi.get( "/account/"+userIds.importexport, function(err, result, httpResponse){
                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)
                assert.propertyVal(result, "u_id", userIds.importexport)
                done()  

             })
        })

  }

}, 60000)

