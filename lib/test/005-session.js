require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var TestCommonLib = require("test/000-common.js")

var ExpressTesterLib = require("ExpressTester")
var app = require( "../account-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    var email = "sessiontest@email.ee"

    var userIds = {}
    var tokens = {}
    var TestCommon = TestCommonLib(mapi, assert, userIds, tokens)

    describe("preparation", function(){

        TestCommon.CreateAccount({e_email:email}, "main")

        it('good password', function(done) {
             mapi.post( "/authentication/credential", {username:email,password:"easy1234"}, function(err, result, httpResponse){
                TestCommon.ValidTokenResponse(err, result, httpResponse, "main")

                done()  
             })
        })
    })



    describe('session', function() {

        it('trying to invalidate invalid token', function(done) {

             mapi.delete( "/token/foo/bar", {}, function(err, result, httpResponse){

                assert.propertyVal(result, "deleted", 0)

                done()
             })
        })    

        it('invalidating valid token', function(done) {

             mapi.delete( "/token/"+tokens.main, {}, function(err, result, httpResponse){

                assert.propertyVal(result, "deleted", 1)

                done()
             })
        })    


        it('token should be invalid', function(done) {
             mapi.post( "/authentication/token", {token:tokens.main}, function(err, result, httpResponse){
                assert.app_error(err, result, httpResponse, "AUTH_FAILED", done)
             })
        })


    })





}, 10000)

