require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var TestCommonLib = require("test/000-common.js")

var ExpressTesterLib = require("ExpressTester")
var app = require( "../account-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

   var emailSupervisor = "account-supervisor@email.ee"
   var emailCustomer = "account-customer@email.ee"

    var userIds = {}
    var TestCommon = TestCommonLib(mapi, assert, userIds)

  describe('create account', function() {

        TestCommon.CreateAccount({e_email:emailSupervisor}, "supervisor")
        TestCommon.CreateAccount({e_email:emailCustomer}, "customer")

   })

	describe('testing account grants', function() {
        it('setting supervisor as a permitted account to access customer', function(done) {

             mapi.post( "/account/"+userIds.customer+"/account_grant", {grant_access_for: emailSupervisor}, function(err, result, httpResponse){
                assert.equal(result, "ok")
                done()
   

             })
        })



        it('fetching accounts having access to the customer', function(done) {

             mapi.get( "/account/"+userIds.customer+"/account_grants/to", function(err, result, httpResponse){
                // console.log(result)
                assert.equal(Array.isArray(result), true)
                assert.equal(result.length, 1)
                assert.equal(result[0].u_primary_email, emailSupervisor)
                done()
   

             })
        })

        it('fetching accounts having access to by the supervisor', function(done) {

             mapi.get( "/account/"+userIds.supervisor+"/account_grants/for", function(err, result, httpResponse){
                // console.log(result)
                assert.equal(Array.isArray(result), true)
                assert.equal(result.length, 1)
                assert.equal(result[0].u_primary_email, emailCustomer)
                done()
   

             })
        })

        it('removing supervisor\'s grant', function(done) {

             mapi.delete( "/account/"+userIds.customer+"/account_grant", {}, function(err, result, httpResponse){
                assert.equal(result, "ok")
                done()
   

             })
        })


        it('the list should now be empty', function(done) {

             mapi.get( "/account/"+userIds.customer+"/account_grants/to", function(err, result, httpResponse){
                // console.log(result)
                assert.equal(Array.isArray(result), true)
                assert.equal(result.length, 0)
                done()
   

             })
        })

   })


}, 10000)

