require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var TestCommonLib = require("test/000-common.js")

var ExpressTesterLib = require("ExpressTester")
var app = require( "../account-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    var userIds = {}
    var TestCommon = TestCommonLib(mapi, assert, userIds)

    const accountUser = "webhostinguser@email.ee"
    const webhostingFromServer = { w_server_name: "sxok", w_webhosting_id: 12345 }

	describe('prepare', function() {

        TestCommon.CreateAccount({e_email:accountUser}, "webhosting")

        it("creating server as well", function(done){
                 mapi.put( "/servers", {"s_name": "sxok", "s_secret": "lie down on the coach", "s_roles":["accountapi","webhosting"]}, function(err, result, httpResponse){
                    assert.isNull(err);
                    done()

                 })
        })

        it("creating one more server", function(done){
                 mapi.put( "/servers", {"s_name": "sxt", "s_secret": "lie down on the coach", "s_roles":["tapi"]}, function(err, result, httpResponse){
                    assert.isNull(err);
                    done()

                 })
        })
   })

    describe('adding webhostings', function() {

        it('trying to add webhosting with invalid syntax', function(done) {

             mapi.put( "/account/"+userIds.webhosting+"/webhostings", {"w_server_name": "../..", "w_webhosting_id": "12345"}, function(err, result, httpResponse){

                assert.validation_error(err, result, httpResponse, done)

             })
        })

        it('trying to add webhosting with non-existing server', function(done) {

             mapi.put( "/account/"+userIds.webhosting+"/webhostings", {"w_server_name": "sx7", "w_webhosting_id": "12345"}, function(err, result, httpResponse){

                assert.app_error(err, result, httpResponse, "SERVER_NOT_FOUND", done)

             })
        })

        it('trying to add webhosting with non-existing server', function(done) {

             mapi.put( "/account/"+userIds.webhosting+"/webhostings", {"w_server_name": "sxt", "w_webhosting_id": "12345"}, function(err, result, httpResponse){

                assert.app_error(err, result, httpResponse, "SERVER_IS_NOT_WEBHOSTING_CAPABLE", done)

             })
        })


        for(var i = 0; i < 3; i++) {
            it('adding webhosting of correct syntax multiple times (next test group should validate if it was added twice or not)', function(done) {

             mapi.put( "/account/"+userIds.webhosting+"/webhostings", {"w_server_name": "sxok", "w_webhosting_id": "12345"}, function(err, result, httpResponse){

                    assert.equal(result, "ok")

                    done(err)

                 })
            })
        }


    })

    describe("reporting admin login ip", function(){

        var reportDone
        var reportPosted

        TestCommon.CredentialAuthShouldWork(accountUser,"easy1234","whatever", function(err, result, httpResponse, done){
            reportDone = done
            callDone()

        }, function() {

            app.GetRelayerPool = function(){
                return {
                    postAsync: function(uri, data){
                        assert.equal(uri, "/s/sxok/ftp/sites/admin-ips")
                        assert.equal(Object.keys(data).length, 1)
                        reportPosted = true
                        callDone()
                        return Promise.resolve()
                    }
                }
            }

        })

        function callDone(){
            if((reportPosted)&&(reportDone))
                reportDone()
        }

    })


    describe('querying and deleting webhostings', function() {

        it('fetching webhostings', function(done) {

             mapi.get( "/account/"+userIds.webhosting+"/webhostings", function(err, result, httpResponse){

                assert.deepEqual(result, [ webhostingFromServer ])

                done(err)

             })
        })

        it('fetching a webhosting by name', function(done) {

             mapi.get( "/account/"+userIds.webhosting+"/webhosting/sxok/12345", function(err, result, httpResponse){

                assert.deepEqual(result, webhostingFromServer )

                done(err)

             })
        })


        it('deleting a webhosting', function(done) {

             mapi.delete( "/account/"+userIds.webhosting+"/webhosting/sxok/12345", {}, function(err, result, httpResponse){

                assert.propertyVal(result, "deleted", 1)

                done(err)

             })
        })

        it('fetching webhostings again', function(done) {

             mapi.get( "/account/"+userIds.webhosting+"/webhostings", function(err, result, httpResponse){

                assert.deepEqual(result, [])

                done(err)

             })
        })

    })



}, 10000)

