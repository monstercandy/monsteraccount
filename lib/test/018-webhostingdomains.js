require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var TestCommonLib = require("test/000-common.js")

var ExpressTesterLib = require("ExpressTester")
var app = require( "../account-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    const simpleDomain = 'wd1.tld';
    const simpleDomainOneMore = 'wd11.tld';
    const simpleDomain2 = "wd2.tld";
    const simpleServer = 'sdomain';
    const simpleWhid = 1234;

    var userIds = {}
    var TestCommon = TestCommonLib(mapi, assert, userIds)

    describe('prepare', function() {


        it("getBareDomain", function(){
             assert.equal("foobar.hu", app.getBareDomain("www.foobar.hu"));
             assert.equal("foobar.hu", app.getBareDomain("foobar.hu"));
             assert.isUndefined(app.getBareDomain("hu"));
             assert.isUndefined(app.getBareDomain("foobar|hu"));
             assert.isUndefined(app.getBareDomain("co.hu"));
             assert.equal("foobar.co.hu", app.getBareDomain("foobar.co.hu"));
        })


        TestCommon.CreateAccount({e_email:"webhostingdomainuser@email.ee"}, "domain")
        TestCommon.CreateAccount({e_email:"webhostingdomainuser2@email.ee"}, "domain2")

   })

    describe('adding domains', function() {

        it("creating server as well", function(done){
                 mapi.put( "/servers", {"s_name": simpleServer, "s_secret": "lie down on the coach", "s_roles":["accountapi","webhosting"]}, function(err, result, httpResponse){
                    assert.isNull(err);
                    done()

                 })
        })


            it('adding domain', function(done) {

                 mapi.put( "/account/"+userIds.domain+"/domains", {"domain": simpleDomain}, function(err, result, httpResponse){

                    assert.deepEqual(result, { d_domain_canonical_name: simpleDomain, d_domain_name: simpleDomain })

                    done(err)
                    
                 })
            })    


            it('adding one more domain', function(done) {

                 mapi.put( "/account/"+userIds.domain+"/domains", {"domain": simpleDomainOneMore}, function(err, result, httpResponse){

                    assert.deepEqual(result, { d_domain_canonical_name: simpleDomainOneMore, d_domain_name: simpleDomainOneMore })

                    done(err)
                    
                 })
            })    

            it('adding domain for the second user (will be needed later)', function(done) {

                 mapi.put( "/account/"+userIds.domain2+"/domains", {"domain": simpleDomain2}, function(err, result, httpResponse){

                    assert.deepEqual(result, { d_domain_canonical_name: simpleDomain2, d_domain_name: simpleDomain2 })

                    done(err)
                    
                 })
            })    


            it('adding webhosting of correct syntax multiple times (next test group should validate if it was added twice or not)', function(done) {

             mapi.put( "/account/"+userIds.domain+"/webhostings", {"w_server_name": simpleServer, "w_webhosting_id": simpleWhid}, function(err, result, httpResponse){

                    assert.equal(result, "ok")

                    done(err)

                 })
            })

    })

    describe('adding webhostingdomains', function() {


            it('querying config', function(done) {

                 mapi.get( "/config/webhostingdomains/txtrecords", function(err, result, httpResponse){

                    assert.deepEqual(result, {required: true});

                    done(err)
                 })
            })   

            it('querying a txt record needed', function(done) {

                 mapi.post( "/account/"+userIds.domain+"/webhostingdomains/txt-record-code", {wd_domain_name: simpleDomain, wd_server: simpleServer, wd_webhosting: simpleWhid}, function(err, result, httpResponse){

                    assert.property(result, "code");
                    assert.ok(result.code);

                    done(err)
                 })
            })   

            it('adding a webhosting domain without server shall be refused', function(done) {

                 mapi.put( "/webhostingdomains/", {"domain": "whatever0.tld"}, function(err, result, httpResponse){

                    assert.propertyVal(err, "message", "VALIDATION_ERROR");
                    done()                    
                 })
            })  

            it('adding a webhosting domain without webhosting id', function(done) {

                 mapi.put( "/webhostingdomains/", {"domain": "whatever1.tld", "wd_server": simpleServer}, function(err, result, httpResponse){

                    assert.equal(result, "ok")

                    done(err)
                    
                 })
            })    

            it('adding a webhosting domain with webhosting id', function(done) {

                 mapi.put( "/webhostingdomains/", {"domain": "whatever2.tld", "wd_server": simpleServer, "wd_webhosting": simpleWhid}, function(err, result, httpResponse){

                    assert.equal(result, "ok")

                    done(err)
                    
                 })
            })    

            it('adding the same webhosting domain with same data second time (should not be added twice!)', function(done) {

                 mapi.put( "/webhostingdomains/", {"domain": "whatever2.tld", "wd_server": simpleServer, "wd_webhosting": simpleWhid}, function(err, result, httpResponse){

                    assert.equal(result, "ok")

                    done(err)
                    
                 })
            })    

            it('querying the complete list of webhosting domains', function(done) {

                 mapi.get( "/webhostingdomains/", function(err, result, httpResponse){

                    // console.log(result);
                    assert.deepEqual(result, [ { wd_domain_canonical_name: 'whatever1.tld',
    wd_domain_name: 'whatever1.tld',
    wd_server: 'sdomain',
    wd_webhosting: 0 },
  { wd_domain_canonical_name: 'whatever2.tld',
    wd_domain_name: 'whatever2.tld',
    wd_server: 'sdomain',
    wd_webhosting: 1234 } ])

                    done(err)
                    
                 })
            })    

            it('querying the list of webhosting domains by webhosting', function(done) {

                 mapi.post( "/webhostingdomains/by-wh", {wd_server: simpleServer, wd_webhosting: 1234}, function(err, result, httpResponse){

                    // console.log(result);
                    assert.deepEqual(result, [ 
  { wd_domain_canonical_name: 'whatever2.tld',
    wd_domain_name: 'whatever2.tld',
    wd_server: 'sdomain',
    wd_webhosting: 1234 } ])

                    done(err)
                    
                 })
            })  

            it('adding a webhosting domain with webhosting id where an entry already existed to the same domain and server but without a webhosting', function(done) {

                 mapi.put( "/webhostingdomains/", {"domain": "whatever1.tld", "wd_server": simpleServer, "wd_webhosting": simpleWhid}, function(err, result, httpResponse){

                    assert.equal(result, "ok")

                    done(err)
                    
                 })
            })    

            it('querying the complete list of webhosting domains', function(done) {

                 mapi.get( "/webhostingdomains/", function(err, result, httpResponse){

                    // console.log(result);
                    assert.deepEqual(result, [ 
  { wd_domain_canonical_name: 'whatever2.tld',
    wd_domain_name: 'whatever2.tld',
    wd_server: 'sdomain',
    wd_webhosting: 1234 },

{ wd_domain_canonical_name: 'whatever1.tld',
    wd_domain_name: 'whatever1.tld',
    wd_server: 'sdomain',
    wd_webhosting: 1234 },
     ])

                    done(err)
                    
                 })
            })    

            it('adding a webhosting domain without webhosting id again should not change anything', function(done) {

                 mapi.put( "/webhostingdomains/", {"domain": "whatever1.tld", "wd_server": simpleServer}, function(err, result, httpResponse){

                    assert.equal(result, "ok")

                    done(err)
                    
                 })
            })    

            it('querying the complete list of webhosting domains', function(done) {

                 mapi.get( "/webhostingdomains/", function(err, result, httpResponse){

                    // console.log(result);
                    assert.deepEqual(result, [ 
  { wd_domain_canonical_name: 'whatever2.tld',
    wd_domain_name: 'whatever2.tld',
    wd_server: 'sdomain',
    wd_webhosting: 1234 },

{ wd_domain_canonical_name: 'whatever1.tld',
    wd_domain_name: 'whatever1.tld',
    wd_server: 'sdomain',
    wd_webhosting: 1234 },
     ])

                    done(err)
                    
                 })
            })    

            it('adding a webhosting domain via the account route shall be refused when the domain is not present', function(done) {


                app.MonsterInfoWebhosting = {
                    GetInfo: function(d){
                        // console.log("GetInfo", d)
                        assert.deepEqual(d, {storage_id: simpleWhid, server: simpleServer})

                        return Promise.resolve(
                        {
                           wh_id: simpleWhid,
                           wh_user_id: userIds.domain,
                           template: {
                            t_domains_max_number_attachable:3,
                           },
                        })
                    }
                };

                 mapi.put( "/account/"+userIds.domain+"/webhostingdomains/", {"wd_domain_name": "whatever3.tld", "wd_server": simpleServer, "wd_webhosting": simpleWhid}, function(err, result, httpResponse){

                    assert.propertyVal(err, "message", "TXT_RECORD_NOT_FOUND");
                    done()                    
                    
                 })
            })    

            it('adding a webhosting domain via the account route shall be refused when the webhosting id is not present', function(done) {

                app.MonsterInfoWebhosting = {
                    GetInfo: function(d){
                        const MError = require('MonsterError');
                        throw new MError("WEBHOSTING_NOT_FOUND");
                    }
                };

                 mapi.put( "/account/"+userIds.domain+"/webhostingdomains/", {"wd_domain_name": simpleDomain, "wd_server": simpleServer, "wd_webhosting": simpleWhid+1}, function(err, result, httpResponse){

                    assert.propertyVal(err, "message", "WEBHOSTING_NOT_FOUND");
                    done()                    
                    
                 })
            })    

            it('adding a webhosting domain via the account route shall be refused when the webhosting belongs to someone else', function(done) {

                app.MonsterInfoWebhosting = {
                    GetInfo: function(d){
                        // console.log("GetInfo", d)
                        assert.deepEqual(d, {storage_id: simpleWhid, server: simpleServer})

                        return Promise.resolve(
                        {
                           wh_id: simpleWhid,
                           wh_user_id: ""+(parseInt(userIds.domain,10) + 1),
                           template: {
                            t_domains_max_number_attachable:3,
                           },
                        })
                    }
                };
                
                 mapi.put( "/account/"+userIds.domain+"/webhostingdomains/", {"wd_domain_name": simpleDomain, "wd_server": simpleServer, "wd_webhosting": simpleWhid}, function(err, result, httpResponse){

                    assert.propertyVal(err, "message", "PERMISSION_DENIED");
                    done()                    
                    
                 })
            })    

            it('adding a webhosting domain via the account route with a domain and webhosting that is present should be fine (note: adding with www, silently removed)', function(done) {

                app.MonsterInfoWebhosting = {
                    GetInfo: function(d){
                        // console.log("GetInfo", d)
                        assert.deepEqual(d, {storage_id: simpleWhid, server: simpleServer})

                        return Promise.resolve(
                        {
                           wh_id: simpleWhid,
                           wh_user_id: userIds.domain,
                           template: {
                            t_domains_max_number_attachable:3,
                           },
                        })
                    }
                };

                 mapi.put( "/account/"+userIds.domain+"/webhostingdomains/", {"wd_domain_name": "www."+simpleDomain, "wd_server": simpleServer, "wd_webhosting": simpleWhid}, 
                     function(err, result, httpResponse){

                    assert.deepEqual(result, { domain: 'wd1.tld', bareDomain: 'wd1.tld' })

                    done(err)
                    
                 })
            })    

            it('adding the same again should be silently accepted', function(done) {

                app.MonsterInfoWebhosting = {
                    GetInfo: function(d){
                        // console.log("GetInfo", d)
                        assert.deepEqual(d, {storage_id: simpleWhid, server: simpleServer})

                        return Promise.resolve(
                        {
                           wh_id: simpleWhid,
                           wh_user_id: userIds.domain,
                           template: {
                            t_domains_max_number_attachable:3,
                           },
                        })
                    }
                };

                 mapi.put( "/account/"+userIds.domain+"/webhostingdomains/", {"wd_domain_name": simpleDomain, "wd_server": simpleServer, "wd_webhosting": simpleWhid}, function(err, result, httpResponse){

                    assert.deepEqual(result, { domain: 'wd1.tld', bareDomain: 'wd1.tld' })

                    done(err)                
                    
                 })
            })   


            it('exceeding the quota shall not be possible', function(done) {

                app.MonsterInfoWebhosting = {
                    GetInfo: function(d){
                        // console.log("GetInfo", d)
                        assert.deepEqual(d, {storage_id: simpleWhid, server: simpleServer})

                        return Promise.resolve(
                        {
                           wh_id: simpleWhid,
                           wh_user_id: userIds.domain,
                           template: {
                            t_domains_max_number_attachable:3,
                           },
                        })
                    }
                };

                 mapi.put( "/account/"+userIds.domain+"/webhostingdomains/", {"wd_domain_name": simpleDomainOneMore, "wd_server": simpleServer, "wd_webhosting": simpleWhid}, function(err, result, httpResponse){

                    assert.propertyVal(err, "message", "WEBSTORE_DOMAIN_QUOTA_EXCEEDED");
                    done()                    
                    
                 })
            })    

            it('querying the webhosting domains that belong to this account', function(done) {

                 mapi.get( "/account/"+userIds.domain+"/webhostingdomains/", function(err, result, httpResponse){

                    // console.log(result);
                    assert.deepEqual(result, [ { wd_domain_canonical_name: simpleDomain,
                        wd_domain_name: 'wd1.tld',
                        wd_server: 'sdomain',
                        wd_webhosting: 1234 } 
                    ])

                    done(err)
                    
                 })
            })    

            it('removing a webhosting domain via the account route', function(done) {

                 mapi.delete( "/account/"+userIds.domain+"/webhostingdomains/", {"wd_domain_name": simpleDomain, "wd_server": simpleServer, "wd_webhosting": simpleWhid}, function(err, result, httpResponse){

                    
                    assert.equal(result, "ok")

                    done(err)
                    
                 })
            })    


            it('adding a webhosting domain that belongs to someone else should be rejected even with correct TXT record', function(done) {

                 mapi.post( "/account/"+userIds.domain+"/webhostingdomains/txt-record-code", {wd_domain_name: simpleDomain2, wd_server: simpleServer, wd_webhosting: simpleWhid}, function(err, result, httpResponse){

                    var code = result.code;

                    app.MonsterInfoWebhosting = {
                        GetInfo: function(d){
                            // console.log("GetInfo", d)
                            assert.deepEqual(d, {storage_id: simpleWhid, server: simpleServer})

                            return Promise.resolve(
                            {
                               wh_id: simpleWhid,
                               wh_user_id: userIds.domain,
                               template: {
                                t_domains_max_number_attachable:3,
                               },
                            })
                        }
                    };

                    var oDnsResolveAsync = app.dnsResolveAsync;
                    app.dnsResolveAsync = function(hostname) {
                        assert.equal(hostname, simpleDomain2);
                        return Promise.resolve([["foobar1", code, "foobar2"]]);
                    }

                     mapi.put( "/account/"+userIds.domain+"/webhostingdomains/", {"wd_domain_name": simpleDomain2, "wd_server": simpleServer, "wd_webhosting": simpleWhid}, function(err, result, httpResponse){

                        assert.propertyVal(err, "message", "DOMAIN_ALREADY_PRESENT");

                        app.dnsResolveAsync = oDnsResolveAsync;

                        done()                    
                        
                     })                    
                 })

            })    

            it('adding a subdomain webhosting domain whose bare domain belongs to someone else should be rejected even with correct TXT record', function(done) {
                 mapi.post( "/account/"+userIds.domain+"/webhostingdomains/txt-record-code", {wd_domain_name: simpleDomain2, wd_server: simpleServer, wd_webhosting: simpleWhid}, function(err, result, httpResponse){

                    var code = result.code;

                    app.MonsterInfoWebhosting = {
                        GetInfo: function(d){
                            // console.log("GetInfo", d)
                            assert.deepEqual(d, {storage_id: simpleWhid, server: simpleServer})

                            return Promise.resolve(
                            {
                               wh_id: simpleWhid,
                               wh_user_id: userIds.domain,
                               template: {
                                t_domains_max_number_attachable:3,
                               },
                            })
                        }
                    };

                    var oDnsResolveAsync = app.dnsResolveAsync;
                    app.dnsResolveAsync = function(hostname) {
                        assert.equal(hostname, simpleDomain2);
                        return Promise.resolve([["foobar1", code, "foobar2"]]);
                    }

                     mapi.put( "/account/"+userIds.domain+"/webhostingdomains/", {"wd_domain_name": "foobar."+simpleDomain2, "wd_server": simpleServer, "wd_webhosting": simpleWhid}, function(err, result, httpResponse){

                        assert.propertyVal(err, "message", "DOMAIN_ALREADY_PRESENT");

                        app.dnsResolveAsync = oDnsResolveAsync;

                        done()                    
                        
                     })                    
                 })
            })    

            var txtDomain = "something-else.hu";
            it('adding a new webhosting domain with correct TXT record should be ok', function(done) {
                 mapi.post( "/account/"+userIds.domain+"/webhostingdomains/txt-record-code", {wd_domain_name: txtDomain, wd_server: simpleServer, wd_webhosting: simpleWhid}, function(err, result, httpResponse){

                    var code = result.code;

                    app.MonsterInfoWebhosting = {
                        GetInfo: function(d){
                            // console.log("GetInfo", d)
                            assert.deepEqual(d, {storage_id: simpleWhid, server: simpleServer})

                            return Promise.resolve(
                            {
                               wh_id: simpleWhid,
                               wh_user_id: userIds.domain,
                               template: {
                                t_domains_max_number_attachable:3,
                               },
                            })
                        }
                    };

                    var oDnsResolveAsync = app.dnsResolveAsync;
                    app.dnsResolveAsync = function(hostname) {
                        assert.equal(hostname, txtDomain);
                        return Promise.resolve([["foobar1", code, "foobar2"]]);
                    }

                     mapi.put( "/account/"+userIds.domain+"/webhostingdomains/", {"wd_domain_name": txtDomain, "wd_server": simpleServer, "wd_webhosting": simpleWhid}, function(err, result, httpResponse){

                        assert.deepEqual(result, { domain: 'something-else.hu', bareDomain: 'something-else.hu' });

                        app.dnsResolveAsync = oDnsResolveAsync;

                        done(err)                    
                        
                     })                    
                 })
            })    

            it('querying the complete list of webhosting domains', function(done) {

                 mapi.get( "/webhostingdomains/", function(err, result, httpResponse){

                    // console.log(result);
                    assert.deepEqual(result, [ { wd_domain_canonical_name: 'whatever2.tld',
    wd_domain_name: 'whatever2.tld',
    wd_server: 'sdomain',
    wd_webhosting: 1234 },
  { wd_domain_canonical_name: 'whatever1.tld',
    wd_domain_name: 'whatever1.tld',
    wd_server: 'sdomain',
    wd_webhosting: 1234 },
  { wd_domain_canonical_name: 'something-else.hu',
    wd_domain_name: 'something-else.hu',
    wd_server: 'sdomain',
    wd_webhosting: 1234 } ])

                    done(err)
                    
                 })
            })    

            it('it should have been added to the authorized domains as well', function(done) {

                 mapi.get( "/account/"+userIds.domain+"/domains", function(err, result, httpResponse){

                    var found = false;
                    result.some(x=>{
                         if(x.d_domain_canonical_name == txtDomain) {
                            found = true;
                            return true;
                         }
                    })


                    assert.ok(found);

                    done(err)
                    
                 })
            })   


            it('querying the list of webhosting domains by webhosting', function(done) {

                 mapi.post( "/webhostingdomains/by-wh", {wd_server: simpleServer, wd_webhosting: 1234}, function(err, result, httpResponse){

                    // console.log(result);
                    assert.deepEqual(result, [ 
  { wd_domain_canonical_name: 'whatever2.tld',
    wd_domain_name: 'whatever2.tld',
    wd_server: 'sdomain',
    wd_webhosting: 1234 },
{
          "wd_domain_canonical_name": "whatever1.tld",
          "wd_domain_name": "whatever1.tld",
          "wd_server": "sdomain",
          "wd_webhosting": 1234,
        },
        {
          "wd_domain_canonical_name": "something-else.hu",
          "wd_domain_name": "something-else.hu",
          "wd_server": "sdomain",
          "wd_webhosting": 1234,
        }
     ])

                    done(err)
                    
                 })
            })


            it('moving domain to another webhosting', function(done){
                 mapi.post( "/webhostingdomains/move", {domain: "whatever2.tld", wd_server: simpleServer, wd_webhosting: 1234, dest_wh_id: 2222}, function(err, result, httpResponse){
                    assert.equal(result, "ok");
                    done(err)                    
                 })

            })

            it('querying the list of webhosting domains by webhosting', function(done) {

                 mapi.post( "/webhostingdomains/by-wh", {wd_server: simpleServer, wd_webhosting: 1234}, function(err, result, httpResponse){

                    // console.log(result);
                    assert.deepEqual(result, [ 
{
          "wd_domain_canonical_name": "whatever1.tld",
          "wd_domain_name": "whatever1.tld",
          "wd_server": "sdomain",
          "wd_webhosting": 1234,
        },
        {
          "wd_domain_canonical_name": "something-else.hu",
          "wd_domain_name": "something-else.hu",
          "wd_server": "sdomain",
          "wd_webhosting": 1234,
        }
     ])

                    done(err)
                    
                 })
            })

            it('querying the list of webhosting domains by webhosting', function(done) {

                 mapi.post( "/webhostingdomains/by-wh", {wd_server: simpleServer, wd_webhosting: 2222}, function(err, result, httpResponse){

                    // console.log(result);
                    assert.deepEqual(result, [ 
  { wd_domain_canonical_name: 'whatever2.tld',
    wd_domain_name: 'whatever2.tld',
    wd_server: 'sdomain',
    wd_webhosting: 2222 },
     ])

                    done(err)
                    
                 })
            })

            it('removing entries by webhosting', function(done) {

                 mapi.delete( "/webhostingdomains/by-wh", {wd_server: simpleServer, wd_webhosting: 1234}, function(err, result, httpResponse){
                    assert.equal(result, "ok");
                    done(err)                    
                 })
            })

            it('removing entries by webhosting', function(done) {

                 mapi.delete( "/webhostingdomains/by-wh", {wd_server: simpleServer, wd_webhosting: 2222}, function(err, result, httpResponse){
                    assert.equal(result, "ok");
                    done(err)                    
                 })
            })

            it('querying the list of webhosting domains by webhosting', function(done) {

                 mapi.post( "/webhostingdomains/by-wh", {wd_server: simpleServer, wd_webhosting: 1234}, function(err, result, httpResponse){

                    // console.log(result);
                    assert.deepEqual(result, [  ])

                    done(err)
                    
                 })
            })

    })



}, 10000)

