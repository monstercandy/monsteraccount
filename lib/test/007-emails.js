require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var TestCommonLib = require("test/000-common.js")

var ExpressTesterLib = require("ExpressTester")
var app = require( "../account-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    var email1 = "emailtest1@email.ee"
    var email2 = "emailtest2@email.ee"
    var otherAddedEmail = "emailtest-added@email.ee"
    var oGetMailer

    var password = "easy1234"

    var userIds = {}
    var TestCommon = TestCommonLib(mapi, assert, userIds)


    describe("preparation", function(){

        TestCommon.CreateAccount({e_email:email1, c_password: password, "u_language": "hu"}, "acc1")
        TestCommon.CreateAccount({e_email:email2, c_password: password, "u_language": "hu"}, "acc2")


    })


    describe('mail confirmation', function() {

        it('sending before service configured', function(done) {

             mapi.post( "/account/"+userIds.acc1+"/email/"+email1+"/send/confirmation", "", function(err, result, httpResponse){

                assert.isNotNull(err)
                assert.equal(err.message, "SERVICE_NOT_CONFIGURED_SERVERS")

                done()
             })
        })

        it('setting mailer service', function() {
             return assert.isFulfilled(
                 mapi.postAsync( "/service/relayer", {"host":"127.0.0.1","port":443,"scheme":"https","api_secret":"z"})
             )
        })

        var e_token
        it('sending', function(done) {
             oGetMailer = app.GetMailer
             app.GetMailer = function() {
                 var re = {}
                 re.SendMailAsync = function(data) {
                     assert.deepProperty(data, "context.email.e_token")
                     assert.deepProperty(data, "context.user.u_id")
                     assert.deepPropertyVal(data, "locale", "hu")

                     e_token = data.context.email.e_token

                     return Promise.resolve("ok")
                 }
                 return re
             }

             mapi.post( "/account/"+userIds.acc1+"/email/"+email1+"/send/confirmation", "", function(err, result, httpResponse){
                assert.equal(result, "ok")

                done()
             })
        })

        it('verifying', function(done) {

             mapi.post( "/account/"+userIds.acc1+"/email/"+email1+"/verify", {confirmation_token: e_token}, function(err, result, httpResponse){
                assert.equal(result, "ok")

                done()
             })
        })


        it('account fetch, email should be verified', function(done) {

             mapi.get( "/account/"+userIds.acc1, function(err, result, httpResponse){
                console.log(result)
                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)
                assert.propertyVal(result, "u_primary_email_confirmed", 1)
                done()

             })
        })

        it('verifying again', function(done) {

             mapi.post( "/account/"+userIds.acc1+"/email/"+email1+"/verify", {confirmation_token: e_token}, function(err, result, httpResponse){
                assert.isNotNull(err)
                assert.equal(err.message, "ACCOUNT_EMAIL_ALREADY_VERIFIED")

                done()
             })
        })

        it('sending again', function(done) {

             mapi.post( "/account/"+userIds.acc1+"/email/"+email1+"/send/confirmation", "", function(err, result, httpResponse){
                assert.isNotNull(err)
                assert.equal(err.message, "ACCOUNT_EMAIL_ALREADY_VERIFIED")

                done()
             })
        })


        it('sending for other account using the default email address', function(done) {

             mapi.post( "/account/"+userIds.acc2+"/email/confirmation/send", "", function(err, result, httpResponse){
                assert.equal(result, "ok")

                done()
             })
        })

        it('verifying other account using the default email address', function(done) {

             mapi.post( "/account/"+userIds.acc2+"/email/confirmation/verify", {confirmation_token: e_token}, function(err, result, httpResponse){
                assert.equal(result, "ok")

                done()
             })
        })


    })

    describe('additional email related functions', function() {
        it('trying to delete primary', function(done) {

             mapi.delete( "/account/"+userIds.acc1+"/email/"+email1, {}, function(err, result, httpResponse){

                assert.isNotNull(err)
                assert.equal(err.message, "PRIMARY_EMAIL_CANNOT_BE_DELETED")

                done()
             })
        })

        it('adding email 1', function(done) {

             mapi.put( "/account/"+userIds.acc1+"/emails", {"e_email": otherAddedEmail, "e_categories": "OTHER"}, function(err, result, httpResponse){

                assert.equal(result, "ok")

                done()
             })
        })

        it('email details with OTHER category', function(done) {
             mapi.get( "/account/"+userIds.acc1+"/emails", function(err, result, httpResponse){

                assert.equal(result.length, 2)
                assert.deepEqual(result[1].e_categories, ["OTHER"])

                done()

             })
        })

        it('changing email category to TECH', function(done) {

             mapi.post( "/account/"+userIds.acc1+"/email/"+otherAddedEmail+"/categories", {"e_categories": "TECH"}, function(err, result, httpResponse){

                assert.equal(result, "ok")

                done()
             })
        })

        it('email details with TECH category', function(done) {
             mapi.get( "/account/"+userIds.acc1+"/emails", function(err, result, httpResponse){

                assert.equal(result.length, 2)
                assert.deepEqual(result[1].e_categories, ["TECH"])

                done()

             })
        })


        it('listing emails with non existent category should return the primary email', function(done) {
             mapi.get( "/account/"+userIds.acc1+"/emails/FOOBAR", function(err, result, httpResponse){

                assert.equal(result.length, 1)
                assert.equal(result[0].e_email, email1)

                done()

             })
        })


        it('set primary email fail', function(done) {

             mapi.post( "/account/"+userIds.acc1+"/email/"+otherAddedEmail+"/set_primary",{}, function(err, result, httpResponse){

                assert.isNotNull(err)
                assert.equal(err.message, "ACCOUNT_EMAIL_NOT_VERIFIED")

                done()
             })

        })

        it('deleting email', function(done) {

             mapi.delete( "/account/"+userIds.acc1+"/email/"+otherAddedEmail, {}, function(err, result, httpResponse){

                assert.equal(result, "ok")

                done()
             })
        })

        it('adding email 2', function(done) {

             mapi.put( "/account/"+userIds.acc1+"/emails", {"e_email": otherAddedEmail,"e_confirmed": true}, function(err, result, httpResponse){

                assert.equal(result, "ok")

                done()
             })
        })

        it('adding email 3', function(done) {

             mapi.put( "/account/"+userIds.acc1+"/emails", {"e_email": "surely.not@primary.email","e_confirmed": true}, function(err, result, httpResponse){

                assert.equal(result, "ok")

                done()
             })
        })

        it('set primary email succeed', function(done) {

             app.MonsterMailer = function() {
                 var re = {}
                 re.SendMailAsync = function(data) {
                     assert.deepPropertyVal(data, "locale", "hu")
                     assert.deepPropertyVal(data, "context.user.old_primary_email", email1)
                     assert.deepPropertyVal(data, "context.user.u_primary_email", otherAddedEmail)
                     assert.deepPropertyVal(data, "cc", email1)
                     assert.deepProperty(data, "context.email.e_token")
                     assert.deepProperty(data, "context.user.u_id")

                     return Promise.resolve("ok")
                 }
                 return re
             }

             mapi.post( "/account/"+userIds.acc1+"/email/"+otherAddedEmail+"/set_primary",{}, function(err, result, httpResponse){

                assert.equal(result, "ok")

                done()
             })

        })

        it('authenticate with new username', function(done) {
             mapi.post( "/authentication/credential", {username:otherAddedEmail,password:password}, function(err, result, httpResponse){
                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)
                assert.property(result, "token" )
                validAuthToken  = result.token
                done()
             })
        })



        it('deleting old primary', function(done) {

             mapi.delete( "/account/"+userIds.acc1+"/email/"+email1, {}, function(err, result, httpResponse){

                assert.equal(result, "ok")

                app.GetMailer = oGetMailer

                done()
             })
        })

    })


    describe("public categories", function(){

        it('list of supported email categories is returned', function(done) {
             mapi.get( "/email/categories", function(err, result, httpResponse){
                assert.equal(Array.isArray(result), true)
                done()

             })

        })

    })

    describe('global route should return the email addresses', function() {

        var first;

        it("list emails", function(done){
             mapi.post( "/emails/search", {}, function(err, result, httpResponse){
                // console.log(result)
                assert.ok(Array.isArray(result));
                assert.ok(result.length > 0);
                first = result[0].e_email;
                done();

             })

        });


        it("setting escalate flag to true", function(done){
             mapi.post( "/emails/set-escalate", {e_email:first, e_escalate:true}, function(err, result, httpResponse){
                // console.log(result)
                assert.equal(result, "ok");
                done();

             })

        });

        it("list emails", function(done){
             mapi.post( "/emails/search", {}, function(err, result, httpResponse){
                // console.log(result)
                assert.propertyVal(result[0], "e_escalate", 1);
                done();

             })

        });


        it("setting escalate flag to false", function(done){
             mapi.post( "/emails/set-escalate", {e_email:first, e_escalate:false}, function(err, result, httpResponse){
                // console.log(result)
                assert.equal(result, "ok");
                done();

             })

        });

        it("list emails", function(done){
             mapi.post( "/emails/search", {}, function(err, result, httpResponse){
                // console.log(result); process.reallyExit();
                assert.propertyVal(result[0], "e_escalate", 0);
                done();

             })

        });
    });

}, 10000)

