require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var TestCommonLib = require("test/000-common.js")

var ExpressTesterLib = require("ExpressTester")
var app = require( "../account-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    var email = "derivation@email.ee"

    var userIds = {}
    var tokens = {}

    var TestCommon = TestCommonLib(mapi, assert, userIds, tokens)

    var servers = Array("derivedtherapy", "derivedmadness")


    var auth_codes = {};

    var someTokenPayload = {server: servers[0]};

    describe('preparation', function() {

        servers.forEach(name=>{
            it("adding server first ("+name+")", function(done){

                 mapi.put( "/servers", {"s_name": name, "s_secret": "lie down on the coach", "s_roles":["accountapi"]}, function(err, result, httpResponse){
                    assert.property(result, "auth_code")
                    done()

                 })
            })

        })


            it("querying their auth code", function(done){

                 mapi.get( "/servers/full", function(err, result, httpResponse){
                    result.forEach(s=>{
                        assert.ok(s.auth_code);
                        auth_codes[s.s_name] = s.auth_code
                    })

                     // console.log(auth_codes);
                     someTokenPayload.auth_code = auth_codes[servers[0]];
                     done()

                 })
            })

        TestCommon.CreateAccount({e_email:email}, "main")

    })

    describe('primary token', function(){


        it('classic credential based authentication (so we learn the token)', function(done) {
             mapi.post( "/authentication/credential", {username:email,password:"easy1234"}, function(err, result, httpResponse){
                TestCommon.ValidTokenResponse(err, result, httpResponse, "main")

                done()
             })
        })

        function emptyDerivedTokens(result){
                assert.property(result, "derived_server_tokens")
                assert.ok(result.derived_server_tokens);

                assert.equal(Object.keys(result.derived_server_tokens).length, 0)
        }


        // when sending no server parameter, derived tokens should not be calculated
        TestCommon.TokenTestWithAuthCode(["ACCOUNT_OWNER"], "main", {}, emptyDerivedTokens)

        // when sending no auth_code parameter, derived tokens should not be calculated
        TestCommon.TokenTestWithAuthCode(["ACCOUNT_OWNER"], "main", {server:servers[0]}, emptyDerivedTokens);

        // when sending incorrect auth_code paramter, derived tokens should not be calculated
        TestCommon.TokenTestWithAuthCode(["ACCOUNT_OWNER"], "main", {server:servers[0], auth_code: "somethingincorrect"}, emptyDerivedTokens);


        var derived_server_tokens
        TestCommon.TokenTestWithAuthCode(["ACCOUNT_OWNER"], "main", someTokenPayload, function(result){
                assert.property(result, "derived_server_tokens")
                assert.ok(result.derived_server_tokens);

                assert.property(result, "derived_mode", false)
                assert.ok(Object.keys(result.derived_server_tokens).length >= servers.length)

                servers.forEach(name=>{
                   assert.property(result.derived_server_tokens, name)
                   assert.ok(result.derived_server_tokens[name])
                   assert.equal(result.derived_server_tokens[name].split(":").length, 3);

                })

                // console.log(result);process.reallyExit();

               derived_server_tokens = result.derived_server_tokens
        })


        it('derived token should be rejected without a server parameter', function(done) {
             mapi.post( "/authentication/token", {token:derived_server_tokens.derivedtherapy}, function(err, result, httpResponse){
                assert.isNotNull(err)
                assert.propertyVal(err, "message", "AUTH_FAILED")

                done()
             })

        })

        it('derived token should be rejected with unknown server name', function(done) {
             mapi.post( "/authentication/token", {token:derived_server_tokens.derivedtherapy, server: "foobar"}, function(err, result, httpResponse){
                assert.isNotNull(err)
                assert.propertyVal(err, "message", "SERVER_NOT_FOUND")

                done()
             })

        })


        it('derived token should be rejected if the token itself is incorrect (therapy server with madness token)', function(done) {
             mapi.post( "/authentication/token", {token:derived_server_tokens.derivedmadness, server: "derivedtherapy"}, function(err, result, httpResponse){
                assert.isNotNull(err)
                assert.propertyVal(err, "message", "AUTH_FAILED")

                done()
             })
        })


        it('derived token should be rejected if the token itself is incorrect (madness server with therapy token)', function(done) {
             mapi.post( "/authentication/token", {token:derived_server_tokens.derivedtherapy, server: "derivedmadness"}, function(err, result, httpResponse){
                assert.isNotNull(err)
                assert.propertyVal(err, "message", "AUTH_FAILED")

                done()
             })
        })

        it('and if everything is correct, user details should be returned (without derived_server_tokens and with originatingServer)', function(done) {
             mapi.post( "/authentication/token", {token:derived_server_tokens.derivedtherapy, server: "derivedtherapy"}, function(err, result, httpResponse){
                // console.log(result);
                assert.isNull(err)

                assert.propertyVal(result.session, "originatingServer", servers[0]);

                assert.property(result, "derived_server_tokens")
                assert.equal(Object.keys(result.derived_server_tokens).length, 0)

                assert.property(result, "derived_mode", true)

                assert.property(result, "account")
                assert.property(result, "session")
                assert.notProperty(result.session, "s_authtoken")
                assert.notProperty(result.session, "s_unique_session_identifier" )

                done()
             })
        })


    })


}, 10000)

