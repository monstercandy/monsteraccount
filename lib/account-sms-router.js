module.exports = function(router, app) {

    router.put("/sms/", function(req,res,next){
       return req.sendOk(app.getSmsLib().Insert(req.body.json, req));
    })

      router.post("/sms/count", doCount);
      router.post("/sms/search", doSearch)


    function doCount(req,res,next){
         return req.sendPromResultAsIs(app.getSmsLib().Count(req.body.json))
    }

    function doSearch(req,res,next){
         return req.sendPromResultAsIs(
            app.getSmsLib().Query(req.body.json)
          )
    }


}
