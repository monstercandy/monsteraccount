var lib = module.exports = function(app){

    var router = app.ExpressPromiseRouter({mergeParams: true})

    const MError = app.MError

    var Credential = require("models/credential.js")
    var Server = require("models/server.js")
    var WebhostingTemplates = require("models/webhostingtemplates.js")


  router.get("/languages", function(req,res,next){
      req.sendResponse(Common.getSupportedLanguages());
  })
  router.get("/servers/roles", function(req,res,next){
     req.sendResponse(Server.SERVER_ROLES);
  })
  function getTxtResponse(){
    return {required: !app.config.get("webhosting_domains_can_be_added_without_txt_records")};
  }
  router.get("/webhostingdomains/txtrecords", function(req,res,next){
     return req.sendResponse(getTxtResponse());
  });
  router.get("/credentials/permissions", function(req,res,next){
     req.sendResponse(Credential.supportedPermissions);
  })

  router.get("/webhostingtemplates/dynamiclanguages", function(req,res,next){
     req.sendResponse(WebhostingTemplates.dynamicLanguages);
  })

  router.get("/loginlog/subsystems", function(req,res,next){
     req.sendResponse(lib.LOGINLOG_SUPPORTED_SUBSYSTEMS);
     next()
  })

  router.get("/callback_request", function(req,res,next){
     req.sendResponse({"supported":app.get_callback_request_email() ? true : false });
  })

    router.route("/sms/providers")
      .get(function(req,res,next){
          return req.sendResponse(Object.keys(app.smsProviders));
      })

    router.route("/sms/:provider/credentials")
       .get(function(req,res,next){
       	  return req.sendResponse(app.smsLib.GetCredentials(req.params.provider))
        })
       .post(function(req,res,next){
       	  return req.sendOk(app.smsLib.SetCredentials(req.params.provider, req.body.json))
        })
       .delete(function(req,res,next){
       	  return req.sendOk(app.smsLib.ClearCredentials(req.params.provider))
        })



    return router


}
lib.LOGINLOG_SUPPORTED_SUBSYSTEMS =  [ "accountapi", "ftp", "email" ];
