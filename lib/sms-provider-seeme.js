module.exports = function(app){
	const MError = require("MonsterError");

	var re = {};

	re.SendSms = function(validated_data) {
		var api_key;
		return Promise.resolve()
		  .then(()=>{
		  	  var raw = app.GetRawCredentials("seeme");
		  	  api_key = raw.ApiKey;
		  	  api_url = app.config.get("sms_seeme_api_url");
		  	  if((!api_key)||(!api_url)) throw new MError("NOT_CONFIGURED");

		  	  var fullNumber = validated_data.s_country_code + validated_data.s_area_code + validated_data.s_phone_no;

		  	  return app.PostForm(api_url, {key: api_key, message: validated_data.s_message, number: fullNumber}, {method: "get"});
		  })

	}

	re.credentialsValidator =  {
      ApiKey:{presence:true,isString:true},
    }


	return re;
}
