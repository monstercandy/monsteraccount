var lib = module.exports = function(knex, app) {

    const dotq = require("MonsterDotq")

    var vali = require("MonsterValidators").ValidateJs()

    function eventlogQueryParams(d){
          return vali.async(d, {
               "limit": {default: 100, numericality:{onlyInteger: true, strict: true, lessThanOrEqualTo: 100}},
               "offset": {default: {value:0}, isInteger:true},
               "order": {inclusion: ["l_username","l_event_type","l_subsystem", "l_timestamp", "l_ip"], default: "l_timestamp"},
               "desc": {isBooleanLazy: true},
               "hide_superuser": {isBooleanLazy: true},

               "result_apply_timezone": {isString: true},

               // where fields
               "notbefore": {isString: true},
               "notafter": {isString: true},

               "l_server": {isString: true},
               "l_event_type": {isString: true},
               "l_event_type_like": {isString: {trim:true}},
               "l_other": {isString: true},
               "l_subsystem": {isString: true},
               "l_username": {isString: true},
               "l_agent": {isString: true},
               "l_user_id": {isString: true},
               "l_ip": {isString: {trim:true}},
               "l_subsystem": {isString: {trim:true}},
               "l_processed": {isBooleanLazy:true},
          })
    }
    function eventlogQueryWhere(d, knex){

       var qStr = [1]
       var qArr = []

       if(d.hide_superuser) {
          qStr.push("l_event_type != 'superuser-switch-user'");
       }

       if(d.notbefore){
          qStr.push("l_timestamp>=?")
          qArr.push(d.notbefore)
       }
       if(d.notafter){
          qStr.push("l_timestamp<=?")
          qArr.push(d.notafter)
       }

       if(d.l_server){
          qStr.push("l_server=?")
          qArr.push(d.l_server)
       }
       if(d.l_other){
          qStr.push("l_other LIKE ?")
          qArr.push('%'+d.l_other+'%')
       }
       if(d.l_username){
          qStr.push("l_username LIKE ?")
          qArr.push('%'+d.l_username+'%')
       }
       if(d.l_user_id){
          qStr.push("l_user_id=?")
          qArr.push(d.l_user_id)
       }
       if(d.l_ip){
          qStr.push("l_ip LIKE ?")
          qArr.push('%'+d.l_ip+'%')
       }
       if(d.l_agent){
          qStr.push("l_agent LIKE ?")
          qArr.push('%'+d.l_agent+'%')
       }
       if(d.l_event_type_like){
          qStr.push("l_event_type LIKE ?")
          qArr.push(d.l_event_type_like+'%')
       }
       if(d.l_event_type){
          qStr.push("l_event_type=?")
          qArr.push(d.l_event_type)
       }
       if(d.l_subsystem){
          qStr.push("l_subsystem=?")
          qArr.push(d.l_subsystem)
       }
       if(typeof d.l_processed != "undefined"){
          qStr.push("l_processed=?")
          qArr.push(d.l_processed)
       }

       var qStr = qStr.join(" AND ")

       return knex.whereRaw(qStr, qArr)

    }



    var re = {}


    const moment = require("MonsterMoment") // we require this so now will exist for sure
    const MError = app.MError

    if(!app.eventSync) app.eventSync = [];

    if(!app.eventSyncTimer)
        rescheduleEventSyncTimer()


    re.Insert = function(in_data){
       return vali.async(in_data, {server: {presence:true, isString:true}, events:{presence:true, isArray:true}})
         .then(d=>{
            d.events.forEach(x=>{
                x.l_server = d.server;
                return x;
            });

            app.eventSync = app.eventSync.concat(d.events);
         })
    }


    re.Query = function (pd) {
           var d
           return eventlogQueryParams(pd)
              .then(ad=>{
                 d = ad
                 var s = eventlogQueryWhere(d,knex.select()
                   .table('loginlog')
                   .limit(d.limit)
                   .offset(d.offset)
                   .orderBy(d.order, d.desc?"desc":undefined))
                 // console.log(s.toString())
                 return s
              })
              .then(function(rows) {

                moment.ApplyTimezoneOnArrayOfHashes(rows, ["l_ts"], d.result_apply_timezone)
                return Promise.resolve({"events": rows })

              })
    }

    re.Count = function (d) {
           return eventlogQueryParams(d)
              .then(d=>{
                 var s = eventlogQueryWhere(d, knex('loginlog').count("*"))
                 // console.log(s.toString())
                 return s
              })
              .then(function(rows) {
                return Promise.resolve({"count": rows[0]["count(*)"] })
              })
    }



    return re;

    function rescheduleEventSyncTimer(){
        app.eventSyncTimer = setTimeout(function(){

            var dataToInsert = app.eventSync;
            app.eventSync = [];

            return Promise.resolve()
              .then(()=>{
                 if(!dataToInsert.length) return;

                  return knex.transaction(trx=>{

                     return dotq.linearMap({array:dataToInsert,action:function(row){
                        return trx.into("loginlog").insert(row).return()
                     }})

                  })
                  .then(()=>{
                     console.log("Successfully inserted", dataToInsert.length, "events into loginlog");
                  })


              })
              .catch(ex=>{
                 // but the show must go on...
                 console.error("We were unable to flush events to loginlog", ex)
              })
              .then(()=>{
                 return rescheduleEventSyncTimer()
              })


        }, app.config.get("event_sync_flush_seconds")*1000)

    }

}

