(function(){
  var lib = module.exports = function(knex, app) {

    const telLib = require("models/tel.js")

    const dotq = require("MonsterDotq")

    var vali = require("MonsterValidators").ValidateJs()

    function smsQueryParams(d){
          return vali.async(d, {
               "limit": {default: 100, numericality:{onlyInteger: true, strict: true, lessThanOrEqualTo: 100}},
               "offset": {default: {value:0}, isInteger:true},
               "order": {inclusion: ["s_id","s_recipient","s_status", "s_timestamp"], default: "s_timestamp"},
               "desc": {isBooleanLazy: true},

               // where fields
               "notbefore": {isString: true},
               "notafter": {isString: true},

               "s_provider": {isString: true},
               "s_country_code": {isString: true},
               "s_area_code": {isString: true},
               "s_phone_no": {isString: true},
               "s_recipient": {isString: true},
               "s_status": {isString: true},
               "s_message": {isString: true},
          })
    }
    function smsQueryWhere(d, knex){

       var qStr = [1]
       var qArr = []

       if(d.notbefore){
          qStr.push("s_timestamp>=?")
          qArr.push(d.notbefore)
       }
       if(d.notafter){
          qStr.push("s_timestamp<=?")
          qArr.push(d.notafter)
       }

       Array("s_provider", "s_status").forEach(x=>{
         if(d[x]){
            qStr.push(x+"=?")
            qArr.push(d[x])
         }
       })

       Array("s_recipient", "s_message", "s_country_code", "s_area_code", "s_phone_no").forEach(x=>{
         if(d[x]){
            qStr.push(x+" LIKE ?")
            qArr.push('%'+d[x]+'%')
         }

       })

       var qStr = qStr.join(" AND ")

       return knex.whereRaw(qStr, qArr)

    }

    var providerNames = Object.keys(app.smsProviders);


    var re = {}


    const MError = app.MError

  function setCredentials(provider, new_creds){
      var vol = app.config.get("volume")
      if(!new_creds)
          delete vol[provider]
      else
          vol[provider] = new_creds
      app.config.saveVolume(vol)

  }
  re.ClearCredentials = function(provider){
    return setCredentials(provider)
  }
  re.SetCredentials = function(provider, in_data){
    var constraints = getProviderOrThrow(provider).credentialsValidator;

    return vali.async(in_data, constraints)
      .then(d=>{
        return setCredentials(provider, d)
      })
  }
  re.GetCredentials = function(provider){
    var prov = getProviderOrThrow(provider);
    var rawNc = app.GetRawCredentials(provider);
    if((!rawNc)||(!Object.keys(rawNc).length)) return {configured: false};
    return prov.getCredentials ? prov.getCredentials(rawNc) : {configured: true};
  }

    re.Insert = function(in_data, req){

       var wasSentSuccessfullyByProvider;
       var d;
       var lastException;

       return vali.async(in_data, {
          s_provider: {inclusion: providerNames},

          s_country_code: telLib.validator_country_code,
          s_area_code: telLib.validator_area_code,
          s_phone_no: telLib.validator_phone_no,

          s_message: {presence: true, isString: true},
       })
       .then(ad=>{
          d = ad;

          d.s_recipient = telLib.TelToHumanString(d, "s");

          var availableProviderNames = d.s_provider ? [d.s_provider] : providerNames

          return dotq.linearMap({array: availableProviderNames, catch: function(ex, provider){
            console.error("unable to send sms via", provider, ex);
            lastException = ex;
          }, action: function(providerName){
              if(wasSentSuccessfullyByProvider) return;

              return getProvider(providerName).SendSms(d)
                .then(()=>{
                   wasSentSuccessfullyByProvider = providerName;
                })
          }})

       })
       .then(()=>{
         // and insert.
         if(wasSentSuccessfullyByProvider) {
           d.s_provider = wasSentSuccessfullyByProvider;
           d.s_status = "sent";
         }
         else
           d.s_status = "failed";

         app.InsertEvent(req, {e_event_type: "sms-send", e_other: d});

         return knex("sms").insert(d).return();

       })
       .then(()=>{
         if(lastException) throw lastException;
       })
    }


    re.Query = function (pd) {
           var d
           return smsQueryParams(pd)
              .then(ad=>{
                 d = ad
                 var s = smsQueryWhere(d,knex.select()
                   .table('sms')
                   .limit(d.limit)
                   .offset(d.offset)
                   .orderBy(d.order, d.desc?"desc":undefined))
                 // console.log(s.toString())
                 return s
              })
    }

    re.Count = function (d) {
           return smsQueryParams(d)
              .then(d=>{
                 var s = smsQueryWhere(d, knex('sms').count("*"))
                 // console.log(s.toString())
                 return s
              })
              .then(function(rows) {
                return {"count": rows[0]["count(*)"] }
              })
    }



    return re;

    function getProvider(providerName){
       return app.smsProviders[providerName]
    }
    function getProviderOrThrow(providerName){
       var re =getProvider(providerName);
       if(!re) throw new MError("PROVIDER_NOT_FOUND");
       return re;
    }

}



})()
