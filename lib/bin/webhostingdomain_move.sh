#!/bin/bash

WD_SERVER=$1
WD_DOMAIN_NAME=$2
OLD_WH=$3
DEST_WH_ID=$4

if [ -z "$DEST_WH_ID" ]; then
   echo "Usage: $0 server domain old_wh_id dest_wh_id"
   exit 1
fi

/opt/client-libs-for-other-languages/internal/perl/generic-light.pl account POST "/accountapi/webhostingdomains/move" \
   "{\"wd_server\":\"$WD_SERVER\",\"domain\":\"$WD_DOMAIN_NAME\",\"wd_webhosting\":\"$OLD_WH\",\"dest_wh_id\":$DEST_WH_ID}"

echo
echo "Dont forget to change the old database as well!"
echo "UPDATE w3_domain SET do_tarhely=(SELECT th_id FROM w3_tarhely WHERE th_mc_id='$DEST_WH_ID' AND th_server='$WD_SERVER')  WHERE do_domain='$WD_DOMAIN_NAME'"
echo
