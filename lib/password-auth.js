module.exports = function(storedHash, clearPassword) {

    var re = {result: false}


    if(storedHash.match(/^\*/)) {

         re.type = "mysql"

         // mysql based
         var crypto = require("crypto")
         var intermediate = crypto.createHash('sha1').update(clearPassword).digest()
         var finalHash = crypto.createHash('sha1').update(intermediate).digest()
         var finalHashString = finalHash.toString('hex');
         var toCompare = "*" + finalHashString.toUpperCase()


         re.result = (storedHash.toUpperCase() == toCompare)
         return Promise.resolve(re)
    } else {
 
        // bcrypt:
        re.type = "bcrypt"

        var bcrypt = require('bcrypt-promise.js');
        return bcrypt.compareAsync(clearPassword, storedHash)
          .then((v)=>{
             re.result = v
             return Promise.resolve(re)
          })
    }

    return re
}
