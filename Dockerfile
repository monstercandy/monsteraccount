FROM monstercommon

ADD lib /opt/MonsterAccount/lib/
ADD etc /etc/monster/
RUN INSTALL_DEPS=1 /opt/MonsterAccount/lib/bin/account-install.sh && /opt/MonsterAccount/lib/bin/account-test.sh

ENTRYPOINT ["/opt/MonsterAccount/lib/bin/account-start.sh"]
